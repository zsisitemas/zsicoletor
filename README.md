# :computer: ***Desenvolvimento:***
- :bulb: **[Izaias](https://www.linkedin.com/in/izaias-filho-0a043a63/)**
    - Coordenador de projetos
    - Desenvolvedor
        - Back End
- :keyboard: **[Cristiano](https://www.linkedin.com/in/cristiano-jr-6a6519213/)**
    - Desenvolvedor
        - Front End
        - Back End

# :gear: ***Ferramentas:***

- :desktop_computer: **IDE**
    - Android Studio
- :coffee: **Linguagem**
    - Java
- :file_cabinet:**Banco de dados** 
    - MySQL Workbench
    - SQLite
    - phpMyAdmin

# :iphone: ***Funcionalidades:***

1. :bust_in_silhouette: **Sistema de Login**
    - Permitir o controle das visualizações de acordo com o tipo de usuário logado.
        - Tipos de usuário:
            - Desenvolvedor
            - Administrador
            - Funcionário
2. :shopping_cart: **Sistema de Carrinho**
    - Gerar pedidos local ou remotamente relacionando os produtos e cliente selecionados ao usuário logado.
        - Produtos:
            - Listar
            - Cadastrar novo
        - Clientes:
            - Listar
            - Cadastrar novo
            - Atualizar existente
            - Inativar existente
        - Pedidos:
            - Atualizar existente
            - Cancelar existente
3. :bar_chart: **Gerenciamento de Lucros**
    - Possibilitar o acompanhamento geral dos lucros obtidos com as vendas.
        - Filtro de resultados
4. :package: **Gerenciamento de Estoque**
    - Realizar ajustes na quantidade de estoque e inventário e ter controle sobre a data de validade de um produto.
        - Localizar produto para realizar ajustes:
            - Scanner
        - Produtos em estoque com data de validade próxima:
            - Listar
            - Adicionar novo
            - Inativar existente
5. :left_right_arrow: **Sistema de Importar/Exportar**
    - Converter arquivos .xlsx para SQLite e vice-versa.
        - Importar:
            - Popular as tabelas do banco SQLite do dispositivo a partir do arquivo .xlsx selecionado através do gerenciador de arquivos do próprio dispositivo
        - Exportar:
            - Criar arquivo .xlsx baseado nas tabelas do banco SQLite do próprio dispositivo
