package com.zsisistemas.zsiempresa.db;

import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class ConexaoCloud extends _Default implements Runnable {

    // Recuperar dados salvos no dispositivo
    private String nome_preferencias = SingletonConfiguracoes.getConfig().getNomeBanco();
    private String userRoot = SingletonConfiguracoes.getConfig().getRoot();

    // Dados do Banco nuvem
    private Connection conn;
    private String host = "sql172.main-hosting.eu";
    private String db = nome_preferencias;
    private int port = 3306;
    private String user = !nome_preferencias.equals("u323187073_empre") ? nome_preferencias : "u323187073_izs";
    private String pass = !nome_preferencias.equals("u323187073_empre") ? "Mnd02661" : "mnd0266";
    private String url = "jdbc:mysql://%s:%d/%s";

    // Dados do Banco local
    public ConexaoCloud() {
        super();
        this.url = String.format(this.url, this.host, this.port, this.db);

        this.conectaCloud();
        this.disconecta();
    }

    // Formar Url do banco
    @Override
    public void run() {
        try{
           // this.user = this.userRoot;
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection(this.url,this.user,this.pass);
        }catch (Exception e){
            this._mensagem = e.getMessage();
            this._status = false;
        }
    }

    // Abrir Conexão
    private void conectaCloud(){
        Thread thread = new Thread(this);
        thread.start();
        try{
            thread.join();
        }catch (Exception e){
            this._mensagem = e.getMessage();
            this._status = false;
        }
    }

    // Fechar Conexão
    private void disconecta(){
        if (this.conn!= null){
            try{
                this.conn.close();
            }catch (Exception e){

            }finally {
                this.conn = null;
            }
        }
    }

    // Ler dados no Banco
    public ResultSet select(String query,String tabela){
        this.conectaCloud();
        ResultSet resultSet = null;
        try {
            resultSet = new ExecuteQueryCloud(this.conn, query,false,tabela).execute().get();
        }catch (Exception e){
            this._status = false;
            this._mensagem = e.getMessage();
        }
        return resultSet;
    }

    // Criar dados no Banco
    public ResultSet execute(String query,String tabela){
        this.conectaCloud();
        ResultSet resultSet = null;
        try {
            resultSet = new ExecuteQueryCloud(this.conn, query,true,tabela).execute().get();
        }catch (Exception e){
            this._status = false;
            this._mensagem = e.getMessage();
        }
        return resultSet;
    }
}
