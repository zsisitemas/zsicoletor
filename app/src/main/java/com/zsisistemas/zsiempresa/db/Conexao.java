package com.zsisistemas.zsiempresa.db;

import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class Conexao extends _Default implements Runnable {

    // Recuperar dados salvos no dispositivo
    private String ip_preferencias = SingletonConfiguracoes.getConfig().getIp();
    private int conn_preferencias = Integer.parseInt(SingletonConfiguracoes.getConfig().getConnection());
    private String nome_preferencias = SingletonConfiguracoes.getConfig().getNomeBanco();

    // Dados do Banco nuvem
    private Connection conn;
    private String host = "sql172.main-hosting.eu";
    private String db = nome_preferencias;
    private int port = 3306;
    private String user = "u323187073_pdv";
    private String pass = "mnd0266";
    private String url = "jdbc:mysql://%s:%d/%s";

    // Dados do Banco local
    public Conexao() {
        super();
        int tipoConexao = conn_preferencias;

        if(tipoConexao == 1 ){
            this.host = ip_preferencias;
            this.user = "root";
        }else{
            if(tipoConexao==2){
                this.host = ip_preferencias;
                this.user = "Retaguarda";
            }
        }

        this.url = String.format(this.url, this.host, this.port, this.db);

        this.conecta();
        this.disconecta();
    }

    // Formar Url do banco
    @Override
    public void run() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection(this.url,this.user,this.pass);
        }catch (Exception e){
            this._mensagem = e.getMessage();
            this._status = false;
        }
    }

    // Abrir Conexão
    private void conecta(){
        Thread thread = new Thread(this);
        thread.start();
        try{
            thread.join();
        }catch (Exception e){
            this._mensagem = e.getMessage();
            this._status = false;
        }
    }

    // Fechar Conexão
    private void disconecta(){
        if (this.conn!= null){
            try{
                this.conn.close();
            }catch (Exception e){

            }finally {
                this.conn = null;
            }
        }
    }

    // Ler dados no Banco
    public ResultSet select(String query,String tabela){
        this.conecta();
        ResultSet resultSet = null;
        try {
            resultSet = new ExecuteQueryMysql(this.conn, query,false,tabela).execute().get();
        }catch (Exception e){
            this._status = false;
            this._mensagem = e.getMessage();
        }
        return resultSet;
    }

    // Criar dados no Banco
    public ResultSet execute(String query,String tabela){
        this.conecta();
        ResultSet resultSet = null;
        try {
            resultSet = new ExecuteQueryMysql(this.conn, query,true,tabela).execute().get();
        }catch (Exception e){
            this._status = false;
            this._mensagem = e.getMessage();
        }
        return resultSet;
    }
}
