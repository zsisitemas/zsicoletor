package com.zsisistemas.zsiempresa.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ConexaoSqlite extends SQLiteOpenHelper {

    private static ConexaoSqlite mInstance = null;
    private static final String name = "banco";
    private static final int version = 3;

    public ConexaoSqlite(@Nullable Context context) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table produtos(id_Produtos integer primary key autoincrement, " +
                    "codigo_produto string(500), descricao_produto string(500), unidade string(10), preco_venda double, " +
                    "estoque double, qtd_inventario double , data string(500), status int, quantidadeProdutos double, preco_total double," +
                    "impressao int)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**
     * Não se deve inicializar o objeto helper usando new DatabaseHelper(context).
     * Envés disso, sempre usar DatabaseHelper.getInstance(context), para garantir que
     * apenas um database helper existirá para o ciclo de vida inteiro da aplicação.
     */
    public static ConexaoSqlite getInstance(Context context){
        if (mInstance == null) {
            mInstance = new ConexaoSqlite(context.getApplicationContext());
        }
        return mInstance;
    }
}
