package com.zsisistemas.zsiempresa.db;

import android.os.AsyncTask;

import com.zsisistemas.zsiempresa.model.Chave;
import com.zsisistemas.zsiempresa.model.ClienteModel;
import com.zsisistemas.zsiempresa.model.Cupom;
import com.zsisistemas.zsiempresa.model.Cupom_itemModel;
import com.zsisistemas.zsiempresa.model.Empresa;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.ValidadeModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom_item;
import com.zsisistemas.zsiempresa.model.singleton.SingletonEmpresa;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;
import com.zsisistemas.zsiempresa.model.singleton.SingletonValidade;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ExecuteQueryMysql extends AsyncTask<String,Void,ResultSet> {

    // Guardar os dados após a leitura do banco
    ArrayList<ProdutoModel> listaProdutos_db = new ArrayList<>();
    ArrayList<ClienteModel> listaClientes_db = new ArrayList<>();
    ArrayList<Cupom_itemModel> listaPedidos_db = new ArrayList<>();
    ArrayList<Cupom> listaCupom_db = new ArrayList<>();
    ArrayList<ClienteModel> listaFuncionarios_db = new ArrayList<>();
    ArrayList<ValidadeModel> listaValidade_db = new ArrayList<>();
    ArrayList<Chave> listaChaves = new ArrayList<>();
    ArrayList<Empresa> listaEmpresa = new ArrayList<>();

    private Connection connection;
    private String query;
    private String tabela;
    private boolean tipoQuery;  // Verifica se a query é para leitura(false) ou inserção(true) de dados

    public ExecuteQueryMysql(Connection connection, String query, boolean tipoQuery, String tabela) {
        this.connection = connection;
        this.query = query;
        this.tipoQuery = tipoQuery;
        this.tabela =tabela;
    }

    @Override
    protected ResultSet doInBackground(String... params) {
        ResultSet resultSet = null;
        try{
            switch (tipoTabela()){

                case 0: // Produto
                    if(tipoQuery){
                        // Cria o dado no banco com executeUpdate()
                        connection.prepareStatement(query).executeUpdate();
                    }else{
                        // Ler o banco com executeQuere() e retorna o dado
                        resultSet = connection.prepareStatement(query).executeQuery();

                        if (resultSet != null){
                            while (resultSet.next()){
                                // Setar os dados do produto na instância
                                ProdutoModel obj = new ProdutoModel();
                                obj.setId_Produtos(resultSet.getInt("id_Produtos"));
                                obj.setDescricao_produto(resultSet.getString("descricao_produto"));
                                obj.setUnidade(resultSet.getString("unidade"));
                                obj.setEstoque(resultSet.getDouble("estoque"));
                                obj.setQtd_inventario(resultSet.getDouble("qtd_inventario"));
                                obj.setPreco_venda(resultSet.getDouble("preco_venda"));
                                obj.setCodigo_produto(resultSet.getString("codigo_produto"));

                                // Disponibilizar os dados da instâcia(obj) globalmente
                                SingletonProduto.getProduto().setId_Produtos(obj.getId_Produtos());
                                SingletonProduto.getProduto().setDescricao_produto(obj.getDescricao_produto());
                                SingletonProduto.getProduto().setUnidade(obj.getUnidade());
                                SingletonProduto.getProduto().setEstoque(obj.getEstoque());
                                SingletonProduto.getProduto().setQtd_inventario(obj.getQtd_inventario());
                                SingletonProduto.getProduto().setPreco_venda(obj.getPreco_venda());
                                SingletonProduto.getProduto().setCodigo_produto(obj.getCodigo_produto());

                                if(!query.contains("where codigo_produto")){
                                    listaProdutos_db.add(obj);
                                    SingletonProduto.getProduto().setListaProdutos(listaProdutos_db);
                                    obj = null;
                                }
                            }
                        }
                    }
                    break;
                case 1: // Cliente
                    if(tipoQuery){
                        // Cria o dado no banco com executeUpdate()
                        connection.prepareStatement(query).executeUpdate();
                    }else{
                        // Ler o banco com executeQuere() e retorna o dado
                        resultSet = connection.prepareStatement(query).executeQuery();

                        if (resultSet != null){
                            while (resultSet.next()){
                                // Setar os dados do produto na instância
                                ClienteModel obj = new ClienteModel();
                                obj.setId(resultSet.getInt("id"));
                                obj.setNome(resultSet.getString("nome"));
                                obj.setCpf(resultSet.getString("cpf"));
                                obj.setSexo(resultSet.getString("sexo"));
                                obj.setTelefone(resultSet.getString("telefone"));
                                obj.setE_mail(resultSet.getString("e_mail"));
                                obj.setRua(resultSet.getString("rua"));
                                obj.setBairro(resultSet.getString("bairro"));
                                obj.setComplemento(resultSet.getString("complemento"));

                                obj.setStatus(resultSet.getString("status"));
                                obj.setPerfil(resultSet.getString("perfil"));

                                // Disponibilizar os dados da instâcia(obj) globalmente
                                if(obj.getCpf() != null){
                                    if(obj.getPerfil().equalsIgnoreCase("cliente")
                                       || obj.getPerfil().equalsIgnoreCase("MESA")){

                                        SingletonCliente.getCliente().setId(obj.getId());
                                        SingletonCliente.getCliente().setNome(obj.getNome());
                                        SingletonCliente.getCliente().setCpf(obj.getCpf());
                                        SingletonCliente.getCliente().setSexo(obj.getSexo());
                                        SingletonCliente.getCliente().setTelefone(obj.getTelefone());
                                        SingletonCliente.getCliente().setE_mail(obj.getE_mail());
                                        SingletonCliente.getCliente().setRua(obj.getRua());
                                        SingletonCliente.getCliente().setBairro(obj.getBairro());
                                        SingletonCliente.getCliente().setComplemento(obj.getComplemento());

                                        SingletonCliente.getCliente().setStatus(obj.getStatus());
                                        SingletonCliente.getCliente().setPerfil(obj.getPerfil());

                                        // Não permitir guardar dados filtrados com WHERE
                                        //if (this.query.equals("select * from usuario")){
                                        listaClientes_db.add(obj);
                                        SingletonCliente.getCliente().setListaclientes(listaClientes_db);
                                    }

                                    if(!obj.getPerfil().equalsIgnoreCase("cliente")
                                        && !obj.getPerfil().equalsIgnoreCase("MESA")){

                                        obj.setLoginFuncionario(resultSet.getString("login"));
                                        obj.setSenhaFuncionario(resultSet.getString("senha"));
                                        obj.setPerfil(resultSet.getString("perfil"));

                                        SingletonCliente.getCliente().setLoginFuncionario(obj.getLoginFuncionario());
                                        SingletonCliente.getCliente().setSenhaFuncionario(obj.getSenhaFuncionario());
                                        SingletonCliente.getCliente().setPerfil(obj.getPerfil());

                                        // Não permitir guardar dados filtrados com WHERE
                                        //if (this.query.equals("select * from usuario")){
                                        listaFuncionarios_db.add(obj);
                                        SingletonCliente.getCliente().setListaFuncionario(listaFuncionarios_db);
                                    }
                                }
                                obj = null;
                            }
                        }
                    }
                    break;
                case 2: // Cupom item
                    if(tipoQuery){
                        // Cria o dado no banco com executeUpdate()
                        connection.prepareStatement(query).executeUpdate();
                    }else {
                        // Ler o banco com executeQuere() e retorna o dado
                        resultSet = connection.prepareStatement(query).executeQuery();

                        if (resultSet != null){
                            while (resultSet.next()){
                                // Setar os dados do produto na instância
                                Cupom_itemModel obj = new Cupom_itemModel();
                                obj.setId_cupom(resultSet.getInt("id_cupom"));
                                obj.setId_produto(resultSet.getInt("id_produto"));
                                obj.setItem(resultSet.getInt("item"));
                                obj.setFinalizado(resultSet.getInt("finalizado"));
                                obj.setNumero_cupom(resultSet.getInt("numero_cupom"));
                                obj.setComanda(resultSet.getInt("comanda"));
                                obj.setNumeroMesa(resultSet.getInt("numeroMesa"));
//                                obj.setImpresso(resultSet.getInt("impresso"));
                                obj.setQuantidade(resultSet.getDouble("quantidade"));
                                obj.setValor(resultSet.getDouble("valor"));
                                obj.setTotal(resultSet.getDouble("total"));
//                                obj.setTotalPagar(resultSet.getDouble("totalPagar"));
                                obj.setValorTipo(resultSet.getDouble("valorTipo"));
//                                obj.setLucroUNI(resultSet.getDouble("lucroUNI"));
//                                obj.setLucroTTAL(resultSet.getDouble("lucroTTAL"));
//                                obj.setValorPago(resultSet.getDouble("valorPago"));
                                obj.setNomeCliete(resultSet.getString("nomeCliente"));
                                obj.setAtendente(resultSet.getString("atendente"));
                                obj.setUnidade(resultSet.getString("unidade"));
                                obj.setCaixa(resultSet.getString("caixa"));
                                obj.setCod_produto(resultSet.getString("cod_produto"));
                                obj.setData(resultSet.getString("data"));
                                obj.setProduto_descricao(resultSet.getString("produto_descricao"));
                                obj.setStatus(resultSet.getString("status"));
                                obj.setStatusComanda(resultSet.getString("statusComanda"));
                                obj.setTipoMesa(resultSet.getString("tipoMesa"));
                                obj.setFone(resultSet.getString("fone"));
                                obj.setAlterado(resultSet.getInt("alterado"));
                                obj.setObs(resultSet.getString("obs"));
//                                obj.setCod_pedidoFPTO(resultSet.getString("cod_pedidoFPTO"));
                                obj.setSubTotal(resultSet.getDouble("subTotal"));

                                // Disponibilizar os dados da instâcia(obj) globalmente
                                if(obj.getNomeCliete() != null){
                                    //if(obj.getPerfil().equalsIgnoreCase("cliente")){
                                    SingletonCupom_item.getCupom_Item().setId_cupom(obj.getId_cupom());
                                    SingletonCupom_item.getCupom_Item().setId_produto(obj.getId_produto());
                                    SingletonCupom_item.getCupom_Item().setItem(obj.getItem());
                                    SingletonCupom_item.getCupom_Item().setFinalizado(obj.getFinalizado());
                                    SingletonCupom_item.getCupom_Item().setNumero_cupom(obj.getNumero_cupom());
                                    SingletonCupom_item.getCupom_Item().setComanda(obj.getComanda());
                                    SingletonCupom_item.getCupom_Item().setNumeroMesa(obj.getNumeroMesa());
                                    SingletonCupom_item.getCupom_Item().setNumero_cupom(obj.getNumero_cupom());
//                                        SingletonCupom_item.getCupom().setImpresso(obj.getImpresso());
                                    SingletonCupom_item.getCupom_Item().setQuantidade(obj.getQuantidade());
                                    SingletonCupom_item.getCupom_Item().setValor(obj.getValor());
                                    SingletonCupom_item.getCupom_Item().setTotal(obj.getTotal());
//                                        SingletonCupom_item.getCupom().setTotalPagar(obj.getTotalPagar());
                                    SingletonCupom_item.getCupom_Item().setValorTipo(obj.getValorTipo());
//                                        SingletonCupom_item.getCupom().setLucroUNI(obj.getLucroUNI());
//                                        SingletonCupom_item.getCupom().setLucroTTAL(obj.getLucroTTAL());
//                                        SingletonCupom_item.getCupom().setValorPago(obj.getValorPago());
                                    SingletonCupom_item.getCupom_Item().setNomeCliete(obj.getNomeCliete());
                                    SingletonCupom_item.getCupom_Item().setAtendente(obj.getAtendente());
                                    SingletonCupom_item.getCupom_Item().setUnidade(obj.getUnidade());
                                    SingletonCupom_item.getCupom_Item().setCaixa(obj.getCaixa());
                                    SingletonCupom_item.getCupom_Item().setCod_produto(obj.getCod_produto());
                                    SingletonCupom_item.getCupom_Item().setData(obj.getData());
                                    SingletonCupom_item.getCupom_Item().setProduto_descricao(obj.getProduto_descricao());
                                    SingletonCupom_item.getCupom_Item().setStatus(obj.getStatus());
                                    SingletonCupom_item.getCupom_Item().setStatusComanda(obj.getStatusComanda());
                                    SingletonCupom_item.getCupom_Item().setComanda(obj.getComanda());
                                    SingletonCupom_item.getCupom_Item().setTipoMesa(obj.getTipoMesa());
                                    SingletonCupom_item.getCupom_Item().setFone(obj.getFone());
                                    SingletonCupom_item.getCupom_Item().setAlterado(obj.getAlterado());
                                    SingletonCupom_item.getCupom_Item().setObs(obj.getObs());
//                                        SingletonCupom_item.getCupom().setCod_pedidoFPTO(obj.getCod_pedidoFPTO());
                                    SingletonCupom_item.getCupom_Item().setSubTotal(obj.getSubTotal());

                                    // Não permitir guardar dados filtrados com WHERE
                                    //if (this.query.equals("select * from usuario")){
                                    listaPedidos_db.add(obj);
                                    SingletonCupom_item.getCupom_Item().setListaPedido(listaPedidos_db);
                                }
                                obj = null;
                            }
                        }
                    }

                    break;
//                case 3:
//                    if(tipoQuery){
//                        // Cria o dado no banco com executeUpdate()
//                        connection.prepareStatement(query).executeUpdate();
//                    }else{
//                        // Ler o banco com executeQuere() e retorna o dado
//                        resultSet = connection.prepareStatement(query).executeQuery();
//
//                        if (resultSet != null){
//                            while (resultSet.next()){
//
//                                Empresa empresa = new Empresa();
//                                empresa.setId(resultSet.getInt("id"));
//                                empresa.setRazao(resultSet.getString("razao"));
//                                empresa.setFantasia(resultSet.getString("fantasia"));
//                                empresa.setCnpj(resultSet.getString("cnpj"));
//                                empresa.setInscricao(resultSet.getString("inscricao"));
//                                empresa.setRua(resultSet.getString("rua"));
//                                empresa.setNumero(resultSet.getString("numero"));
//                                empresa.setCep(resultSet.getString("cep"));
//                                empresa.setComplemento(resultSet.getString("complemento"));
//                                empresa.setBairro(resultSet.getString("bairro"));
//                                empresa.setCidade(resultSet.getString("cidade"));
//                                empresa.setEstado(resultSet.getString("estado"));
//                                empresa.setFone1(resultSet.getString("fone1"));
//                                empresa.setFone2(resultSet.getString("fone2"));
//                                empresa.setE_mail(resultSet.getString("e_mail"));
//                                empresa.setBanco(resultSet.getString("banco"));
//                                empresa.setQtd_login(resultSet.getInt("qtd_login"));
//                                empresa.setData_aviso(resultSet.getString("data_aviso"));
//                                empresa.setData_vencimento(resultSet.getString("data_vencimento"));
//                                empresa.setStatus(resultSet.getString("status"));
//                                empresa.setAtividade(resultSet.getString("atividade"));
//                                empresa.setLogin(resultSet.getString("login"));
//                                empresa.setTipo_pacote(resultSet.getString("tipoconexao"));
//                                empresa.setTipo_pacote(resultSet.getString("tipo_pacote"));
//                                empresa.setUrl(resultSet.getString("url"));
//                                empresa.setBloqueio_auto(resultSet.getInt("bloqueio_auto"));
//                                empresa.setTipoLicenca(resultSet.getInt("tipoLicenca"));
//                                empresa.setAlt(resultSet.getInt("alt"));
//                                empresa.setData_implementacao(resultSet.getString("data_implementacao"));
//                                empresa.setRede_social(resultSet.getString("rede_social"));
//                                empresa.setValor_mensal(resultSet.getDouble("valor_mensal"));
//                                empresa.setRg(resultSet.getString("rg"));
//                                empresa.setCpf(resultSet.getString("cpf"));
//                                empresa.setPonto_ref(resultSet.getString("ponto_ref"));
//                                empresa.setProjeto(resultSet.getString("projeto"));
//                                empresa.setObs(resultSet.getString("obs"));
//                                empresa.setVersao(resultSet.getString("versao"));
//                                empresa.setStatus(resultSet.getString("status"));
//                                empresa.setStatus_financa(resultSet.getInt("financa"));
//                                empresa.setIp(resultSet.getString("ip"));
//                                empresa.setDataNasc(resultSet.getString("dataNasc"));
//                                empresa.setUltimoAcesso(resultSet.getString("ultimoAcesso"));
//                                empresa.setMsgCliente(resultSet.getString("msgCliente"));
//                                empresa.setOff(resultSet.getString("off"));
//                                empresa.setMei(resultSet.getInt("mei"));
//                                empresa.setQtdCaixa(resultSet.getInt("qtdCaixa"));
//                                // Disponibilizar os dados da instâcia(obj) globalmente
//                                SingletonEmpresa.getEmpresa().setId(empresa.getId());
//                                SingletonEmpresa.getEmpresa().setRazao(empresa.getRazao());
//                                SingletonEmpresa.getEmpresa().setFantasia(empresa.getFantasia());
//                                SingletonEmpresa.getEmpresa().setCnpj(empresa.getCnpj());
//                                SingletonEmpresa.getEmpresa().setInscricao(empresa.getInscricao());
//                                SingletonEmpresa.getEmpresa().setRua(empresa.getRua());
//                                SingletonEmpresa.getEmpresa().setNumero(empresa.getNumero());
//                                SingletonEmpresa.getEmpresa().setCep(empresa.getCep());
//                                SingletonEmpresa.getEmpresa().setComplemento(empresa.getComplemento());
//                                SingletonEmpresa.getEmpresa().setBairro(empresa.getBairro());
//                                SingletonEmpresa.getEmpresa().setCidade(empresa.getCidade());
//                                SingletonEmpresa.getEmpresa().setEstado(empresa.getEstado());
//                                SingletonEmpresa.getEmpresa().setFone1(empresa.getFone1());
//                                SingletonEmpresa.getEmpresa().setFone2(empresa.getFone2());
//                                SingletonEmpresa.getEmpresa().setE_mail(empresa.getE_mail());
//                                SingletonEmpresa.getEmpresa().setBanco(empresa.getBanco());
//                                SingletonEmpresa.getEmpresa().setQtd_login(empresa.getQtd_login());
//                                SingletonEmpresa.getEmpresa().setData_aviso(empresa.getData_aviso());
//                                SingletonEmpresa.getEmpresa().setData_vencimento(empresa.getData_vencimento());
//                                SingletonEmpresa.getEmpresa().setStatus(empresa.getStatus());
//                                SingletonEmpresa.getEmpresa().setAtividade(empresa.getAtividade());
//                                SingletonEmpresa.getEmpresa().setLogin(empresa.getLogin());
//                                SingletonEmpresa.getEmpresa().setTipo_pacote(empresa.getTipo_pacote());
//                                SingletonEmpresa.getEmpresa().setUrl(empresa.getUrl());
//                                SingletonEmpresa.getEmpresa().setBloqueio_auto(empresa.getBloqueio_auto());
//                                SingletonEmpresa.getEmpresa().setTipoLicenca(empresa.getTipoLicenca());
//                                SingletonEmpresa.getEmpresa().setAlt(empresa.getAlt());
//                                SingletonEmpresa.getEmpresa().setData_implementacao(empresa.getData_implementacao());
//                                SingletonEmpresa.getEmpresa().setRede_social(empresa.getRede_social());
//                                SingletonEmpresa.getEmpresa().setValor_mensal(empresa.getValor_mensal());
//                                SingletonEmpresa.getEmpresa().setRg(empresa.getRg());
//                                SingletonEmpresa.getEmpresa().setCpf(empresa.getCpf());
//                                SingletonEmpresa.getEmpresa().setPonto_ref(empresa.getPonto_ref());
//                                SingletonEmpresa.getEmpresa().setProjeto(empresa.getProjeto());
//                                SingletonEmpresa.getEmpresa().setObs(empresa.getObs());
//                                SingletonEmpresa.getEmpresa().setVersao(empresa.getVersao());
//                                SingletonEmpresa.getEmpresa().setStatus(empresa.getStatus());
//                                SingletonEmpresa.getEmpresa().setStatus_financa(empresa.getStatus_financa());
//                                SingletonEmpresa.getEmpresa().setIp(empresa.getIp());
//                                SingletonEmpresa.getEmpresa().setDataNasc(empresa.getDataNasc());
//                                SingletonEmpresa.getEmpresa().setUltimoAcesso(empresa.getUltimoAcesso());
//                                SingletonEmpresa.getEmpresa().setMsgCliente(empresa.getMsgCliente());
//                                SingletonEmpresa.getEmpresa().setOff(empresa.getOff());
//                                SingletonEmpresa.getEmpresa().setMei(empresa.getMei());
//                                SingletonEmpresa.getEmpresa().setQtdCaixa(empresa.getQtdCaixa());
//
//                                listaEmpresa.add(empresa);
//                                SingletonEmpresa.getEmpresa().setListaEmpresas(listaEmpresa);
//                                empresa = null;
//                            }
//                        }
//                    }
//                    break;
                case 4: // Validade
                    if(tipoQuery){
                        // Cria o dado no banco com executeUpdate()
                        connection.prepareStatement(query).executeUpdate();
                    }else {
                        // Ler o banco com executeQuere() e retorna o dado
                        resultSet = connection.prepareStatement(query).executeQuery();

                        if (resultSet != null){
                            while (resultSet.next()){
                                // Setar os dados da validade na instância
                                ValidadeModel obj = new ValidadeModel();
                                obj.setIdValidade(resultSet.getInt("id"));
                                obj.setCodigo(resultSet.getString("codigo"));
                                obj.setDescricao(resultSet.getString("descricao"));
                                obj.setQtdEstoque(resultSet.getDouble("qtd"));
                                obj.setDataValidade(resultSet.getString("data"));
                                obj.setStatus(resultSet.getInt("status"));
                                obj.setAlterado(resultSet.getInt("alterado"));
                                obj.setChave(resultSet.getString("chave"));

                                // Disponibilizar os dados da instâcia(obj) globalmente
                                if(obj.getDescricao() != null){
                                    SingletonValidade.getValidade().setIdValidade(obj.getIdValidade());
                                    SingletonValidade.getValidade().setCodigo(obj.getCodigo());
                                    SingletonValidade.getValidade().setDescricao(obj.getDescricao());
                                    SingletonValidade.getValidade().setQtdEstoque(obj.getQtdEstoque());
                                    SingletonValidade.getValidade().setDataValidade(obj.getDataValidade());
                                    SingletonValidade.getValidade().setStatus(obj.getStatus());
                                    SingletonValidade.getValidade().setAlterado(obj.getAlterado());
                                    SingletonValidade.getValidade().setChave(obj.getChave());

                                    // Não permitir guardar dados filtrados com WHERE
                                    //if (this.query.equals("select * from usuario")){
                                    listaValidade_db.add(obj);
                                    SingletonValidade.getValidade().setListaValidade(listaValidade_db);
                                }
                                obj = null;
                            }
                        }
                    }
                case 5:
                    if(tipoQuery){
                        // Cria o dado no banco com executeUpdate()
                        connection.prepareStatement(query).executeUpdate();
                    }else{
                        // Ler o banco com executeQuere() e retorna o dado
                        resultSet = connection.prepareStatement(query).executeQuery();

                        if (resultSet != null){
                            while (resultSet.next()){

                                Empresa empresa = new Empresa();
                                empresa.setId(resultSet.getInt("id"));
                                empresa.setRazao(resultSet.getString("razao"));
                                empresa.setFantasia(resultSet.getString("fantasia"));
                                empresa.setCnpj(resultSet.getString("cnpj"));
                                empresa.setInscricao(resultSet.getString("inscricao"));
                                empresa.setRua(resultSet.getString("rua"));
                                empresa.setNumero(resultSet.getString("numero"));
                                empresa.setCep(resultSet.getString("cep"));
                                empresa.setComplemento(resultSet.getString("complemento"));
                                empresa.setBairro(resultSet.getString("bairro"));
                                empresa.setCidade(resultSet.getString("cidade"));
                                empresa.setEstado(resultSet.getString("estado"));
                                empresa.setFone1(resultSet.getString("fone1"));
                                empresa.setFone2(resultSet.getString("fone2"));
                                empresa.setE_mail(resultSet.getString("e_mail"));
                                empresa.setBanco(resultSet.getString("banco"));
                                empresa.setQtd_login(resultSet.getInt("qtd_login"));
                                empresa.setData_aviso(resultSet.getString("data_aviso"));
                                empresa.setData_vencimento(resultSet.getString("data_vencimento"));
                                empresa.setStatus(resultSet.getString("status"));
                                empresa.setAtividade(resultSet.getString("atividade"));
                                empresa.setLogin(resultSet.getString("login"));
                                empresa.setTipo_pacote(resultSet.getString("tipoconexao"));
                                empresa.setTipo_pacote(resultSet.getString("tipo_pacote"));
                                empresa.setUrl(resultSet.getString("url"));
                                empresa.setBloqueio_auto(resultSet.getInt("bloqueio_auto"));
                                empresa.setTipoLicenca(resultSet.getInt("tipoLicenca"));
                                empresa.setAlt(resultSet.getInt("alt"));
                                empresa.setData_implementacao(resultSet.getString("data_implementacao"));
                                empresa.setRede_social(resultSet.getString("rede_social"));
                                empresa.setValor_mensal(resultSet.getDouble("valor_mensal"));
                                empresa.setRg(resultSet.getString("rg"));
                                empresa.setCpf(resultSet.getString("cpf"));
                                empresa.setPonto_ref(resultSet.getString("ponto_ref"));
                                empresa.setProjeto(resultSet.getString("projeto"));
                                empresa.setObs(resultSet.getString("obs"));
                                empresa.setVersao(resultSet.getString("versao"));
                                empresa.setStatus(resultSet.getString("status"));
                                empresa.setStatus_financa(resultSet.getInt("financa"));
                                empresa.setIp(resultSet.getString("ip"));
                                empresa.setDataNasc(resultSet.getString("dataNasc"));
                                empresa.setUltimoAcesso(resultSet.getString("ultimoAcesso"));
                                empresa.setMsgCliente(resultSet.getString("msgCliente"));
                                empresa.setOff(resultSet.getString("off"));
                                empresa.setMei(resultSet.getInt("mei"));
                                empresa.setQtdCaixa(resultSet.getInt("qtdCaixa"));
                                // Disponibilizar os dados da instâcia(obj) globalmente
                                SingletonEmpresa.getEmpresa().setId(empresa.getId());
                                SingletonEmpresa.getEmpresa().setRazao(empresa.getRazao());
                                SingletonEmpresa.getEmpresa().setFantasia(empresa.getFantasia());
                                SingletonEmpresa.getEmpresa().setCnpj(empresa.getCnpj());
                                SingletonEmpresa.getEmpresa().setInscricao(empresa.getInscricao());
                                SingletonEmpresa.getEmpresa().setRua(empresa.getRua());
                                SingletonEmpresa.getEmpresa().setNumero(empresa.getNumero());
                                SingletonEmpresa.getEmpresa().setCep(empresa.getCep());
                                SingletonEmpresa.getEmpresa().setComplemento(empresa.getComplemento());
                                SingletonEmpresa.getEmpresa().setBairro(empresa.getBairro());
                                SingletonEmpresa.getEmpresa().setCidade(empresa.getCidade());
                                SingletonEmpresa.getEmpresa().setEstado(empresa.getEstado());
                                SingletonEmpresa.getEmpresa().setFone1(empresa.getFone1());
                                SingletonEmpresa.getEmpresa().setFone2(empresa.getFone2());
                                SingletonEmpresa.getEmpresa().setE_mail(empresa.getE_mail());
                                SingletonEmpresa.getEmpresa().setBanco(empresa.getBanco());
                                SingletonEmpresa.getEmpresa().setQtd_login(empresa.getQtd_login());
                                SingletonEmpresa.getEmpresa().setData_aviso(empresa.getData_aviso());
                                SingletonEmpresa.getEmpresa().setData_vencimento(empresa.getData_vencimento());
                                SingletonEmpresa.getEmpresa().setStatus(empresa.getStatus());
                                SingletonEmpresa.getEmpresa().setAtividade(empresa.getAtividade());
                                SingletonEmpresa.getEmpresa().setLogin(empresa.getLogin());
                                SingletonEmpresa.getEmpresa().setTipo_pacote(empresa.getTipo_pacote());
                                SingletonEmpresa.getEmpresa().setUrl(empresa.getUrl());
                                SingletonEmpresa.getEmpresa().setBloqueio_auto(empresa.getBloqueio_auto());
                                SingletonEmpresa.getEmpresa().setTipoLicenca(empresa.getTipoLicenca());
                                SingletonEmpresa.getEmpresa().setAlt(empresa.getAlt());
                                SingletonEmpresa.getEmpresa().setData_implementacao(empresa.getData_implementacao());
                                SingletonEmpresa.getEmpresa().setRede_social(empresa.getRede_social());
                                SingletonEmpresa.getEmpresa().setValor_mensal(empresa.getValor_mensal());
                                SingletonEmpresa.getEmpresa().setRg(empresa.getRg());
                                SingletonEmpresa.getEmpresa().setCpf(empresa.getCpf());
                                SingletonEmpresa.getEmpresa().setPonto_ref(empresa.getPonto_ref());
                                SingletonEmpresa.getEmpresa().setProjeto(empresa.getProjeto());
                                SingletonEmpresa.getEmpresa().setObs(empresa.getObs());
                                SingletonEmpresa.getEmpresa().setVersao(empresa.getVersao());
                                SingletonEmpresa.getEmpresa().setStatus(empresa.getStatus());
                                SingletonEmpresa.getEmpresa().setStatus_financa(empresa.getStatus_financa());
                                SingletonEmpresa.getEmpresa().setIp(empresa.getIp());
                                SingletonEmpresa.getEmpresa().setDataNasc(empresa.getDataNasc());
                                SingletonEmpresa.getEmpresa().setUltimoAcesso(empresa.getUltimoAcesso());
                                SingletonEmpresa.getEmpresa().setMsgCliente(empresa.getMsgCliente());
                                SingletonEmpresa.getEmpresa().setOff(empresa.getOff());
                                SingletonEmpresa.getEmpresa().setMei(empresa.getMei());
                                SingletonEmpresa.getEmpresa().setQtdCaixa(empresa.getQtdCaixa());

                                listaEmpresa.add(empresa);
                                SingletonEmpresa.getEmpresa().setListaEmpresas(listaEmpresa);
                                empresa = null;
                            }
                        }
                    }
                    break;

                default:    // Lucros
                    if(tipoQuery){
                        // Cria o dado no banco com executeUpdate()
                        connection.prepareStatement(query).executeUpdate();
                    }else {
                        // Ler o banco com executeQuere() e retorna o dado
                        resultSet = connection.prepareStatement(query).executeQuery();

                        if (resultSet != null){
                            while (resultSet.next()){

                                Cupom cup = new Cupom();

                                cup.setForma_pagamento(resultSet.getString("forma_pagamento"));
                                cup.setSumSubtotal(resultSet.getDouble("valor"));

                                // Disponibilizar os dados da instâcia(obj) globalmente
                                if(cup.getForma_pagamento() != null){
                                    SingletonCupom.getCupom().setForma_pagamento(cup.getForma_pagamento());
                                    SingletonCupom.getCupom().setSumSubtotal(cup.getSumSubtotal());

                                    listaCupom_db.add(cup);

                                }
                                SingletonCupom.getCupom().setListaCupom(listaCupom_db);
                                cup = null;
                            }
                        }
                    }
                    break;
            }

        }catch (Exception e){

        }finally {
            try {
                connection.close();
            }catch (Exception ex){

            }
        }
        return resultSet;
    }

    public int tipoTabela(){
        int tipo =0;
        if(this.tabela.equalsIgnoreCase("produtos")){
            tipo=0;
        }else{
            if(this.tabela.equalsIgnoreCase("usuario")){
                tipo=1;
            }else{
                if(this.tabela.equalsIgnoreCase("cupom_item")){
                    tipo=2;
                }else{
                    if(this.tabela.equalsIgnoreCase("cupom")){
                        tipo=3;
                    }else{
                        if(this.tabela.equalsIgnoreCase("validade")){
                            tipo=4;
                        }else{
                            if(this.tabela.equalsIgnoreCase("empresa")){
                                tipo=5;
                            }
                        }
                    }
                }
            }
        }
        return tipo;
    }
}
