package com.zsisistemas.zsiempresa.controller;

import android.content.Context;
import android.content.SharedPreferences;

import com.zsisistemas.zsiempresa.dao.ProdutoDAO_Mysql;
import com.zsisistemas.zsiempresa.dao.ProdutoDAO_Sqlite;
import com.zsisistemas.zsiempresa.model.ClienteModel;
import com.zsisistemas.zsiempresa.model.Cupom_itemModel;
import com.zsisistemas.zsiempresa.model.Empresa;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.ValidadeModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom_item;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;

import java.util.ArrayList;

import static com.zsisistemas.zsiempresa.controller.Util.dataAtualAmericana;

public class Controller_Conexao {

    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";

    public Controller_Conexao() {

    }

    public int getTipoConexao(){
        int tipoConexao = 0;
        String tipo = SingletonConfiguracoes.getConfig().getConnection();
        if(tipo != null){
            if(!tipo.equals("")){
               tipoConexao = Util.validaInteiro(tipo);
            }
        }
        return tipoConexao;
    }

    public void salvar(Context context, ProdutoModel produtoModel) {
        if (getTipoConexao() == 3) {
            ProdutoDAO_Sqlite produtoDAOSqlite = new ProdutoDAO_Sqlite(context);

            produtoDAOSqlite.inserir(produtoModel);
        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.inserir(produtoModel);
        }
    }

    public void importToSqlite(Context context, ProdutoModel produtoModel) {
            ProdutoDAO_Sqlite produtoDAOSqlite = new ProdutoDAO_Sqlite(context);
            produtoDAOSqlite.importToSqlite(produtoModel);
    }

    public void salvarValidade(Context context, ValidadeModel validadeModel) {
        if (getTipoConexao() == 3) {
        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.inserirValidade(validadeModel);
        }
    }

    public void salvarCliente(Context context, ClienteModel clienteModel) {
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.inserirCliente(clienteModel);
        }
    }

    public void salvarPedidoCupom_item(Context context, Cupom_itemModel cupom_itemModel){
        ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
        produtoDAO_mysql.inserirCupom_item(cupom_itemModel);
    }


    public void salvarPedidoCarrinhoTemporariamente(Context context, ProdutoModel produtoModel){
        ProdutoDAO_Sqlite produtoDAOSqlite = new ProdutoDAO_Sqlite(context);
        produtoDAOSqlite.inserir(produtoModel);
    }

    public void listaProdutosTemporarioCarrinho(Context context){

        ProdutoDAO_Sqlite produtoDAOSqlite = new ProdutoDAO_Sqlite(context);

        ArrayList<ProdutoModel> lista = new ArrayList<>();

        lista =(ArrayList<ProdutoModel>) produtoDAOSqlite.obterTodos();

        SingletonProduto.getProduto().setListaCarrinho(lista);

    }

    public void listarProdutos(Context context) {
        if (getTipoConexao() == 3) {

            ProdutoDAO_Sqlite produtoDAOSqlite = new ProdutoDAO_Sqlite(context);
            ArrayList<ProdutoModel> lista = new ArrayList<>();
            lista =(ArrayList<ProdutoModel>) produtoDAOSqlite.obterTodos();
            SingletonProduto.getProduto().setListaProdutos(lista);
            int tam = lista.size();

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listarProdutos();
        }
    }

    public void listarValidades(Context context) {
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listarValidades();
        }
    }

    public void listarCupom(Context context) {
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listarPedidos();
        }
    }

    public void listarCupomFun(Context context, String loginFun) {
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listarPedidosFun(loginFun);
        }
    }

    public void listarFormaPagamento(Context context) {

        String dataI;
        String dataF;

        if(SingletonConfiguracoes.getConfig().getDataInicio() == null){
            dataI = dataAtualAmericana();
        } else {
            dataI = SingletonConfiguracoes.getConfig().getDataInicio();
        }

        if(SingletonConfiguracoes.getConfig().getDataFim() == null){
            dataF = dataAtualAmericana();
        } else {
            dataF = SingletonConfiguracoes.getConfig().getDataFim();
        }

        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listaFormaPagamento(dataI, dataF);
        }
    }

    public void listarEmpresas(Context context) {
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listarClientesEmpresa();
        }
    }

    public void listarPagamentoFun(Context context, String loginFun) {

        String dataI;
        String dataF;

        if(SingletonConfiguracoes.getConfig().getDataInicio() == null){
            dataI = dataAtualAmericana();
        } else {
            dataI = SingletonConfiguracoes.getConfig().getDataInicio();
        }

        if(SingletonConfiguracoes.getConfig().getDataFim() == null){
            dataF = dataAtualAmericana();
        } else {
            dataF = SingletonConfiguracoes.getConfig().getDataFim();
        }

        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listaPagamentoFun(dataI, dataF, loginFun);
        }
    }

    public void cancelaPedido(Context context,int comanda) {
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.cancelaPedido(comanda);
        }
    }

    public void listaItensPedidos(Context context, Cupom_itemModel cupom_itemModel) {
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listaItensPedido(cupom_itemModel);

            ArrayList<Cupom_itemModel> arrayList = new ArrayList<>();
            arrayList = SingletonCupom_item.getCupom_Item().getListaPedido();
            if(arrayList != null){
                if(arrayList.size()>0){
                    Controller_Conexao controller_conexao = new Controller_Conexao();
                   // limparBanco(context);//limpa a tabela
                    for (Cupom_itemModel cupomItem:arrayList) {

                        ProdutoModel produtoModel = new ProdutoModel();

                        produtoModel.setCodigo_produto(cupomItem.getCod_produto());
                        produtoModel.setDescricao_produto(cupomItem.getProduto_descricao());
                        produtoModel.setPreco_venda(cupomItem.getValor());
                        produtoModel.setQuantidadeProdutos(cupomItem.getQuantidade());
                        produtoModel.setStatus(1);
                        produtoModel.setImpressao(1);

                        SharedPreferences preferencesPedido = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                        SharedPreferences.Editor editor = preferencesPedido.edit();

                        String pedido = "true";
                        String obs = cupomItem.getObs();

                        editor.putString("pedido", pedido);
                        editor.putString("obs", obs);

                        editor.apply();

                        SingletonProduto.getProduto().setCheckPedido(Boolean.parseBoolean(pedido));
                        SingletonProduto.getProduto().setObs(obs);

                        controller_conexao.salvarPedidoCarrinhoTemporariamente(context,produtoModel);
                        // DESSA FORMA O CARRINHO VAI ESTAR COM OS ITENS DO PEDIDO
                    }
                }
            }
        }
    }

    public void verificarStatusPedido(Context context, int numeroMesa) {
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.verificarStatusPedido(numeroMesa);

            ArrayList<Cupom_itemModel> arrayList = new ArrayList<>();
            arrayList.clear();
            arrayList = SingletonCupom_item.getCupom_Item().getListaPedido();

            // Se lista vir preenchida é pedido cancelado.
            if(arrayList != null){
                if(arrayList.size()>0){
                    // cupom_item com status CANC.

                    SingletonCupom_item.getCupom_Item().setStatusPedido(false);

                } else {
                    SingletonCupom_item.getCupom_Item().setStatusPedido(true);
                }
            } else {
                SingletonCupom_item.getCupom_Item().setStatusPedido(true);
            }
        }
    }

    public void localizaProdutoPorCodigo(Context context,String codigo_produto) {

        SingletonProduto.getProduto().setId_Produtos(null);
        SingletonProduto.getProduto().setDescricao_produto(null);
        SingletonProduto.getProduto().setUnidade(null);
        SingletonProduto.getProduto().setEstoque(0.0);
        SingletonProduto.getProduto().setQtd_inventario(0.0);
        SingletonProduto.getProduto().setPreco_venda(0.0);
        SingletonProduto.getProduto().setCodigo_produto(null);

        if (getTipoConexao() == 3) {
            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
            produtoDAO_sqlite.localizaProduto(codigo_produto);
        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.selectProdutoPorCodigo(codigo_produto);
        }
    }

    public void localizaProdutoPorStatus(Context context) {
        if (getTipoConexao() == 3) {
            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
            produtoDAO_sqlite.localizaProdutoStatus();
        } else {
            // TODO criar controller para produtos coletados em nuvem[?]
        }
    }

    public void localizaProdutoColetar(Context context) {
        if (getTipoConexao() == 3) {
            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
            produtoDAO_sqlite.localizaProdutoColetar();
        }
    }

    public void localizaProdutoColetados(Context context) {
        if (getTipoConexao() == 3) {
            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
            produtoDAO_sqlite.localizaProdutoColetado();
        }
    }

    public void inserirEtiquetaProduto(Context context,ProdutoModel produtoModel) {
        if (getTipoConexao() == 3) {
//            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
//            produtoDAO_sqlite.atualizarProdutoId(produtoModel);
        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.atualizaColetaEtiqueta(produtoModel);


        }
    }

    public void atualizaProdutoPorCodigo(Context context,ProdutoModel produtoModel) {
        if (getTipoConexao() == 3) {
//            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
//            produtoDAO_sqlite.atualizarProdutoId(produtoModel);
        } else {
//            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
//            produtoDAO_mysql.atualizarPrduto(produtoModel);

            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
            produtoDAO_sqlite.atualizarProdutoId(produtoModel);
        }
    }

    public void atualizaCupomItem(Context context,Cupom_itemModel cupom_itemModel) {
        if (getTipoConexao() == 3) {
//            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
//            produtoDAO_sqlite.atualizarProdutoId(cupom_itemModel);
        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.atualizarCupomItem(cupom_itemModel);
        }
    }

    public void atualizaEstoque(Context context,ProdutoModel produtoModel) {
        if (getTipoConexao() == 3) {
            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
            produtoDAO_sqlite.atualizarEstoque(produtoModel);
        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.atualizarEstoque(produtoModel);
        }
    }

    public void atualizaQtdInventario(Context context,ProdutoModel produtoModel, String campo) {
        if (getTipoConexao() == 3) {
            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
            produtoDAO_sqlite.atualizarQtdInventario(produtoModel);
        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.atualizarQtdInvantario(produtoModel, campo);
        }
    }

    public void limparBanco(Context context) {
        if (getTipoConexao() == 3) {
            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
            produtoDAO_sqlite.excluirTodos();
        } else {

        }
    }

    public void limparPorId(Context context, ProdutoModel p) {
        if (getTipoConexao() == 3) {
            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
            produtoDAO_sqlite.excluir(p);
        } else {

        }
    }

    public void zerarContagem(Context context) {
        if (getTipoConexao() == 3) {
            ProdutoDAO_Sqlite produtoDAO_sqlite = new ProdutoDAO_Sqlite(context);
            produtoDAO_sqlite.atualizarInventario();
        } else {

        }
    }

    public void listaChavesCliete(Context context){
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listarChavesClientes();
        }
    }

    public void listaCliete(Context context){
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listarClientes("");
        }
    }

    public void listaMesa(Context context){
        if (getTipoConexao() == 3) {

        } else {
            //TODO criar metodo na DAO com query pra chamar so as mesas
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.listarClientes("MESA");
        }
    }

    public void atualizarCliete(ClienteModel clienteModel, int id){
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.atualizarCliente(clienteModel, id);
        }
    }

    public void atualizarValidade(ValidadeModel validadeModel, String chave){
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.atualizarValidade(validadeModel, chave);
        }
    }

    public void inativarVencimento(String chave){
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.inativarVencimento(chave);
        }
    }

    public void liberarAcessoEmpresa(Empresa empresa){
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.pagar(empresa);
        }
    }

    public void alterarVencimntoEmpresa(Empresa empresa){
        if (getTipoConexao() == 3) {

        } else {
            ProdutoDAO_Mysql produtoDAO_mysql = new ProdutoDAO_Mysql();
            produtoDAO_mysql.alterarVencimento(empresa);
        }
    }
}
