package com.zsisistemas.zsiempresa.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;

import com.zsisistemas.zsiempresa.model.Chave;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom_item;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;
import com.zsisistemas.zsiempresa.model.singleton.SingletonValidade;
import com.zsisistemas.zsiempresa.view.activities.ConnectionDialog;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    public static String maisUMmes(String dataAtual) {
        String dataAjustada = "";

        if (dataAtual != null) {
            String[] dataFatiada = new String[3];

            dataFatiada = dataAtual.split("-");
            String mes = dataFatiada[1];
            int mesA = Integer.parseInt(mes);

            if (mesA == 12) {
                mesA = 1;
            } else {
                mesA++;
            }

            String mesAtualizado = String.valueOf(mesA);
            dataAjustada = dataFatiada[0] + "-" + mesAtualizado + "-" + dataFatiada[2];
        }

        return dataAjustada;
    }

    public static void limpaSingletons(){
        SingletonCupom_item.getCupom_Item().setListaPedido(null);
        SingletonCupom.getCupom().setListaCupom(null);
        SingletonValidade.getValidade().setListaValidade(null);
        SingletonCliente.getCliente().setListaclientes(null);
        SingletonProduto.getProduto().setListaProdutos(null);
    }

    public static void restauraSharedPreferences(Context context,String ARQUIVO_PREFERENCIA){
        SharedPreferences preferencesConn = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        SharedPreferences.Editor editor = preferencesConn.edit();

        String conn = "3";
        editor.putString("conn", conn);
        editor.apply();
        SingletonConfiguracoes.getConfig().setConnection(conn);

        String ip = "127.0.0.1";
        editor.putString("ip", ip);
        editor.commit();
        SingletonConfiguracoes.getConfig().setIp(ip);

        String nome = null;
        editor.putString("nome", nome);
        editor.commit();
        SingletonConfiguracoes.getConfig().setNomeBanco(nome);

        String root = null;
        editor.putString("root", root);
        editor.apply();
        SingletonConfiguracoes.getConfig().setRoot(root);

        String chave = null;
        editor.putString("chave", chave);
        editor.apply();
        SingletonConfiguracoes.getConfig().setChave(chave);

        String perfilFun = null;
        editor.putString("perfilFun", perfilFun);
        editor.apply();
        SingletonCliente.getCliente().setPerfil(perfilFun);

        String lembrarFun = null;
        editor.putString("lembrarFun", lembrarFun);
        editor.apply();
        SingletonConfiguracoes.getConfig().setListaLembrarLogin(lembrarFun);

        String loginFun = null;
        editor.putString("loginFun", loginFun);
        editor.apply();
        SingletonCliente.getCliente().setLoginFuncionario(loginFun);

        String senhaFun = null;
        editor.putString("senhaFun", senhaFun);
        editor.apply();
        SingletonCliente.getCliente().setSenhaFuncionario(senhaFun);

        String validade = null;
        editor.putString("validade", validade);
        editor.apply();
        SingletonConfiguracoes.getConfig().setValidade(validade);

        String nomeUsuario = null;
        editor.putString("nomeUsuario", nomeUsuario);
        editor.apply();
        SingletonCliente.getCliente().setNome(nomeUsuario);

        String chaveId = null;
        editor.putString("chaveId", chaveId);
        editor.apply();
        SingletonCliente.getCliente().setChaveId(chaveId);
    }

    public static void insereSharedPreferences(Context context, String ARQUIVO_PREFERENCIA, Chave ch){
        SharedPreferences preferencesConn = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        SharedPreferences.Editor editor = preferencesConn.edit();
        String conn = "3";
        editor.putString("conn", conn);
        editor.apply();
        SingletonConfiguracoes.getConfig().setConnection(conn);
        String ip = "127.0.0.1";
        editor.putString("ip", ip);
        editor.commit();
        SingletonConfiguracoes.getConfig().setIp(ip);
        String nome = "";
        editor.putString("nome", nome);
        editor.commit();
        SingletonConfiguracoes.getConfig().setNomeBanco(nome);
        String root = "";
        editor.putString("root", root);
        editor.apply();
        SingletonConfiguracoes.getConfig().setRoot(root);
        String chave = "";
        editor.putString("chave", chave);
        editor.apply();
        SingletonConfiguracoes.getConfig().setChave(chave);

    }

    public static void mensagemToast(Context context, String mensagem) {
        Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show();
    }

    public static boolean validaCampos(ProdutoModel produtoModel) {
        if (produtoModel.getCodigo_produto() != null) {
            if (!produtoModel.getDescricao_produto().equals("")) {
                return true;
            }
        }
        return false;
    }

    public static double converteStringDouble(String valorRecebido) {
        double valorConvertido = 0;
        try {
            valorConvertido = Double.parseDouble(valorRecebido);
        } catch (Exception e) {
            valorConvertido = 0;
        }

        return valorConvertido;
    }

    public static int validaInteiro(String valorRecebido) {
        int valorConvertido = 0;
        try {
            valorConvertido = Integer.parseInt(valorRecebido);
        } catch (Exception e) {
            valorConvertido = 3;
        }

        return valorConvertido;
    }

    public static int verificaDiasVencimento(String dataBanco) {    // dd-mm-yyyy
        String[] dataInvertida = new String[3];

        dataInvertida = dataBanco.split("-");   // dd-mm-yyyy

        String anoBanco = dataInvertida[2];
        String mesBanco = dataInvertida[1];
        String diaBanco = dataInvertida[0];

        int yyyyBanco = Integer.parseInt(anoBanco); // Dia
        int MMBanco = Integer.parseInt(mesBanco);   // Mês
        int ddBanco = Integer.parseInt(diaBanco);   // Ano

        String[] dataAtualInvertida = new String[3];

        dataAtualInvertida = dataAtual().split("-");

        String anoAtual = dataAtualInvertida[2];
        String mesAtual = dataAtualInvertida[1];
        String diaAtual = dataAtualInvertida[0];

        int yyyyAtual = Integer.parseInt(anoAtual); // Ta dia
        int MMAtual = Integer.parseInt(mesAtual);
        int ddAtual = Integer.parseInt(diaAtual);   // Ta ano

        DateTime dataAtual = new DateTime(yyyyAtual, MMAtual, ddAtual, 12, 0);
        DateTime dataLicenca = new DateTime(yyyyBanco, MMBanco, ddBanco, 12, 30);
        int dias = Days.daysBetween(dataAtual, dataLicenca).getDays();
        if(dias <= 0){
            if(ddAtual != ddBanco){
              dias--;
            }
        }

        return dias;
    }

    public static int validaLicenca(){
        int validado=-1;
        String dataBanco = SingletonConfiguracoes.getConfig().getValidade();
        String licencaBanco = SingletonConfiguracoes.getConfig().getChave();

        if(MD5_Hash(dataBanco).equals(licencaBanco)){   // Se a data do banco bate com a licença
            int dias = verificaDiasVencimento(dataBanco);
                validado=dias;
        }

        return validado;
    }

    public static String MD5_Hash(String s) {
        MessageDigest m = null;
        String chave = "bcf55/*1236*/BCF55"+s;  // dd-mm-yyyy
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        m.update(chave.getBytes(),0,chave.length());
        String hash = new BigInteger(1, m.digest()).toString(16);
        return hash;
    }

    public static String dataAtual(){
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formataData = new SimpleDateFormat("dd-MM-yyyy");
        Date data = new Date();
        String dataFormatada = formataData.format(data);
        return  dataFormatada;
    }

    public static String dataAtualAmericana(){
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formataData = new SimpleDateFormat("yyyy-MM-dd");
        Date data = new Date();
        String dataFormatada = formataData.format(data);
        return  dataFormatada;
    }

    public static boolean validaTipoData(String s){
        boolean retorno = false;
        @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat ("dd-MM-yyyy");
        df.setLenient (false);  // Aqui o pulo do gato
        try {
            df.parse (s);
            // Data válida
            retorno = true;
        } catch (ParseException ex) {
            // Data inválida
            retorno=false;
        }
        return retorno;
    }

    public static String formatarDataHora(String dataHora){
        String formatou = "";

        @SuppressLint("SimpleDateFormat") SimpleDateFormat formataData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formataDataBr = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        try {
            Date data = formataData.parse(dataHora);
            String dataFormatada = formataDataBr.format(data);
            formatou = dataFormatada;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formatou;
    }

    public static String formatarDataBr(String dataBr){
        String formatou = "";

        @SuppressLint("SimpleDateFormat") SimpleDateFormat formataData = new SimpleDateFormat("yyyy-MM-dd");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formataDataBr = new SimpleDateFormat("dd-MM-yyyy");

        try {
            Date data = formataData.parse(dataBr);
            String dataFormatada = formataDataBr.format(data);
            formatou = dataFormatada;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formatou;
    }

    public static String formatarDataAm(String dataAm){
        String formatou = "";

        @SuppressLint("SimpleDateFormat") SimpleDateFormat formataDataBr = new SimpleDateFormat("dd-MM-yyyy");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formataData = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date data = formataDataBr.parse(dataAm);
            String dataFormatada = formataData.format(data);
            formatou = dataFormatada;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formatou;
    }

    public static void openConnectionDialog(Context context, FragmentManager getSupport){
        ConnectionDialog connectionDialog = new ConnectionDialog();
        connectionDialog.show(getSupport, "Diálogo");
    }

    public static void recuperaDadosCadastrados(Context context,String ARQUIVO_PREFERENCIA){

        // Recuperar dados(ip) salvos no celular
        SharedPreferences preferencesIp = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        // Validar se temos o ip preferencias
        if (preferencesIp.contains("ip")){
            String ip = preferencesIp.getString("ip", "IP não definido");
            SingletonConfiguracoes.getConfig().setIp(ip); // Setar dados salvos do celular no ipConfigurações
        }

        // Recuperar dados(conexão) salvos no dispositivo
        SharedPreferences preferencesConn = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        // Validar se temos o conn preferências
        if (preferencesConn.contains("conn")){
            String conn = preferencesConn.getString("conn", "Tipo de Conexão não definido");
            SingletonConfiguracoes.getConfig().setConnection(conn); // Setar dados salvos do celular no connConfigurações
        }

        // Recuperar dados(root) salvos no dispositivo
        SharedPreferences preferencesRoot = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        // Validar se temos o root preferências
        if (preferencesRoot.contains("root")){
            String root = preferencesRoot.getString("root", "Root não definido");
            SingletonConfiguracoes.getConfig().setRoot(root); // Setar dados salvos do celular no rootConfigurações
        }

        // Recuperar dados(nome do banco) salvos no dispositivo
        SharedPreferences preferencesNome = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        // Validar se temos o nome preferências
        if (preferencesNome.contains("nome")){
            String nome = preferencesNome.getString("nome", "Nome do banco não informado");
            SingletonConfiguracoes.getConfig().setNomeBanco(nome); // Setar dados salvos do celular no nomeConfigurações
        }

        // Recuperar dados(chave da licença) salvos no dispositivo
        SharedPreferences preferencesChave = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        // Validar se temos o chaveLicenca preferências
        if (preferencesChave.contains("chave")){
            String chave = preferencesChave.getString("chave", "Chave da licença não informada");
            SingletonConfiguracoes.getConfig().setChave(chave); // Setar dados salvos do celular no chaveConfigurações
        }

        // Recuperar dados(validade da licença) salvos no dispositivo
        SharedPreferences preferencesValidade = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        // Validar se temos o nome preferências
        if (preferencesValidade.contains("validade")){
            String validade = preferencesValidade.getString("validade", "Validade da licença não informada");
            SingletonConfiguracoes.getConfig().setValidade(validade); // Setar dados salvos do celular no validadeConfigurações
        }
        String testeValidade = SingletonConfiguracoes.getConfig().getValidade();
        String teste2 = testeValidade;
    }

    public static int gerarNumeroPedido() {
        @SuppressLint("SimpleDateFormat") DateFormat formatodata = new SimpleDateFormat("ddHHmmss");
        Date data = new Date();

        //this.Data = (formatodata.format(data));
        String registro = formatodata.format(data);
        int numeroGerado = Integer.parseInt(registro);
        return numeroGerado;

    }

    public static void limparBanco(Context context){
        String guardaCondexao = SingletonConfiguracoes.getConfig().getConnection();

        SingletonConfiguracoes.getConfig().setConnection("3");
        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.limparBanco(context);
        SingletonConfiguracoes.getConfig().setConnection(guardaCondexao);
    }

    public static void cancelaPedido(Context context){
        String guardaCondexao = SingletonConfiguracoes.getConfig().getConnection();

        SingletonConfiguracoes.getConfig().setConnection("3");
        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.cancelaPedido(context,1);
        SingletonConfiguracoes.getConfig().setConnection(guardaCondexao);
    }

    public static String formataDouble(Double numero){
        DecimalFormat df = new DecimalFormat("###,##0.00"); // Formatar os valores double para retornar casas decimais arredondadas corretamente.

        return df.format(numero).replace(",",".");
    }

    public static boolean validarConfigs(Context context,String ARQUIVO_PREFERENCIA){
        boolean retorno = false;

        // Recuperar dados(conexao) salvos no celular
        SharedPreferences preferencesConn = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        // Validar e atualizar se temos o connPreferencias
        if (preferencesConn.contains("conn")){
            String conn = preferencesConn.getString("conn", "Tipo de Conexão não definido");
            SingletonConfiguracoes.getConfig().setConnection(conn); //setar dados salvos do celular no connConfiguracoes
            retorno = true;
        } else {
            retorno = false;
            return false;
        }

        // Recuperar dados(nome do banco) salvos no celular
        SharedPreferences preferencesNomeBanco = context.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        // Validar e atualizar se temos o nomePreferencias
        if (preferencesNomeBanco.contains("nome")){
            String nome = preferencesNomeBanco.getString("nome", "Nome do Banco não informado");
            SingletonConfiguracoes.getConfig().setNomeBanco(nome); //setar dados salvos do celular no nomeConfiguracoes
            retorno = true;
        } else {
            retorno = false;
        }

        if (retorno){
            return true;
        }else {
            return false;
        }

    }

    public static String completaCodigo(String codigo) {
        String retorno = "";
        int tam = codigo.length();
        String conc = "";

        if (tam < 13) {//1

            for (int x = tam; x < 13; x++) {
                conc += "0";
            }
            retorno = conc + codigo;
        } else {
            retorno = codigo;
        }

        return retorno;
    }

    public static int codBalanca(String codigo) {
        int retorno = 7;

        try {
            char pri = codigo.charAt(0);
            char seg = codigo.charAt(1);
            char ter = codigo.charAt(2);
            char quar = codigo.charAt(3);

            String primeiroDigito = String.valueOf(pri);

            String codUm = String.valueOf(pri);
            String codDois = String.valueOf(seg);
            String codTres = String.valueOf(ter);
            String codQuatro = String.valueOf(quar);

            String codZero = codUm + codDois + codTres + codQuatro;

            if (codZero.equals("0000")) {
                // é um codigo de balança pesquisado na tela de  venda
                retorno = 0;
            } else {

                if (primeiroDigito.equals("2")) {
                    // é um codigo de balança fornecido pela etiqueta
                    retorno = 2;
                } else {
                    // é um codigo de um produto normal
                    retorno = 7;
                }
            }

        } catch (Exception e) {
        }

        return retorno;
    }

    // Fechar Teclado
    public static void closeKeyboard(View view, Context ctx) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
