package com.zsisistemas.zsiempresa.view.ui.empresa;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.model.Empresa;
import com.zsisistemas.zsiempresa.model.singleton.SingletonEmpresa;
import com.zsisistemas.zsiempresa.view.adapter.AdapterEmpresa;

import java.util.ArrayList;
import java.util.List;

public class EmpresaFragment extends Fragment {

    private RecyclerView recyclerView;
    public static List<Empresa> listaEmpresas = new ArrayList<>();
    public static AdapterEmpresa adapterEmpresa;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_empresa, container, false);

        recyclerView = view.findViewById(R.id.recycler_view_empresa);

        listaEmpresas = SingletonEmpresa.getEmpresa().getListaEmpresas();
        adapterEmpresa = new AdapterEmpresa(listaEmpresas, getActivity());
        adapterEmpresa.notifyDataSetChanged();

        // Configurar RecyclerView
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter( adapterEmpresa );

        return view;
    }
}