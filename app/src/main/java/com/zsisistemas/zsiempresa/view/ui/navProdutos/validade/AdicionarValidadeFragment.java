package com.zsisistemas.zsiempresa.view.ui.navProdutos.validade;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ValidadeModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;
import com.zsisistemas.zsiempresa.model.singleton.SingletonValidade;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.zsisistemas.zsiempresa.controller.Util.closeKeyboard;

public class AdicionarValidadeFragment extends Fragment {

    private TextView textoIdColeta, textoCodigoProduto, textoDescricaoProduto, dataValidade, textoIdentifica;
    private EditText editarCampoLocaliza, editarCampoQtd;
    private Button botaoEscanner, botaoLocalizar, botaoCancelar, botaoAdicionar;
    private ProgressDialog progressNoDialog;
    private ProgressDialog progressDialog;
    private ValidadeModel validadeModel;
    public static String checkValidade = ""; // vazio novo produto em vencimento, vindo com id ajustar existente.
    private List<ValidadeModel> listaValidades = new ArrayList<>();
    ValidadeFragment vf = new ValidadeFragment();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_adicionar_validade, container, false);

        textoIdColeta = view.findViewById(R.id.id_coleta);
        textoCodigoProduto = view.findViewById(R.id.text_codigo_produto);
        textoDescricaoProduto = view.findViewById(R.id.text_descricao_produto);
        textoIdentifica = view.findViewById(R.id.textView20);
        dataValidade = view.findViewById(R.id.data_validade);
        editarCampoLocaliza = view.findViewById(R.id.edit_campo_localiza);
        editarCampoQtd = view.findViewById(R.id.edit_campo_qtd);
        botaoLocalizar = view.findViewById(R.id.button_localizar);
        botaoCancelar = view.findViewById(R.id.button_cancelar);
        botaoAdicionar = view.findViewById(R.id.button_adicionar);
        botaoEscanner = view.findViewById(R.id.button_scann);
        botaoEscanner.setOnClickListener(mOnclickListener);
        validadeModel = new ValidadeModel();
        listaValidades = SingletonValidade.getValidade().getListaValidade();

        botaoLocalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(view, getActivity());

                String codigo = editarCampoLocaliza.getText().toString().trim();

                if(!codigo.isEmpty()){
                    progressNoDialog = new ProgressDialog(getActivity());
                    progressNoDialog.show();
                    progressNoDialog.setCancelable(false);
                    progressNoDialog.setContentView(R.layout.progress_no_dialog);
                    progressNoDialog.getWindow().setBackgroundDrawableResource(
                            android.R.color.transparent
                    );

                    new Thread() {
                        @Override
                        public void run() {

                            exibirProduto();
                            progressNoDialog.dismiss();

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {

                                    if(SingletonProduto.getProduto().getDescricao_produto() != null){

                                        setarTextos();

                                    }else{
                                        Toast.makeText(getActivity(),"Produto Não encontrado!",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    }.start();
                } else{
                    Toast.makeText(getActivity(),"Informe o código do produto!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        botaoCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limparTela();
                closeKeyboard(view, getActivity());
            }
        });

        dataValidade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(view, getActivity());

                String desc = textoDescricaoProduto.getText().toString().trim();

                if(!desc.isEmpty()){
                    if(!checkValidade.trim().isEmpty()){
                        String dataSelecionada = dataValidade.getText().toString().trim();
                        String s[] = dataSelecionada.split("-");

                        int mYear = Integer.parseInt(s[0]);
                        int mMonth = Integer.parseInt(s[1]);
                        int mDay = Integer.parseInt(s[2]);
                        mMonth--;

                        DatePickerDialog dateDialog= new DatePickerDialog(getActivity(), datePickerListener, mYear, mMonth, mDay);
                        dateDialog.show();

                    } else {

                        final Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR);
                        int mMonth = c.get(Calendar.MONTH);
                        int mDay = c.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog dateDialog= new DatePickerDialog(getActivity(), datePickerListener, mYear, mMonth, mDay);
                        dateDialog.show();

                    }

                } else {
                    Toast.makeText(getActivity(), "Primeiro localize o produto!", Toast.LENGTH_SHORT).show();
                }

            }
        });
        
        botaoAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(view, getActivity());

                String desc = textoDescricaoProduto.getText().toString().trim();
                String dataSelecionada = dataValidade.getText().toString().trim();
                String qtdDigitada = editarCampoQtd.getText().toString().trim();

                if(!desc.isEmpty()) {
                    if(!dataSelecionada.equalsIgnoreCase("Selecione")) {
                        if(!qtdDigitada.isEmpty()) {

                            int chave = Util.gerarNumeroPedido();

                            validadeModel.setCodigo(SingletonProduto.getProduto().getCodigo_produto());
                            validadeModel.setDescricao(SingletonProduto.getProduto().getDescricao_produto());
                            validadeModel.setQtdEstoque(Double.parseDouble(qtdDigitada));
                            validadeModel.setDataValidade(dataSelecionada);

                            if(checkValidade.trim().isEmpty()){
                                validadeModel.setChave(String.valueOf(chave));
                            } else {
                                validadeModel.setChave(checkValidade);
                            }

                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.show();
                            progressDialog.setContentView(R.layout.progress_dialog);
                            progressDialog.getWindow().setBackgroundDrawableResource(
                                    android.R.color.transparent
                            );

                            new Thread() {
                                @Override
                                public void run() {

                                    salvarValidade();
                                    progressDialog.dismiss();

                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {

                                            if(botaoAdicionar.getText().equals("AJUSTAR")){
                                                Toast.makeText(getActivity(), "Vencimento ajustado!", Toast.LENGTH_SHORT).show();
                                            }else {
                                                Toast.makeText(getActivity(), "Adicionado na lista de vencimento!", Toast.LENGTH_SHORT).show();
                                            }

                                            limparTela();
                                            SingletonValidade.getValidade().setListaValidade(null);
                                            vf.botaoListarValidade.performClick();
                                        }
                                    });
                                }
                            }.start();

                        } else {
                            Toast.makeText(getActivity(), "Informe a quantidade do estoque!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Selecione a data de validade!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Primeiro localize o produto!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);

        if(result != null){

            if(result.getContents() != null){
                editarCampoLocaliza.setText(result.getContents());
                exibirProduto();

                if(SingletonProduto.getProduto().getDescricao_produto() != null){
                    setarTextos();
                }else{
                    Toast.makeText(getActivity(),"Produto Não encontrado!",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private View.OnClickListener mOnclickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button_scann:
                    IntentIntegrator.forSupportFragment(AdicionarValidadeFragment.this).initiateScan();
                    break;
            }
        }
    };

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            monthOfYear++;

            String mes = ""+monthOfYear;
            String dia = ""+dayOfMonth;

            if(monthOfYear < 10){
                mes = "0"+monthOfYear;
            }

            if(dayOfMonth < 10){
                dia = "0"+dayOfMonth;
            }

            String dateYouChoosed = year + "-" + mes + "-" + dia;
            dataValidade.setText(dateYouChoosed );
        }
    };

    public void exibirProduto(){
        String codigo = editarCampoLocaliza.getText().toString().trim();

        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.localizaProdutoPorCodigo(getActivity(),codigo);

    }

    public void salvarValidade(){
        Controller_Conexao controller_conexao = new Controller_Conexao();

        if(!checkValidade.trim().isEmpty()){
            String chave = checkValidade;

            controller_conexao.atualizarValidade(validadeModel, chave);
        } else {
            controller_conexao.salvarValidade(getActivity(),validadeModel);
        }

    }

    @SuppressLint("SetTextI18n")
    public void setarTextos(){

        textoIdColeta.setText(" " + SingletonProduto.getProduto().getId_Produtos());
        textoCodigoProduto.setText(" " + SingletonProduto.getProduto().getCodigo_produto());
        textoDescricaoProduto.setText(" " + SingletonProduto.getProduto().getDescricao_produto());
        editarCampoLocaliza.setText("");
    }

    public void limparTela(){

        textoIdColeta.setText("");
        textoCodigoProduto.setText("");
        textoDescricaoProduto.setText("");
        editarCampoLocaliza.setText("");
        dataValidade.setText("Selecione");
        editarCampoQtd.setText("");
        checkValidade = "";
        botaoAdicionar.setText("ADICIONAR");
        textoIdentifica.setText("ADICIONAR");

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResume() {
        super.onResume();

        if(!checkValidade.trim().isEmpty()){
            botaoAdicionar.setText("AJUSTAR");
            textoIdentifica.setText("AJUSTAR");

            String chave = checkValidade;

            for (ValidadeModel v: listaValidades) {
                if (v.getChave().equals(chave)){

                    textoIdColeta.setText(" " + v.getIdValidade());
                    textoCodigoProduto.setText(" " + v.getCodigo());
                    textoDescricaoProduto.setText(" " + v.getDescricao());

                    dataValidade.setText(v.getDataValidade());
                    editarCampoQtd.setText(String.valueOf(v.getQtdEstoque()));
                    break;
                }

            }

        } else {
            botaoAdicionar.setText("ADICIONAR");
            textoIdentifica.setText("ADICIONAR");
        }
    }
}