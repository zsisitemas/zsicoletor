package com.zsisistemas.zsiempresa.view.ui.clientes;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ClienteModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;

public class ClientesFragment extends Fragment {

    public static Button botaoListaCliente, botaoHistoricoCliente, botaoCadastroCliente;
    private ListaClientesFragment listaClienteFragment;
    private CadastroClientesFragment cadastroClienteFragment;
    private HistoricoClienteFragment historicoClienteFragment;
    private View viewLista, viewHistorico, viewCadastro;
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cliente, container, false);

        botaoListaCliente = view.findViewById(R.id.button_lista_cliente);
        botaoHistoricoCliente = view.findViewById(R.id.button_historico_cliente);
        botaoCadastroCliente = view.findViewById(R.id.button_cadastro_cliente);
        viewLista = view.findViewById(R.id.view_lista);
        viewHistorico = view.findViewById(R.id.view_historico);
        viewCadastro = view.findViewById(R.id.view_cadastro);
        listaClienteFragment = new ListaClientesFragment();
        cadastroClienteFragment = new CadastroClientesFragment();
        historicoClienteFragment = new HistoricoClienteFragment();

        // Configurar o objeto para o fragment

        botaoCadastroCliente.setTextColor(Color.parseColor("#CCD6D7D7"));
        botaoHistoricoCliente.setTextColor(Color.parseColor("#CCD6D7D7"));
        botaoListaCliente.setTextColor(Color.parseColor("#FFFFFF"));

        viewCadastro.setBackgroundColor(Color.parseColor("#00000000"));
        viewHistorico.setBackgroundColor(Color.parseColor("#00000000"));
        viewLista.setBackgroundColor(Color.parseColor("#FFFFFF"));

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_conteudo, listaClienteFragment);
        setHasOptionsMenu(true);
        transaction.commit();

        botaoListaCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                botaoCadastroCliente.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoHistoricoCliente.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoListaCliente.setTextColor(Color.parseColor("#FFFFFF"));

                viewCadastro.setBackgroundColor(Color.parseColor("#00000000"));
                viewHistorico.setBackgroundColor(Color.parseColor("#00000000"));
                viewLista.setBackgroundColor(Color.parseColor("#FFFFFF"));

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_conteudo, listaClienteFragment);
                setHasOptionsMenu(true);
                transaction.commit();
            }
        });

        botaoHistoricoCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nomeCliente = SingletonCliente.getCliente().getNomeClienteSelecionado();
                
                if(nomeCliente != null){

                    botaoCadastroCliente.setTextColor(Color.parseColor("#CCD6D7D7"));
                    botaoListaCliente.setTextColor(Color.parseColor("#CCD6D7D7"));
                    botaoHistoricoCliente.setTextColor(Color.parseColor("#FFFFFF"));

                    viewCadastro.setBackgroundColor(Color.parseColor("#00000000"));
                    viewLista.setBackgroundColor(Color.parseColor("#00000000"));
                    viewHistorico.setBackgroundColor(Color.parseColor("#FFFFFF"));

                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_conteudo, historicoClienteFragment);
                    setHasOptionsMenu(false);
                    transaction.commit();
                }else {
                    Toast.makeText(getActivity(), "Selecione um cliente para ver o histórico", Toast.LENGTH_SHORT).show();
                }

            }
        });

        botaoCadastroCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                botaoListaCliente.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoHistoricoCliente.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoCadastroCliente.setTextColor(Color.parseColor("#FFFFFF"));

                viewHistorico.setBackgroundColor(Color.parseColor("#00000000"));
                viewLista.setBackgroundColor(Color.parseColor("#00000000"));
                viewCadastro.setBackgroundColor(Color.parseColor("#FFFFFF"));

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_conteudo, cadastroClienteFragment);
                setHasOptionsMenu(false);
                transaction.commit();
            }
        });
        return view;
    }

    // Buscar cliente na recycler pela letra digitada no menu search
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_busca, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.app_bar_buscar).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                procuraCliente(newText);
                Util.recuperaDadosCadastrados(getActivity(), ARQUIVO_PREFERENCIA);
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    public void procuraCliente(String nome) {

        if(listaClienteFragment.listaClientes != null){

            SingletonConfiguracoes.getConfig().setConnection("3");
            listaClienteFragment.listaClientesFiltrados.clear();

            for (ClienteModel c : listaClienteFragment.listaClientes) {
                if (c.getNome() != null){
                    if (c.getNome().toLowerCase().contains(nome.toLowerCase())) {
                        listaClienteFragment.listaClientesFiltrados.add(c);
                    }
                }
            }

            listaClienteFragment.adapterClientes.notifyDataSetChanged();

        }
    }
}