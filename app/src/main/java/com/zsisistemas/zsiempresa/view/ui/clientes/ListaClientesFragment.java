package com.zsisistemas.zsiempresa.view.ui.clientes;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ClienteModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;
import com.zsisistemas.zsiempresa.view.activities.RecyclerItemClickListener;
import com.zsisistemas.zsiempresa.view.adapter.AdapterClientes;

import java.util.ArrayList;
import java.util.List;

public class ListaClientesFragment extends Fragment {

    private RecyclerView recyclerView;
    public static List<ClienteModel> listaClientes = new ArrayList<>();
    public static List<ClienteModel> listaClientesFiltrados = new ArrayList<>();
    AdapterClientes adapterClientes;
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";
    private boolean pedidoChecked;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_clientes, container, false);

        recyclerView = view.findViewById(R.id.recycler_view_clientes);
        Util.recuperaDadosCadastrados(getActivity(), ARQUIVO_PREFERENCIA);
        pedidoChecked = SingletonProduto.getProduto().isCheckPedido();

        // Listagem de Clientes
        if(SingletonCliente.getCliente().getListaclientes() == null){
            this.listarClientes();
        }

        // Configurar Adapter
        listaClientes = SingletonCliente.getCliente().getListaclientes();
        listaClientesFiltrados.clear();

        if(listaClientes != null){
            listaClientesFiltrados.addAll(listaClientes);
        }

        adapterClientes = new AdapterClientes(listaClientesFiltrados, getActivity());

        // Configurar RecyclerView
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter( adapterClientes );

        SharedPreferences preferencesPedido = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        SharedPreferences.Editor editor = preferencesPedido.edit();

        // Evento de clique no item do recycler
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getActivity().getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                ClienteModel clienteModel = listaClientesFiltrados.get(position);
                                // Usando getObservacao como auxiliar pra recuperar disponibilidade dos clientes.
                                String disponibilidade = clienteModel.getObservacao();

                                if (disponibilidade != null && disponibilidade.equalsIgnoreCase("OCUPADO!")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setTitle("Cliente " + clienteModel.getNome() + " ocupado")
                                            .setMessage("Impossível selecionar clientes ocupados!")
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            })
                                            .show();

                                } else {
                                    if(pedidoChecked){
                                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                        builder.setTitle("Pedido em atualização")
                                                .setMessage("Selecionar cliente descartará todas as alterações do pedido!")
                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        Util.limparBanco(getActivity());
                                                        SingletonProduto.getProduto().setCheckPedido(false);
                                                        selecionarCliente(clienteModel);

                                                        String pedido = "false";
                                                        String obs = null;

                                                        editor.putString("obs", obs);
                                                        editor.putString("pedido", pedido);
                                                        editor.apply();

                                                        SingletonProduto.getProduto().setCheckPedido(Boolean.parseBoolean(pedido));
                                                        SingletonProduto.getProduto().setObs(obs);
                                                    }
                                                })
                                                .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                })
                                                .show();
                                    }else {
                                        selecionarCliente(clienteModel);
                                    }
                                }
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );

        return view;
    }

    public void listarClientes(){
        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.listaCliete(getActivity());
    }

    public void selecionarCliente(ClienteModel clienteModel){
        SharedPreferences preferencesIdCliente = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        SharedPreferences.Editor editor = preferencesIdCliente.edit();

        String idCli = String.valueOf(clienteModel.getId());
        String nomeCli = clienteModel.getNome();
        String telefoneCli = String.valueOf(clienteModel.getTelefone());

        editor.putString("idCli", idCli);
        editor.putString("nomeCli", nomeCli);
        editor.putString("telefoneCli", telefoneCli);
        editor.apply();

        SingletonCliente.getCliente().setIdClienteSelecionado(Integer.parseInt(idCli));
        SingletonCliente.getCliente().setNomeClienteSelecionado(nomeCli);
        SingletonCliente.getCliente().setTelefoneClienteSelecionado(telefoneCli);

        Toast.makeText(getActivity(),"Cliente selecionado(a): "+ clienteModel.getNome(),Toast.LENGTH_SHORT).show();
    }
}