package com.zsisistemas.zsiempresa.view.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.google.android.material.navigation.NavigationView;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.view.ui.navProdutos.consulta.ColetaFragment;

import static com.zsisistemas.zsiempresa.controller.Util.closeKeyboard;
import static com.zsisistemas.zsiempresa.controller.Util.validarConfigs;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";
    private TextView navWelcome;
    private TextView navUsername;
    private TextView navLogin;
    private MenuItem nav_dashboard_desenvolvedor;
    private MenuItem nav_dashboard_carrinho;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Util.recuperaDadosCadastrados(this,"ArquivoPreferencia");

        // Verificar se usuário está logado ou não.
        //p1
        SharedPreferences preferencesLoginFuncionario = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if (preferencesLoginFuncionario.contains("loginFun")){
            String loginFun = preferencesLoginFuncionario.getString("loginFun", null);

            SingletonCliente.getCliente().setLoginFuncionario(loginFun);
        } else {
            SingletonCliente.getCliente().setLoginFuncionario(null);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        Intent intent = new Intent(this, AberturaActivity.class);
        startActivity(intent);

        // Configura barra de navegação
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    closeKeyboard(v, getApplicationContext());
                }
            }
        });

        // Cria referência para toda a área do NavigationDrawer
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                View focusedView = getCurrentFocus();
                if (focusedView != null){
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);// TODO Se nenhuma view esta em foco vai lançar NullPointerException
                }
            }
        });

        // Cria referência para a área Navegação
        NavigationView navigationView = findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);
        navWelcome = headerView.findViewById(R.id.textWelcome);
        navUsername = headerView.findViewById(R.id.textUserName);
        navLogin = headerView.findViewById(R.id.textLogin);

        Menu menu = navigationView.getMenu();
        nav_dashboard_desenvolvedor = menu.findItem(R.id.nav_empresa);
        nav_dashboard_carrinho = menu.findItem(R.id.nav_carrinho);

        nav_dashboard_desenvolvedor.setVisible(false);
        nav_dashboard_carrinho.setVisible(true);

        /*recarga(getApplicationContext());*/

        navLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validarConfigs(getApplicationContext(), ARQUIVO_PREFERENCIA)) {
                    Toast.makeText(getApplicationContext(), "Primeiro entre como convidado e ajuste as configurações", Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(getApplicationContext(), AberturaActivity.class);
                    startActivity(intent);
                }
            }
        });

        // Define Configurações do NavigationDrawer
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_inicio, R.id.nav_produtos, R.id.nav_carrinho, R.id.nav_cliente,
                R.id.nav_empresa, R.id.nav_configuracao, R.id.nav_sobre, R.id.nav_importar_exportar)
                .setDrawerLayout(drawer)
                .build();

        // Configura a área que irá carregar os fagments
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        // Configura menu superior de navegação
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
//        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);

        // Configura navegação para NavigationView
        NavigationUI.setupWithNavController(navigationView, navController);

        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {

                if(destination.getId() == R.id.nav_carrinho) {
                    navigationView.setCheckedItem(R.id.nav_carrinho);
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferencesPerfilFun = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if(preferencesPerfilFun.contains("perfilFun")){
            String perfilFun = preferencesPerfilFun.getString("perfilFun", "perfilFun não definido");
            SingletonCliente.getCliente().setPerfil(perfilFun);

            // Controlar conflito entre listas carrinho e produtos quando ha pessoa logada com banco sqlite
            if (SingletonConfiguracoes.getConfig().getConnection().equals("3")) {
                nav_dashboard_carrinho.setVisible(false);
            } else {
                nav_dashboard_carrinho.setVisible(true);
            }

            if (perfilFun.equalsIgnoreCase("dev")) {
                nav_dashboard_desenvolvedor.setVisible(true);
            } else {
                nav_dashboard_desenvolvedor.setVisible(false);
            }
        } else {
            nav_dashboard_desenvolvedor.setVisible(false);
            nav_dashboard_carrinho.setVisible(true);
        }

        // Verificar o nome do usuário logado.
        SharedPreferences preferencesNomeUsuario = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if (preferencesNomeUsuario.contains("nomeUsuario")) {
            String nomeUsuario = preferencesNomeUsuario.getString("nomeUsuario", null);

            SingletonCliente.getCliente().setNome(nomeUsuario);
        } else {
            SingletonCliente.getCliente().setNome(null);
        }

        if (SingletonCliente.getCliente().getNome() != null) {
            navWelcome.setVisibility(View.VISIBLE);

            navUsername.setText(SingletonCliente.getCliente().getNome());
            navUsername.setVisibility(View.VISIBLE);

            navLogin.setVisibility(View.GONE);
        } else {
            navWelcome.setVisibility(View.INVISIBLE);

            navUsername.setVisibility(View.GONE);

            navLogin.setVisibility(View.VISIBLE);
        }
    }
}
