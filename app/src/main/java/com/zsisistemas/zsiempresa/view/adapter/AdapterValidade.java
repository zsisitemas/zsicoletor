package com.zsisistemas.zsiempresa.view.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ValidadeModel;
import com.zsisistemas.zsiempresa.view.ui.navProdutos.validade.AdicionarValidadeFragment;
import com.zsisistemas.zsiempresa.view.ui.navProdutos.validade.ListarValidadeFragment;
import com.zsisistemas.zsiempresa.view.ui.navProdutos.validade.ValidadeFragment;

import java.util.List;

public class AdapterValidade extends RecyclerView.Adapter<AdapterValidade.MyViewHolder> {

    // Lista com todos validade
    private List<ValidadeModel> listaValidade;
    ValidadeFragment validadeFragment = new ValidadeFragment();
    private ProgressDialog progressDialog;
    Context ctx;
    ValidadeFragment vf = new ValidadeFragment();

    public AdapterValidade(List<ValidadeModel> lista, Context ctx) {
        this.listaValidade = lista;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mainCardViewLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cartao_recycler_validade, parent, false);
        // Define o layout do item
        return new MyViewHolder(mainCardViewLista);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // Seta os dados do produto para exibição
        ValidadeModel validade = listaValidade.get( position );
        String diasFaltando = Util.formatarDataBr(validade.getDataValidade());

        holder.idValidade.setText(String.valueOf(validade.getIdValidade()));
        holder.descricao.setText(validade.getDescricao());
        holder.qtd_estoque.setText(String.valueOf(validade.getQtdEstoque()));
        holder.dataValidade.setText(diasFaltando);

        int dia = Util.verificaDiasVencimento(diasFaltando);
        String diasMSG="";
        if(dia <0){
            diasMSG = String.valueOf(dia)+ " VENCIDO!!!";
        }else{
            diasMSG = String.valueOf(dia)+ " Dia(s)";
        }
        holder.dias.setText(diasMSG);

        holder.chave.setText(validade.getChave());

    }

    @Override
    public int getItemCount() {
        // Define a quantidade de itens na RecyclerView
        // Retornar tamanho da lista se não for nula
        return listaValidade != null ? listaValidade.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{

        // Criar objetos para exibir os itens mantendo a visualização
        // Cada item é um objeto da MyViewHolder

        TextView idValidade;
        TextView descricao;
        TextView qtd_estoque;
        TextView dataValidade;
        TextView dias;
        TextView chave;

        public MyViewHolder(View itemView){
            super(itemView);

            idValidade = itemView.findViewById(R.id.idValidade);
            descricao = itemView.findViewById(R.id.text_descricao);
            qtd_estoque = itemView.findViewById(R.id.text_estoque);
            dataValidade = itemView.findViewById(R.id.text_validade);
            dias = itemView.findViewById(R.id.text_dias);
            chave = itemView.findViewById(R.id.text_chave);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            menu.setHeaderTitle(descricao.getText().toString());
            menu.add("Inativar").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    ListarValidadeFragment listarValidadeFragment = new ListarValidadeFragment();
                    String chaveProduto = chave.getText().toString();

                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                    builder.setTitle("Inativar vencimento")
                            .setMessage("Tem certeza que deseja inativar este produto da lista?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    progressDialog = new ProgressDialog(ctx);
                                    progressDialog.show();
                                    progressDialog.setContentView(R.layout.progress_dialog);
                                    progressDialog.getWindow().setBackgroundDrawableResource(
                                            android.R.color.transparent
                                    );

                                    new Thread() {
                                        @Override
                                        public void run() {

                                            inativarVencimento(chaveProduto);
                                            progressDialog.dismiss();

                                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                @Override
                                                public void run() {

                                                    for (ValidadeModel vm:listarValidadeFragment.listaValidade) {
                                                        if(vm.getChave()==chaveProduto){
                                                            listarValidadeFragment.listaValidade.remove(vm);
                                                            listarValidadeFragment.adapterValidade.notifyDataSetChanged();
                                                        }
                                                    }

//                                                    vf.botaoListarValidade.performClick();
                                                    Toast.makeText(ctx, "Vencimento inativado!", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    }.start();
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                    return true;
                }
            });

            menu.add("Ajustar").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    AdicionarValidadeFragment adicionarValidadeFragment = new AdicionarValidadeFragment();

                    adicionarValidadeFragment.checkValidade = chave.getText().toString();

                    validadeFragment.botaoAdicionarValidade.performClick();
                    return true;
                }
            });
        }
    }

    public void inativarVencimento(String chaveProduto){

        Controller_Conexao cc = new Controller_Conexao();
        cc.inativarVencimento(chaveProduto);

    }
}
