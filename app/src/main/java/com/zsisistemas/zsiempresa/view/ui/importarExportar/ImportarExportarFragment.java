package com.zsisistemas.zsiempresa.view.ui.importarExportar;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.text.InputType.TYPE_NULL;
import static com.zsisistemas.zsiempresa.controller.Util.validaLicenca;

/**
 * Documentação
 * botaoExportar - Percorre as colunas da tabela produtos do banco SQLite do dispositivo convertendo
 * os campos para gerar um arquivo .xlsx salvo na memória do próprio dispositivo.
 * <p>
 * botaoImpotar - Percorre o arquivo .xlsx selecionado a partir do gerenciador de arquivos do
 * dispositivo convertendo os campos para popular a tabela produtos do banco SQLite
 * do próprio dispositivo.
 */
public class ImportarExportarFragment extends Fragment {

    private final int CHOOSE_DOCUMENT_FROM_DEVICE = 1001;
    private final int SAVE_DOCUMENT_IN_DEVICE = 1002;
    private static final String TAG = "ImportarExportarFragment";
    private Button botaoExportar, botaoImpotar, botaoLimparProdutos, botaoZerarContagem;
    private ProdutoModel produtoModel;
    private Controller_Conexao controller_conexao;
    private ArrayList<ProdutoModel> listaTodosProdutos = new ArrayList<>();
    private ArrayList<ProdutoModel> uploadDados;
    private int contar_linhas = 0;
    private int conn_preferencias = Integer.parseInt(SingletonConfiguracoes.getConfig().getConnection());
    private ProgressDialog progressDialog;
    private EditText inputNomeArquivo;
    private String nomeArquivo;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_importar_exportar, container, false);

        botaoImpotar = view.findViewById(R.id.button_import);
        botaoExportar = view.findViewById(R.id.button_export);
        botaoLimparProdutos = view.findViewById(R.id.button_limpar_banco);
        botaoZerarContagem = view.findViewById(R.id.button_zerar_contagem);
        uploadDados = new ArrayList<>();
        this.produtoModel = new ProdutoModel();
        controller_conexao = new Controller_Conexao();

        if (conn_preferencias != 3) {
            SingletonConfiguracoes.getConfig().setConnection("3");
        }

//        Precisa checar as permissões
        checarPermissoesArquivos();

        botaoImpotar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirExploradorArquivos();
            }
        });

        botaoExportar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int dias = validaLicenca();
              //  if (dias > 0) {
//                    Executar o método para escrever dados Excel.
                    controller_conexao.listarProdutos(getActivity());
                    listaTodosProdutos = SingletonProduto.getProduto().getListaProdutos();

                    if (listaTodosProdutos.size() > 0) {
                        abrirExploradorPastas();
                    } else {
                        Toast.makeText(getActivity(), "Impossível exportar banco vazio!", Toast.LENGTH_SHORT).show();
                    }
                //} else {
                 //   Toast.makeText(getActivity(), "Licença Vencida!", Toast.LENGTH_SHORT).show();
               // }
            }
        });

        botaoLimparProdutos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Limpar dados do banco.
                controller_conexao.listarProdutos(getActivity());
                listaTodosProdutos = SingletonProduto.getProduto().getListaProdutos();
                if (listaTodosProdutos.size() > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Limpar Banco")
                            .setMessage("Tem certeza que deseja limpar todos os produtos do banco?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    limparBanco();
                                    Toast.makeText(getActivity(), "Banco limpo com sucesso!", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }
            }
        });

        botaoZerarContagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Zerar contagem do banco.
                controller_conexao.listarProdutos(getActivity());
                listaTodosProdutos = SingletonProduto.getProduto().getListaProdutos();
                if (listaTodosProdutos.size() > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Zerar Contagem")
                            .setMessage("Tem certeza que deseja zerar a contagem dos produtos?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    zerarContagem();
                                    Toast.makeText(getActivity(), "Contagem zerada com sucesso!", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }
            }
        });

        return view;
    }

    private void abrirExploradorArquivos() {
        Intent it = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        it.addCategory(Intent.CATEGORY_OPENABLE);
//        Define o tipo de arquivo a ser exibido no explorador de arquivos.
        it.setType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        startActivityForResult(it, CHOOSE_DOCUMENT_FROM_DEVICE);
    }

    //    TODO Observação: a ação da intent ACTION_OPEN_DOCUMENT_TREE está disponível no Android 5.0 (API de nível 21) e em versões mais recentes.
    public void abrirExploradorPastas() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, SAVE_DOCUMENT_IN_DEVICE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHOOSE_DOCUMENT_FROM_DEVICE && resultCode == RESULT_OK) {
//            data contem um URI para o documento ou diretorio que o usuario selecionou
            if (data != null) {
//                Aqui nos mostraremos o path ou uri do arquivo
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Diretório: " + data.getData().getPath())
                        .setMessage("Atenção: \n " +
                                    "* O formato do arquivo deve ser .xlsx \n " +
                                    "* O arquivo deve ter entre 4 e 13 colunas \n " +
                                    "* As colunas devem começar respectivamente com: CÓDIGO, DESCRIÇÃO e ESTOQUE.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                progressDialog = new ProgressDialog(getActivity());
                                progressDialog.show();
                                progressDialog.setCancelable(false);
                                progressDialog.setContentView(R.layout.progress_dialog);
                                progressDialog.getWindow().setBackgroundDrawableResource(
                                        android.R.color.transparent
                                );

                                new Thread() {
                                    @Override
                                    public void run() {
                                        boolean retorno = lerDadosExcel(data.getData());
                                        // Toast.makeText(getActivity(), "Formato do arquivo Excel incorreto!", Toast.LENGTH_SHORT).show();
                                        progressDialog.dismiss();
                                        if (retorno == true) {
                                            getActivity().runOnUiThread(new Runnable() {
                                                public void run() {
                                                    Toast.makeText(getActivity(), "Produtos importados com sucesso!", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        } else {
                                            getActivity().runOnUiThread(new Runnable() {
                                                public void run() {
                                                    Toast.makeText(getActivity(), "Formato do arquivo Excel incorreto!!", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }

                                    }
                                }.start();
                            }
                        })
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
            }
        }

        if (requestCode == SAVE_DOCUMENT_IN_DEVICE && resultCode == RESULT_OK) {
//            data contem um URI para o documento ou diretorio que o usuario selecionou
            if (data != null) {
//                Aqui nos mostraremos o path ou uri do arquivo
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Diretório: " + data.getData().getPath())
                        .setMessage("Tem certeza que deseja exportar o banco para esta pasta?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                AlertDialog.Builder builderNome = new AlertDialog.Builder(getActivity());
                                builderNome.setTitle("Renomeie");
                                inputNomeArquivo = new EditText(getActivity());
                                builderNome.setView(inputNomeArquivo);
                                inputNomeArquivo.setFocusable(true);

                                inputNomeArquivo.setFocusableInTouchMode(true);
                                int dias = validaLicenca();
                                if (dias >= 0) {
                                    inputNomeArquivo.setText("zsiempresa_app");
                                } else {
                                    inputNomeArquivo.setText("zsiempresa_app");
                                    inputNomeArquivo.setInputType(TYPE_NULL);
                                    inputNomeArquivo.setOnClickListener(new View.OnClickListener() {

                                        @Override
                                        public void onClick(View v) {
                                            getActivity().runOnUiThread(new Runnable() {
                                                public void run() {
                                                    Toast.makeText(getActivity(), "Libere a versão completa para renomear o arquivo!", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    });
                                }

                                builderNome.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Util.closeKeyboard(inputNomeArquivo, getActivity());
                                        String arquivoNome = inputNomeArquivo.getText().toString().trim();
                                        if (!arquivoNome.isEmpty()) {
                                            nomeArquivo = arquivoNome;

                                            progressDialog = new ProgressDialog(getActivity());
                                            progressDialog.show();
                                            progressDialog.setCancelable(false);
                                            progressDialog.setContentView(R.layout.progress_dialog);
                                            progressDialog.getWindow().setBackgroundDrawableResource(
                                                    android.R.color.transparent
                                            );

                                            new Thread() {
                                                @Override
                                                public void run() {
                                                    escreverDadosExcel(listaTodosProdutos, data.getData());
                                                    progressDialog.dismiss();
                                                }
                                            }.start();
                                        }
                                    }
                                })
                                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        })
                                        .show();
                            }
                        })
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
            }
        }
    }

    /**
     * Ler colunas depois linhas do arquivo excel. Armazenar dados como objeto de ExcelUploadDados
     *
     * @return
     */
    private boolean lerDadosExcel(Uri uri) {
        boolean retorno = true;
        try {
            InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            int rowsCount = sheet.getPhysicalNumberOfRows();
            FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
            StringBuilder sb = new StringBuilder();

            for (int r = 0; r < rowsCount; r++) {
                Row row = sheet.getRow(r);
                int cellsCount = row.getPhysicalNumberOfCells();
//                Loop interno, percorrer colunas
                for (int c = 0; c < cellsCount; c++) {
//                    Trata se houver muitas colunas na planilha Excel.
                    if (cellsCount > 13 || cellsCount < 4) {
                        retorno = false;
                        Log.e(TAG, "lerDadosExcel: ERRO. O formato de arquivo Excel está incorreto! ");

                        break;
                    } else {
                        String value = getCellAsString(row, c, formulaEvaluator);
                        String cellInfo = "r:" + r + "; c:" + c + "; v:" + value;
                        Log.d(TAG, "lerDadosExcel: Dados da linha: " + cellInfo);

//                        Define a String que vai separar as células
                        sb.append(value).append("trocou de celula ");
                    }
                }
                sb.append(":");
            }
            Log.d(TAG, "lerDadosExcel: STRINGBUILDER: " + sb.toString());

            parseStringBuilder(sb);

        } catch (FileNotFoundException e) {
            Log.e(TAG, "lerDadosExcel: FileNotFoundException. FileNotFoundException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "lerDadosExcel: Erro ao ler inputstream. IOException: " + e.getMessage());
        }
        return retorno;
    }

    /**
     * Criar arquivo excel para receber dados exportados do sqlite.
     *
     * @return
     */
    private void escreverDadosExcel(List<ProdutoModel> listaProdutosToXlsx, Uri uri) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("mysheet"));

        int limitFreeTrial = 0;
        for (int r = 0; r <= listaProdutosToXlsx.size(); r++) {
            ProdutoModel produtoModel = new ProdutoModel();
            if (r > 0) {
                produtoModel = listaProdutosToXlsx.get(r - 1);
            } else {
                produtoModel = listaProdutosToXlsx.get(r);
            }
//            Criar linhas de acordo com o tamanho da lista
            Row row = sheet.createRow(limitFreeTrial);
//            Seta as células da respectiva coluna
            Cell cell = row.createCell(0);
            Cell cell1 = row.createCell(1);
            Cell cell2 = row.createCell(2);
            Cell cell3 = row.createCell(3);
            Cell cell4 = row.createCell(4);
             int limite = 100;
             int licencaValida = validaLicenca();
             if(licencaValida >=0){
                 limite = 99999999;
             }
            if (r > 0 && limitFreeTrial > 0) {
                if (produtoModel.getStatus() == 1) {
                    if (limitFreeTrial <= limite) {

//                        Seta o valor de cada linha
                        cell.setCellValue(produtoModel.getId_Produtos());
                        cell1.setCellValue(produtoModel.getCodigo_produto());
                        cell2.setCellValue(produtoModel.getDescricao_produto());
                        cell3.setCellValue(produtoModel.getEstoque());
                        cell4.setCellValue(produtoModel.getQtd_inventario());

                        limitFreeTrial++;
                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Limite atingido")
                                        .setMessage("Versão de testes é limitada a exportar apenas 100 dos produtos coletados")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Toast.makeText(getActivity(), "Libere a versão completa para exportações ilimitadas!", Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .show();
                            }
                        });
                        break;
                    }
                }
            } else {
//                Seta o nome dos campos
                cell.setCellValue("id_Produtos");
                cell1.setCellValue("codigo_produto");
                cell2.setCellValue("descricao_produto");
                cell3.setCellValue("estoque");
                cell4.setCellValue("qtd_inventario");
                limitFreeTrial++;
            }
        }

        DocumentFile arquivoXLSX = null;

        if (limitFreeTrial > 1) {
            // TODO definir extençao do arquivo exportado aqui
            arquivoXLSX = DocumentFile.fromTreeUri(getActivity(), uri)
                    .createFile("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", nomeArquivo);

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Banco exportado com sucesso!", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Exportação cancelada! Colete alguns produtos para continuar!", Toast.LENGTH_LONG).show();
                }
            });
        }

        OutputStream streamDeSaida = null;

        try {
            if (arquivoXLSX != null) {
                streamDeSaida = getActivity().getContentResolver().openOutputStream(arquivoXLSX.getUri());
                workbook.write(streamDeSaida);
                streamDeSaida.flush();
                streamDeSaida.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Método para converter dados importados e armazenar em ArrayList<ProdutoModel>
     */
    @SuppressLint("SetTextI18n")
    public void parseStringBuilder(StringBuilder mStringBuilder) {

        Log.d(TAG, "parseStringBuilder: Começou a converter.");

//        Separar o sb em linhas.
        String[] rows = mStringBuilder.toString().split(":");
        contar_linhas = rows.length;

//        Adcionar ao ArrayList<ProdutoModel> linha por linha
        for (int i = 1; i < rows.length; i++) {
//            Separar as colunas das linhas
            String[] columns = rows[i].split("trocou de celula ");

//            Usar try catch para garantir que não haja "" que tente converter em doubles.
            try {

                /*String id_Produtos = (columns[0]);*/
                String codigo_produto = (columns[0]);
                String descricao_produto = (columns[1]);
                String estoque = (columns[2]);

                String cellInfo = "(codigo_produto, descricao_produto, estoque): ("
                        /*+ id_Produtos + ","*/
                        + codigo_produto + ","
                        + descricao_produto + ","
                        + estoque + ")";
                Log.d(TAG, "ParseStringBuilder: Dados da linha: " + cellInfo);

                /*double id = Double.parseDouble(*//*id_Produtos*//*);*/
                double estq = Double.parseDouble(estoque.replace(",", "."));

//                Adcionar ao uploadDados ArrayList
//                enquanto uploadDados < rows add
                uploadDados.add(new ProdutoModel(/*Integer.parseInt(new DecimalFormat("#").format(id)),*/
                        codigo_produto, descricao_produto, estq));

            } catch (NumberFormatException e) {

                Log.e(TAG, "parseStringBuilder: NumberFormatException: " + e.getMessage());
            }
        }

        importarDados();
    }

    private void importarDados() {
        Log.d(TAG, "importarDados: Imprimindo dados no log...");
        limparBanco();

//        rows.lenght retorna uma posicao a mais pois a primeira linha é a posicao 0.
        contar_linhas--;
        for (int i = 0; i < contar_linhas; i++) {

            /*Integer id_Produtos = uploadDados.get(i).getId_Produtos();*/
            String codigo_produto = uploadDados.get(i).getCodigo_produto();
            String descricao_produto = uploadDados.get(i).getDescricao_produto();
            double estoque = uploadDados.get(i).getEstoque();

            Log.d(TAG, "importarDados: (codigo_produto, descricao_produto, estoque): ("
                    /*+ id_Produtos + ","*/
                    + codigo_produto + ","
                    + descricao_produto + ","
                    + estoque + ")");

            /*produtoModel.setId_Produtos(id_Produtos);*/
            produtoModel.setCodigo_produto(codigo_produto);
            produtoModel.setDescricao_produto(descricao_produto);
            produtoModel.setEstoque(estoque);

            System.out.println(descricao_produto + " SALVANDO!!!!");
            controller_conexao.importToSqlite(getActivity(), produtoModel);
        }

        zerarContagem(); // Seta qtd_inventario como 0. importante para os itens nao coletados aparecerem na lista.
    }

    /**
     * Retorna a célula como uma stirng do arquivo excel
     *
     * @param row
     * @param c
     * @param formulaEvaluator
     * @return
     */
    private String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator) {
        String value = "";
        try {
            Cell cell = row.getCell(c);
            CellValue cellValue = formulaEvaluator.evaluate(cell);

            switch (cellValue.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    value = "" + cellValue.getBooleanValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    double numericValue = cellValue.getNumberValue();
                    if (HSSFDateUtil.isCellDateFormatted(cell)) {
                        double date = cellValue.getNumberValue();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter =
                                new SimpleDateFormat("dd/MM/yy");
                        value = formatter.format(HSSFDateUtil.getJavaDate(date));
                    } else {
                        value = "" + numericValue;
                    }
                    break;
                case Cell.CELL_TYPE_STRING:
                    value = "" + cellValue.getStringValue();
                    break;
                default:
            }
        } catch (NullPointerException e) {

            Log.e(TAG, "getCellAsString: NullPointerException: " + e.getMessage());
        }
        return value;
    }

    public void limparBanco() {
        SingletonProduto.getProduto().setListaProdutos(null);
        SingletonProduto.getProduto().setListaCarrinho(null);

        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.limparBanco(getActivity());
    }

    public void zerarContagem() {
        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.zerarContagem(getActivity());
    }

    //    TODO Observação: Avaliar se o app precisa declarar permissões
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checarPermissoesArquivos() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

//            Determinar se o app já recebeu a permissão
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1003);
            }
        } else {
            Log.d(TAG, "checarPermissoesArquivos: Não precisa checar as permissões. Versão do SDK < LOLLIPOP.");
        }
    }
}
