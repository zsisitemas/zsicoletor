package com.zsisistemas.zsiempresa.view.activities;

import android.content.Context;
import android.content.Intent;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ProgressBarAnimation extends Animation {

    private Context context;
    private ProgressBar progressBarImportExport;
    private TextView textoProgressBarImportExport;
    private float from;
    private float to;

    public ProgressBarAnimation(Context context, ProgressBar progressBarImportExport, TextView textoProgressBarImportExport, float from, float to) {
        this.context = context;
        this.progressBarImportExport = progressBarImportExport;
        this.textoProgressBarImportExport = textoProgressBarImportExport;
        this.from = from;
        this.to = to;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t){
        super.applyTransformation(interpolatedTime, t);
        float value = from + (to - from) * interpolatedTime;
        progressBarImportExport.setProgress((int) value);
        textoProgressBarImportExport.setText((int) value +" %");

        if(value == to){
            context.startActivity(new Intent(context, MainActivity.class));
        }
    }
}
