package com.zsisistemas.zsiempresa.view.ui.carrinho;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.Cupom_itemModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom_item;
import com.zsisistemas.zsiempresa.view.activities.RecyclerItemClickListener;
import com.zsisistemas.zsiempresa.view.adapter.AdapterPedidos;

import java.util.ArrayList;
import java.util.List;

public class ProdutosPedidosFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<Cupom_itemModel> listaPedido = new ArrayList<>();
    private ProgressDialog progressNoDialog;

    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";
    CarrinhoFragment carrinhoFragment;
    ProdutosCarrinhoFragment produtosCarrinhoFragment;
    AdapterPedidos adapterPedidos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pedidos, container, false);

        recyclerView = view.findViewById(R.id.recycler_view_pedidos);
        carrinhoFragment = new CarrinhoFragment();
        produtosCarrinhoFragment = new ProdutosCarrinhoFragment();

        Util.recuperaDadosCadastrados(getActivity(), ARQUIVO_PREFERENCIA);

        progressNoDialog = new ProgressDialog(getActivity());
        progressNoDialog.show();
        progressNoDialog.setCancelable(false);
        progressNoDialog.setContentView(R.layout.progress_no_dialog);
        progressNoDialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent
        );

        new Thread() {
            @Override
            public void run() {
                listarPedido(getActivity());
                progressNoDialog.dismiss();

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        // Configurar Adapter
                        listaPedido = SingletonCupom_item.getCupom_Item().getListaPedido();

                        adapterPedidos = new AdapterPedidos( listaPedido );

                        adapterPedidos.notifyDataSetChanged();

                        // Configurar RecyclerView
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter( adapterPedidos );

                    }
                });
            }
        }.start();

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getActivity().getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Cupom_itemModel cupom_itemModel = listaPedido.get(position);

                                progressNoDialog = new ProgressDialog(getActivity());
                                progressNoDialog.show();
                                progressNoDialog.setCancelable(false);
                                progressNoDialog.setContentView(R.layout.progress_no_dialog);
                                progressNoDialog.getWindow().setBackgroundDrawableResource(
                                        android.R.color.transparent
                                );

                                new Thread() {
                                    @Override
                                    public void run() {

                                        metodosStatus(cupom_itemModel.getNumeroMesa());

                                        if(SingletonCupom_item.getCupom_Item().isStatusPedido()){
                                            progressNoDialog.dismiss();

                                            getActivity().runOnUiThread(new Runnable() {
                                                public void run() {

                                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                                    builder.setTitle("Oops! Pedido finalizado/cancelado")
                                                            .setMessage("Impossível continuar! Este pedido será descartado!")
                                                            .setCancelable(false)
                                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    listaPedido.remove(cupom_itemModel);
                                                                    adapterPedidos.notifyDataSetChanged();

                                                                    SharedPreferences preferencesPedido = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                                                                    SharedPreferences.Editor editor = preferencesPedido.edit();

                                                                    Util.limparBanco(getActivity());
                                                                    produtosCarrinhoFragment.listaCarrinho.clear();
                                                                    produtosCarrinhoFragment.adapterCarrinho.notifyDataSetChanged();

                                                                    carrinhoFragment.idCliente.setText(null);
                                                                    carrinhoFragment.nomeCliente.setText(null);
                                                                    carrinhoFragment.textoDefault.setText("não selecionado!");

                                                                    String idCli = null;
                                                                    String nomeCli = null;
                                                                    String telefoneCli = null;
                                                                    int numeroMesa = 0;
                                                                    String obs = null;

                                                                    editor.putString("idCli", idCli);
                                                                    editor.putString("nomeCli", nomeCli);
                                                                    editor.putString("telefoneCli", telefoneCli);
                                                                    editor.putInt("numeroMesa", numeroMesa);
                                                                    editor.putString("obs", obs);
                                                                    editor.apply();

                                                                    SingletonCliente.getCliente().setNomeClienteSelecionado(nomeCli);

                                                                    String pedido = "false";
                                                                    editor.putString("pedido", pedido);
                                                                    editor.apply();
                                                                }
                                                            })
                                                            .show();
                                                }
                                            });
                                        } else {
                                            metodos(cupom_itemModel);
                                            progressNoDialog.dismiss();

                                            getActivity().runOnUiThread(new Runnable() {
                                                public void run() {
                                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                                    transaction.replace(R.id.frame_conteudo, produtosCarrinhoFragment);
                                                    transaction.commit();

                                                    carrinhoFragment.botaoPedidos.setTextColor(Color.parseColor("#CCD6D7D7"));
                                                    carrinhoFragment.botaoCarrinho.setTextColor(Color.parseColor("#FFFFFF"));

                                                    carrinhoFragment.viewPedidos.setBackgroundColor(Color.parseColor("#00000000"));
                                                    carrinhoFragment.viewCarrinho.setBackgroundColor(Color.parseColor("#FFFFFF"));

                                                    SharedPreferences preferencesIdCliente = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                                                    SharedPreferences.Editor editor = preferencesIdCliente.edit();

//                                                String idCli = String.valueOf(cupom_itemModel.getComanda());
                                                    String idCli = String.valueOf(cupom_itemModel.getComanda());
                                                    String nomeCli = cupom_itemModel.getNomeCliete();
                                                    String telefoneCli = cupom_itemModel.getFone();
                                                    int numeroMesa = cupom_itemModel.getNumeroMesa();

                                                    editor.putString("idCli", idCli);
                                                    editor.putString("nomeCli", nomeCli);
                                                    editor.putString("telefoneCli", telefoneCli);
                                                    editor.putInt("numeroMesa", numeroMesa);
                                                    editor.apply();

                                                    SingletonCliente.getCliente().setIdClienteSelecionado(Integer.parseInt(idCli));
                                                    SingletonCliente.getCliente().setNomeClienteSelecionado(nomeCli);
                                                    SingletonCliente.getCliente().setTelefoneClienteSelecionado(telefoneCli);
                                                    SingletonCupom_item.getCupom_Item().setNumeroMesa(numeroMesa);

                                                    carrinhoFragment.textoDefault.setText("");
                                                    carrinhoFragment.idCliente.setText(idCli);
                                                    carrinhoFragment.nomeCliente.setText(" - " + nomeCli);
                                                }
                                            });
                                        }
                                    }
                                }.start();
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );

        return view;
    }

    public void listarPedido(Context ctx){

        // Evitar repetir esse mesmo codigo nas classes AberturaActivity, ProdutosCarrinhoFragment e em outras futuras utilizações,
        // Se chamar esse metodo em outra classe sem passar context como parametro vai quebrar, porque getActivity retorna nulo quando
        // a classe que ta utilizando-o nao foi iniciada.
        if (getActivity() != null) {
            ctx = getActivity();
        }

        SingletonCupom_item.getCupom_Item().setListaPedido(null);
        Controller_Conexao controller_conexao = new Controller_Conexao();

        SharedPreferences preferencesLoginFuncionario = ctx.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        String funLogado = "";

        if (preferencesLoginFuncionario.contains("loginFun")){
            String loginFun = preferencesLoginFuncionario.getString("loginFun", null);

            SingletonCliente.getCliente().setLoginFuncionario(loginFun);

            funLogado = SingletonCliente.getCliente().getLoginFuncionario();
        } else {
            SingletonCliente.getCliente().setLoginFuncionario(null);
        }

        SharedPreferences preferencesPerfilFun = ctx.getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        if(preferencesPerfilFun.contains("perfilFun")){
            String perfilFun = preferencesPerfilFun.getString("perfilFun", "perfilFun não definido");

            SingletonCliente.getCliente().setPerfil(perfilFun);

            if(perfilFun.equalsIgnoreCase("admin")
                || perfilFun.equalsIgnoreCase("dev")){
                controller_conexao.listarCupom(ctx.getApplicationContext());
            } else {
                controller_conexao.listarCupomFun(ctx.getApplicationContext(), funLogado);
            }
        } else {
            controller_conexao.listarCupomFun(ctx.getApplicationContext(), funLogado);
        }
    }

    public void metodos(Cupom_itemModel cupom_itemModel){
        Controller_Conexao controller_conexao = new Controller_Conexao();
        Util.limparBanco(getContext());
        controller_conexao.listaItensPedidos(getActivity(), cupom_itemModel);
    }

    public void metodosStatus(int numeroMesa){
        Controller_Conexao controller = new Controller_Conexao();

        SingletonCupom_item.getCupom_Item().setListaPedido(null);

        controller.verificarStatusPedido(getActivity(), numeroMesa);

    }
}