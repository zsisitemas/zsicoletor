package com.zsisistemas.zsiempresa.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.model.ProdutoModel;

import java.util.List;

public class AdapterColetados extends RecyclerView.Adapter<AdapterColetados.MyViewHolder> {

    // Lista com produtos coletados
    private List<ProdutoModel> listaColetados;

    public AdapterColetados(List<ProdutoModel> lista) {
        this.listaColetados = lista;
    }

    @NonNull
    @Override
    public AdapterColetados.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mainCardViewLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cartao_recycler_coletado, parent, false);
        // Define o layout do item
        return new AdapterColetados.MyViewHolder(mainCardViewLista);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterColetados.MyViewHolder holder, int position) {
        // Seta os dados do produto para exibição
        ProdutoModel produto = listaColetados.get( position );

        holder.descricao_produto.setText(produto.getDescricao_produto());
        holder.codigo_produto.setText(String.valueOf("Código: " + produto.getCodigo_produto()));
        holder.status.setText("Status: Coletado");

    }

    @Override
    public int getItemCount() {
        // Define a quantidade de itens na RecyclerView
        // Retornar tamanho da lista se não for nula
        return listaColetados != null ? listaColetados.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        // Criar objetos para exibir os itens mantendo a visualização
        // Cada item é um objeto da MyViewHolder

        TextView descricao_produto;
        TextView codigo_produto;
        TextView status;

        public MyViewHolder(View itemView){
            super(itemView);

            descricao_produto = itemView.findViewById(R.id.text_descricao);
            codigo_produto = itemView.findViewById(R.id.text_codigo);
            status = itemView.findViewById(R.id.text_status);
        }
    }
}
