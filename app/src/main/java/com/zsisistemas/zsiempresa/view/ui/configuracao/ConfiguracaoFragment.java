package com.zsisistemas.zsiempresa.view.ui.configuracao;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.view.activities.AberturaActivity;

import java.util.Calendar;

import static android.text.InputType.TYPE_NULL;
import static com.zsisistemas.zsiempresa.controller.Util.MD5_Hash;
import static com.zsisistemas.zsiempresa.controller.Util.closeKeyboard;
import static com.zsisistemas.zsiempresa.controller.Util.validarConfigs;

public class ConfiguracaoFragment extends Fragment {

    private EditText editarIp, editarNomeBanco, editarRoot, editarChave, editarValidade;
    private EditText inputIp, inputNomeBanco, inputRoot, inputChave;
    private TextView textoLogar, textoNomeUsuario, textoLogin, textoPerfil;
    private ImageButton botaoCancelLicenca, botaoCheckLicenca;
    private RadioGroup radioGroup;
    private RadioButton radioMysqlNuvem, radioMysqlLocal, radioSqlite;
    private String conn;
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_configuracao, container, false);

        editarIp = view.findViewById(R.id.edit_ip);
        editarNomeBanco = view.findViewById(R.id.edit_nome_banco);
        editarRoot = view.findViewById(R.id.edit_root);
        editarChave = view.findViewById(R.id.id_campoChave);
        editarValidade = view.findViewById(R.id.id_campoValidade);
        textoLogar = view.findViewById(R.id.textLogar);
        textoNomeUsuario = view.findViewById(R.id.textUserName);
        textoLogin = view.findViewById(R.id.textLogin);
        textoPerfil = view.findViewById(R.id.textPerfil);
        botaoCancelLicenca = view.findViewById(R.id.imageButton_cancel);
        botaoCheckLicenca = view.findViewById(R.id.imageButton_check);
        radioGroup = view.findViewById(R.id.radio_tipo_conexao);
        radioMysqlNuvem = view.findViewById(R.id.mysql_nuvem);
        radioMysqlLocal = view.findViewById(R.id.mysql_local);
        radioSqlite = view.findViewById(R.id.sqlite);

        Util.recuperaDadosCadastrados(getActivity(), ARQUIVO_PREFERENCIA);

        textoLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validarConfigs(getActivity(), ARQUIVO_PREFERENCIA)) {
                    Toast.makeText(getActivity(), "Primeiro ajuste as configurações", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(getActivity(), AberturaActivity.class);
                    startActivity(intent);
                }
            }
        });

        if(SingletonConfiguracoes.getConfig().getConnection() != null){
            conn = SingletonConfiguracoes.getConfig().getConnection(); //   Default = 0;
        }

        if(conn.equals("0")){
            radioMysqlNuvem.setChecked(true);
        }
        if(conn.equals("2")){
            radioMysqlLocal.setChecked(true);
        }
        if(conn.equals("3")){
            radioSqlite.setChecked(true);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                closeKeyboard(view, getActivity());

                SharedPreferences preferencesConn = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                SharedPreferences.Editor editor = preferencesConn.edit();

                switch (checkedId){
                    case R.id.mysql_nuvem:
                        String conn = "0";
                        editor.putString("conn", conn);
                        editor.apply();

                        SingletonConfiguracoes.getConfig().setConnection(conn);
                        break;
                    case R.id.mysql_local:
                        conn = "2";
                        editor.putString("conn", conn);
                        editor.apply();

                        SingletonConfiguracoes.getConfig().setConnection(conn);
                        break;
                    case R.id.sqlite:
                        conn = "3";
                        editor.putString("conn", conn);
                        editor.apply();

                        SingletonConfiguracoes.getConfig().setConnection(conn);
                        break;
                }
            }
        });

        tipoInputNull();

        editarIp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builderIp = new AlertDialog.Builder(getActivity());
                builderIp.setTitle("Ip local");
                inputIp = new EditText(getActivity());
                builderIp.setView(inputIp);
                inputIp.setText(editarIp.getText().toString());
                inputIp.setSelectAllOnFocus(true);
                builderIp.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences preferencesIp = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                        SharedPreferences.Editor editor = preferencesIp.edit();

                        // Validar ip
                        if (!inputIp.getText().toString().trim().isEmpty()) {
                            editarIp.setText(inputIp.getText().toString().trim());

                            String ip = editarIp.getText().toString().trim();
                            editor.putString("ip", ip);
                            editor.commit();

                            SingletonConfiguracoes.getConfig().setIp(ip);

                            editarIp.setText(ip);

                        }
                    }
                })
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            }
        });

        editarNomeBanco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builderNomeBanco = new AlertDialog.Builder(getActivity());
                builderNomeBanco.setTitle("Nome do banco");
                inputNomeBanco = new EditText(getActivity());
                builderNomeBanco.setView(inputNomeBanco);
                inputNomeBanco.setText(editarNomeBanco.getText().toString());
                inputNomeBanco.setSelectAllOnFocus(true);

                inputNomeBanco.setText("u323187073_");

                builderNomeBanco.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences preferencesNomeBanco = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                        SharedPreferences.Editor editor = preferencesNomeBanco.edit();

                        // Validar conn
                        if (!inputNomeBanco.getText().toString().trim().isEmpty()) {
                            editarNomeBanco.setText(inputNomeBanco.getText().toString().trim());

                            String nome = editarNomeBanco.getText().toString();
                            editor.putString("nome", nome);
                            editor.commit();

                            SingletonConfiguracoes.getConfig().setNomeBanco(nome);

                            editarNomeBanco.setText(nome);
                        }
                    }
                })
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            }
        });

        editarChave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builderChave = new AlertDialog.Builder(getActivity());
                builderChave.setTitle("Chave");
                inputChave = new EditText(getActivity());
                builderChave.setView(inputChave);
                inputChave.setText(editarChave.getText().toString());
                inputChave.setSelectAllOnFocus(true);

              //  inputChave.setText("8dafc750ffde6d3bc93b264707995fe9");
                builderChave.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editarChave.setText(inputChave.getText().toString().trim());
                    }
                })
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            }
        });

        editarRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builderRoot = new AlertDialog.Builder(getActivity());
                builderRoot.setTitle("Nome do usuário");
                inputRoot = new EditText(getActivity());
                builderRoot.setView(inputRoot);
                inputRoot.setText(editarRoot.getText().toString());
                inputRoot.setSelectAllOnFocus(true);
                builderRoot.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences preferencesRoot = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                        SharedPreferences.Editor editor = preferencesRoot.edit();

                        // Validar root
                        if (!inputRoot.getText().toString().trim().isEmpty()) {
                            editarRoot.setText(inputRoot.getText().toString());

                            String root = editarRoot.getText().toString();
                            editor.putString("root", root);
                            editor.apply();

                            SingletonConfiguracoes.getConfig().setRoot(root);

                            editarRoot.setText(root);

                        }
                    }
                })
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            }
        });

        editarValidade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String chaveAtual = editarChave.getText().toString().trim();
                if(chaveAtual.isEmpty()){
                    Toast.makeText(getActivity(), "Informe a chave!", Toast.LENGTH_SHORT).show();
                } else {
                    if(SingletonConfiguracoes.getConfig().getValidade() != null){

                        String dataSelecionada = SingletonConfiguracoes.getConfig().getValidade();
                        String s[] = dataSelecionada.split("-");

                        int mYear = Integer.parseInt(s[2]);
                        int mMonth = Integer.parseInt(s[1]);
                        int mDay = Integer.parseInt(s[0]);
                        mMonth--;

                        DatePickerDialog dateDialog= new DatePickerDialog(getActivity(), datePickerListener, mYear, mMonth, mDay);
                        dateDialog.show();

                    } else {

                        final Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR);
                        int mMonth = c.get(Calendar.MONTH);
                        int mDay = c.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog dateDialog= new DatePickerDialog(getActivity(), datePickerListener, mYear, mMonth, mDay);
                        dateDialog.show();

                    }
                }
            }
        });

        // Recuperar dados(ip) salvos no dispositivo
        SharedPreferences preferencesIp = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        // Validar se temos o ip preferencias
        if (preferencesIp.contains("ip")){
            String ip = preferencesIp.getString("ip", "IP não definido");

            SingletonConfiguracoes.getConfig().setIp(ip); // Setar dados salvos do celular no ipConfigurações
            editarIp.setText(ip);
        }else{
            editarIp.setText(null);
        }

        // Recuperar dados(nome do banco) salvos no dispositivo
        SharedPreferences preferencesNome = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        // Validar se temos o nome preferências
        if (preferencesNome.contains("nome")){
            String nome = preferencesNome.getString("nome", "Nome do banco não informado");

            SingletonConfiguracoes.getConfig().setNomeBanco(nome); // Setar dados salvos do celular no nomeConfigurações
            editarNomeBanco.setText(nome);
        }else{
            editarNomeBanco.setText(null);
        }

        // Recuperar dados(root) salvos no dispositivo
        SharedPreferences preferencesRoot = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        // Validar se temos o root preferências
        if (preferencesRoot.contains("root")){
            String root = preferencesRoot.getString("root", "root não informado");

            SingletonConfiguracoes.getConfig().setRoot(root); // Setar dados salvos do celular no rootConfigurações
            editarRoot.setText(root);
        }else{
            editarRoot.setText(null);
        }

        // Recuperar dados(validade da licença) salvos no dispositivo
        SharedPreferences preferencesLicenca = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        // Validar se temos o nome preferências
        if (preferencesLicenca.contains("chave") &&
                preferencesLicenca.contains("validade")){
            String chave = preferencesLicenca.getString("chave", "chave nao informada");
            String validade = preferencesLicenca.getString("validade", "validade nao informada");

            SingletonConfiguracoes.getConfig().setChave(chave); // Setar dados salvos do celular no chaveConfigurações
            SingletonConfiguracoes.getConfig().setValidade(validade); // Setar dados salvos do celular no validadeConfigurações

            editarChave.setText(chave);
            editarValidade.setText(validade);

            botaoCancelLicenca.setVisibility(View.INVISIBLE);
            botaoCheckLicenca.setVisibility(View.VISIBLE);
        }else{
            editarChave.setText(null);
            editarValidade.setText(null);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Verificar o nome do usuário logado.
        SharedPreferences preferencesNomeUsuario = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if (preferencesNomeUsuario.contains("nomeUsuario")) {
            String nomeUsuario = preferencesNomeUsuario.getString("nomeUsuario", null);

            SingletonCliente.getCliente().setNome(nomeUsuario);
        } else {
            SingletonCliente.getCliente().setNome(null);
        }

        // Verificar se usuário está logado ou não.
        SharedPreferences preferencesLoginFuncionario = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if (preferencesLoginFuncionario.contains("loginFun")) {
            String loginFun = preferencesLoginFuncionario.getString("loginFun", null);

            SingletonCliente.getCliente().setLoginFuncionario(loginFun);
        } else {
            SingletonCliente.getCliente().setLoginFuncionario(null);
        }

        SharedPreferences preferencesPerfilFun = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        String perfil = null;
        if (preferencesPerfilFun.contains("perfilFun")) {
            String perfilFun = preferencesPerfilFun.getString("perfilFun", "perfilFun não definido");

            SingletonCliente.getCliente().setPerfil(perfilFun);
            if (perfilFun.equalsIgnoreCase("admin")) {
                perfil = "administrador";
            } else {
                if (perfilFun.equalsIgnoreCase("funcionario")) {
                    perfil = "funcionário";
                } else {
                    if (perfilFun.equalsIgnoreCase("dev")) {
                        perfil = "desenvolvedor";
                    } else {
                        if (perfilFun.equalsIgnoreCase("vendedor")) {
                            perfil = "vendedor";
                        }
                    }

                }
            }
        }

        if (SingletonCliente.getCliente().getLoginFuncionario() != null) {
            textoNomeUsuario.setText(SingletonCliente.getCliente().getNome());
            textoNomeUsuario.setVisibility(View.VISIBLE);

            textoLogin.setText("Login: " + SingletonCliente.getCliente().getLoginFuncionario());
            textoLogin.setVisibility(View.VISIBLE);

            if (!perfil.isEmpty()) {
                textoPerfil.setText("Perfil: " + perfil);
                textoPerfil.setVisibility(View.VISIBLE);
            }

            textoLogar.setVisibility(View.GONE);
        }
    }

    public void tipoInputNull(){
        editarIp.setFocusable(false);
        editarIp.setFocusableInTouchMode(false);
        editarIp.setInputType(TYPE_NULL);

        editarNomeBanco.setFocusable(false);
        editarNomeBanco.setFocusableInTouchMode(false);
        editarNomeBanco.setInputType(TYPE_NULL);

        editarChave.setFocusable(false);
        editarChave.setFocusableInTouchMode(false);
        editarChave.setInputType(TYPE_NULL);

        editarValidade.setFocusable(false);
        editarValidade.setFocusableInTouchMode(false);
        editarValidade.setInputType(TYPE_NULL);

        editarRoot.setFocusable(false);
        editarRoot.setFocusableInTouchMode(false);
        editarRoot.setInputType(TYPE_NULL);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            monthOfYear++;

            String mes = ""+monthOfYear;
            String dia = ""+dayOfMonth;

            if(monthOfYear < 10){
                mes = "0"+monthOfYear;
            }

            if(dayOfMonth < 10){
                dia = "0"+dayOfMonth;
            }

            String dateYouChoosed = year + "-" + mes + "-" + dia;

            SharedPreferences preferencesLicenca = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
            SharedPreferences.Editor editor = preferencesLicenca.edit();

            // Licença
            String chave = editarChave.getText().toString();
            // Data
            String validade = MD5_Hash(Util.formatarDataBr(dateYouChoosed));

            if(chave.equals(validade)){
                editor.putString("chave", chave);
                editor.apply();
                SingletonConfiguracoes.getConfig().setChave(chave);

                validade = Util.formatarDataBr(dateYouChoosed);
                editor.putString("validade", validade);
                editor.commit();
                SingletonConfiguracoes.getConfig().setValidade(validade);

                editarValidade.setText(Util.formatarDataBr(dateYouChoosed));
                botaoCancelLicenca.setVisibility(View.INVISIBLE);
                botaoCheckLicenca.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(), "Licença inserida com sucesso!", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getActivity(), "Chave ou licença inválida!", Toast.LENGTH_LONG).show();
            }

        }
    };
}
