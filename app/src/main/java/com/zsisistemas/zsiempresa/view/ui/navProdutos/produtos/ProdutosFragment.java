package com.zsisistemas.zsiempresa.view.ui.navProdutos.produtos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;
import com.zsisistemas.zsiempresa.view.activities.RecyclerItemClickListener;
import com.zsisistemas.zsiempresa.view.adapter.AdapterProdutos;

import java.util.ArrayList;
import java.util.List;

import static com.zsisistemas.zsiempresa.view.ui.navProdutos.produtos.NavProdutosFragment.fab;

public class ProdutosFragment extends Fragment{

    private EditText inputQuantidade;
    private RecyclerView recyclerView;
    public static List<ProdutoModel> listaProdutos = new ArrayList<>();
    public static List<ProdutoModel> listaProdutosFiltrados = new ArrayList<>();
    public static AdapterProdutos adapterProdutos;
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";
//    private ProdutoModel produtoModel = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_produtos, container, false);

        Util.recuperaDadosCadastrados(getActivity(), ARQUIVO_PREFERENCIA);

//        SharedPreferences preferencesProdutoModel = getActivity().getPreferences(MODE_PRIVATE);
//
//        if(preferencesProdutoModel.contains("produtoPrefs")){
//
//            Gson gsonGet = new Gson();
//            String json = preferencesProdutoModel.getString("produtoPrefs", "");
//            ProdutoModel produtoModel1 = gsonGet.fromJson(json, ProdutoModel.class);
//
//            if(produtoModel1.getStatus() == 1){
//                produtoModel = produtoModel1;
//
//            }else{
//                produtoModel = null;
//            }
//
//        }

        recyclerView = view.findViewById(R.id.recycler_view_produtos);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0){
                    fab.hide();
                } else {
                    fab.show();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        // Listagem de Produtos
        if(SingletonConfiguracoes.getConfig().getConnection().equals("3")){
            if(SingletonProduto.getProduto().getListaProdutos() == null){
                this.listarProdutos();
            }
        }

        // Configurar Adapter
        listaProdutos = SingletonProduto.getProduto().getListaProdutos();
        listaProdutosFiltrados.clear();
        if(listaProdutos != null) {
            listaProdutosFiltrados.addAll(listaProdutos);
        }
        this.adapterProdutos = new AdapterProdutos( listaProdutosFiltrados );

        // Configurar RecyclerView
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter( adapterProdutos );
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        // Evento de clique no item do recycler
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getActivity().getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Controller_Conexao controller_conexao = new Controller_Conexao();

                                ProdutoModel produtoModel = listaProdutosFiltrados.get(position);

                                //TODO guardar instancia no sharedpreferences para verificar se vai atualizar ou inserir nova instancia.
//
//                                ProdutoModel produtoModelAtualizar = listaProdutosFiltrados.get(position);
//
//                                SharedPreferences preferencesProdutoModel = getActivity().getPreferences(MODE_PRIVATE);
//                                SharedPreferences.Editor editor = preferencesProdutoModel.edit();
//
//                                Gson gson = new Gson();
//                                String produtoPrefs = gson.toJson(produtoModelAtualizar);
//
//                                editor.putString("produtoPrefs", produtoPrefs);
//                                editor.apply();

//                                if(produtoModel.getEstoque() < 1){
//                                    Toast.makeText(getActivity(), "Sem estoque!" ,Toast.LENGTH_SHORT).show();
//                                } else {

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Quantidade");

                                inputQuantidade = new EditText(getActivity());
                                inputQuantidade.setInputType(InputType.TYPE_CLASS_NUMBER);
                                builder.setView(inputQuantidade);
                                inputQuantidade.setText("1");
                                inputQuantidade.requestFocus();

                                int maxLength = 3;
                                InputFilter[] fArray = new InputFilter[1];
                                fArray[0] = new InputFilter.LengthFilter(maxLength);
                                inputQuantidade.setFilters(fArray);

                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

//                                            if(preferencesProdutoModel.contains("produtoPrefs")){
//
//                                                Gson gsonGet = new Gson();
//                                                String json = preferencesProdutoModel.getString("produtoPrefs", "");
//                                                ProdutoModel produtoModel1 = gsonGet.fromJson(json, ProdutoModel.class);
//
//                                                if(produtoModel1.getStatus() == 1){
//                                                    produtoModel = produtoModel1;
//
//                                                    produtoModel.setQuantidadeProdutos(produtoModel.getQuantidadeProdutos());
//
//                                                }else{
//                                                    produtoModel = null;
//                                                }
//
//                                            }

                                        String quantidade = inputQuantidade.getText().toString().trim();
                                        double qtd;

                                        if (quantidade.isEmpty()) {
                                            qtd = 1;
                                        } else {
                                            qtd = Integer.parseInt(quantidade);
                                        }

                                        if(qtd == 0){
                                            qtd = 1;
                                        }
//
//                                            if (qtd > 0) {
//                                                if(qtd <= produtoModel.getEstoque()){

                                        //TODO produtoModel = nulo cria nova instancia e insere senao apenas atualiza o que ja tem no preferences.
//                                                    if (produtoModel != null){ // Atualiza
//
//                                                        controller_conexao.atualizaProdutoPorCodigo(getActivity(), produtoModel);
//
//                                                        Toast.makeText(getActivity(), produtoModel.getQuantidadeProdutos() + " <- Atualizado!", Toast.LENGTH_SHORT).show();
//                                                    } else { // Inserir

//                                                        produtoModel = listaProdutosFiltrados.get(position);

                                        produtoModel.setQuantidadeProdutos(qtd);

//                                                        produtoModel.setStatus(1); // 1 - ja no carrinho 0 - nao ta no carrinho

                                        controller_conexao.salvarPedidoCarrinhoTemporariamente(getActivity(), produtoModel);

                                        Toast.makeText(getActivity(), produtoModel.getDescricao_produto() + " Adicionado ao carrinho!", Toast.LENGTH_SHORT).show();

//                                                    }

//                                                }else {
//                                                    Toast.makeText(getActivity(), " Estoque insuficiente!" ,Toast.LENGTH_SHORT).show();
//                                                }
//
//                                            }

                                    }
                                })
                                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        })
                                        .show();
//                                }
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );

        return view;
    }

    public void listarProdutos(){
        Controller_Conexao controller = new Controller_Conexao();
        controller.listarProdutos(getActivity());
    }
}
