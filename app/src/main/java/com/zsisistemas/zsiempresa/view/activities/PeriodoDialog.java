package com.zsisistemas.zsiempresa.view.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;

import java.util.Calendar;

public class PeriodoDialog extends DialogFragment {

    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);
    private String dataIselecionada;
    private String dataFselecionada;
    LucrosActivity lucrosActivity = new LucrosActivity();
    DatePickerDialog dataInicio;
    DatePickerDialog dataFim;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        if(SingletonConfiguracoes.getConfig().getDataInicio() != null){

            dataIselecionada = SingletonConfiguracoes.getConfig().getDataInicio();
            String s[] = dataIselecionada.split("-");

            int ano = Integer.parseInt(s[0]);
            int mes = Integer.parseInt(s[1]);
            int dia = Integer.parseInt(s[2]);
            mes--;

            dataInicio = new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), ano, mes, dia);

        } else {
            return new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);
        }

        if(SingletonConfiguracoes.getConfig().getDataFim() != null){

            dataFselecionada = SingletonConfiguracoes.getConfig().getDataFim();
            String s[] = dataFselecionada.split("-");

            int ano = Integer.parseInt(s[0]);
            int mes = Integer.parseInt(s[1]);
            int dia = Integer.parseInt(s[2]);
            mes--;

            dataFim = new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), ano, mes, dia);

        } else {
            return new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);
        }

        if(lucrosActivity.inicioFim == true){
            return dataInicio;
        } else {
            return dataFim;
        }
    }
}
