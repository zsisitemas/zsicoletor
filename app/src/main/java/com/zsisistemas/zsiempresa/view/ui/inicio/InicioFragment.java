package com.zsisistemas.zsiempresa.view.ui.inicio;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.view.activities.LucrosActivity;

import static com.zsisistemas.zsiempresa.controller.Util.validarConfigs;

public class InicioFragment extends Fragment {

    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inicio, container, false);

        SharedPreferences preferencesIp = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if (preferencesIp.contains("ip")){
            String ip = preferencesIp.getString("ip", "IP não definido");
            SingletonConfiguracoes.getConfig().setIp(ip);
        }

        SharedPreferences preferencesLoginFuncionario = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if (preferencesLoginFuncionario.contains("loginFun")){
            String loginFun = preferencesLoginFuncionario.getString("loginFun", null);

            SingletonCliente.getCliente().setLoginFuncionario(loginFun);
        } else {
            SingletonCliente.getCliente().setLoginFuncionario(null);
        }

        if(!validarConfigs(getActivity(), ARQUIVO_PREFERENCIA)) {
            Toast.makeText(getActivity(), "Ajuste as configurações", Toast.LENGTH_LONG).show();
        } else {
            if (SingletonCliente.getCliente().getLoginFuncionario() == null){
                Toast.makeText(getActivity(), "Faça login", Toast.LENGTH_LONG).show();
            }
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_lucros, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_lucros:
                Intent intent = new Intent(getContext(), LucrosActivity.class);
                startActivity(intent);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences preferencesPerfilFun = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        if(preferencesPerfilFun.contains("perfilFun")){
            String perfilFun = preferencesPerfilFun.getString("perfilFun", "perfilFun não definido");

            SingletonCliente.getCliente().setPerfil(perfilFun);

            setHasOptionsMenu(true);
        } else {
            setHasOptionsMenu(false);
        }
    }
}