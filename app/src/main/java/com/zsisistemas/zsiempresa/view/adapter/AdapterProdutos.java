package com.zsisistemas.zsiempresa.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.model.ProdutoModel;

import java.util.List;

public class AdapterProdutos extends RecyclerView.Adapter<AdapterProdutos.MyViewHolder> {

    // Lista com todos os produtos
    private List<ProdutoModel> listaProdutos;

    public AdapterProdutos(List<ProdutoModel> lista) {
        this.listaProdutos = lista;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mainCardViewLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cartao_recycler_produto, parent, false);
        // Define o layout do item
        return new MyViewHolder(mainCardViewLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // Seta os dados do produto para exibição
        ProdutoModel produto = listaProdutos.get( position );
        holder.descricao_produto.setText( produto.getDescricao_produto());
        holder.quantidade_estoque.setText( String.valueOf(produto.getEstoque()));
        holder.preco_venda.setText( String.valueOf(produto.getPreco_venda()));
        holder.id.setText(String.valueOf(produto.getId_Produtos()));
        holder.codigo.setText(produto.getCodigo_produto());
    }

    @Override
    public int getItemCount() {
        // Define a quantidade de itens na RecyclerView
        // Retornar tamanho da lista se não for nula
        return listaProdutos != null ? listaProdutos.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        // Criar objetos para exibir os itens mantendo a visualização
        // Cada item é um objeto da MyViewHolder

        TextView descricao_produto;
        TextView quantidade_estoque;
        TextView preco_venda;
        TextView id;
        TextView codigo;
        ImageButton botaoMais;

        public MyViewHolder(View itemView){
            super(itemView);

            descricao_produto = itemView.findViewById(R.id.text_descricao);
            quantidade_estoque = itemView.findViewById(R.id.text_estoque);
            preco_venda = itemView.findViewById(R.id.text_preco);
            botaoMais = itemView.findViewById(R.id.imageButton_more);
            id = itemView.findViewById(R.id.idProduto);
            codigo = itemView.findViewById(R.id.idCodigo);
        }
    }
}
