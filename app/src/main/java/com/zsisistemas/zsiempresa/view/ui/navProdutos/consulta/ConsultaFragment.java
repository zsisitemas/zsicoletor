package com.zsisistemas.zsiempresa.view.ui.navProdutos.consulta;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import static com.zsisistemas.zsiempresa.controller.Util.closeKeyboard;

public class ConsultaFragment extends Fragment {

    private TextView textoIdColeta, textoCodigoProduto, textoDescricaoProduto, textoPrecoVenda, textoQuantidadeEstoque, textoUnidade, textoQuantidadeInventario, textoPreco;
    private TextInputLayout editarCampoLocaliza, editarCampoEstoque, editarCampoPreco;
    private Button botaoEscanner, botaoLocalizar, botaoCancelar, botaoAjustar, botaoEtiqueta;
    private ProgressDialog progressNoDialog;
    private ProgressDialog progressDialog;
    private ProgressDialog dialogConsulta;
    private RadioGroup radioColetar;
    private RadioGroup radioGroup;
    private RadioButton radioAdicionar, radioSubtrair, radioAjustar;
    private RadioButton radioEstoque, radioInvi;
    private int tipoAjuste = 1; // 1 - estoque, 2 - inventario
    private int tipoQuery = 1; // 1 - adicionar (+=), 2 - subtrair (-=), 3 - ajustar (=)
    private ProdutoModel produtoModel;
    private int conn_preferencias = Integer.parseInt(SingletonConfiguracoes.getConfig().getConnection());
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_consulta, container, false);

        textoIdColeta = view.findViewById(R.id.id_coleta);
        textoCodigoProduto = view.findViewById(R.id.text_codigo_produto);
        textoDescricaoProduto = view.findViewById(R.id.text_descricao_produto);
        textoUnidade = view.findViewById(R.id.text_unidade);
        textoQuantidadeInventario = view.findViewById(R.id.text_estoque_inventario);
        textoPrecoVenda = view.findViewById(R.id.text_preco_venda);
        textoQuantidadeEstoque = view.findViewById(R.id.text_estoque_produto);
        textoPreco = view.findViewById(R.id.textView22);
        editarCampoLocaliza = view.findViewById(R.id.edit_campo_localiza);
        editarCampoEstoque = view.findViewById(R.id.edit_campo_estoque);
        editarCampoPreco = view.findViewById(R.id.edit_campo_preco);
        botaoLocalizar = view.findViewById(R.id.button_localizar);
        botaoEscanner = view.findViewById(R.id.button_scann);
        botaoEscanner.setOnClickListener(mOnclickListener);
        botaoCancelar = view.findViewById(R.id.button_cancelar);
        botaoAjustar = view.findViewById(R.id.button_ajustar);
        botaoEtiqueta = view.findViewById(R.id.id_botao_imprimir);
        radioColetar = view.findViewById(R.id.radio_coletar);
        radioEstoque = view.findViewById(R.id.estoque);
        radioInvi = view.findViewById(R.id.inventario);
        radioGroup = view.findViewById(R.id.radio_tipo_coleta);
        radioAdicionar = view.findViewById(R.id.adicionar);
        radioSubtrair = view.findViewById(R.id.subtrair);
        radioAjustar = view.findViewById(R.id.ajustar);
        produtoModel = new ProdutoModel();

        if(conn_preferencias == 3){
            radioEstoque.setVisibility(View.GONE);
            tipoAjuste = 2;
        }

        if(tipoAjuste == 1){
            radioEstoque.setChecked(true);
        }
        if(tipoAjuste == 2){
            radioInvi.setChecked(true);
        }

        if(tipoQuery == 1){
            radioAdicionar.setChecked(true);
        }
        if(tipoQuery == 2){
            radioSubtrair.setChecked(true);
        }
        if(tipoQuery == 3){
            radioAjustar.setChecked(true);
        }

        SharedPreferences preferencesPerfilFun = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if(preferencesPerfilFun.contains("perfilFun")){
            String perfilFun = preferencesPerfilFun.getString("perfilFun", "perfilFun não definido");
            SingletonCliente.getCliente().setPerfil(perfilFun);

            if (perfilFun.equalsIgnoreCase("admin") || perfilFun.equalsIgnoreCase("dev")) {
                editarCampoPreco.setVisibility(view.VISIBLE);
                textoPreco.setVisibility(view.VISIBLE);
            }
        } else {
            editarCampoPreco.setVisibility(view.VISIBLE);
            textoPreco.setVisibility(view.VISIBLE);
        }

        botaoLocalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(view, getActivity());

                String codigo = editarCampoLocaliza.getEditText().getText().toString().trim();

                if(!codigo.isEmpty()){
                    progressNoDialog = new ProgressDialog(getActivity());
                    progressNoDialog.show();
                    progressNoDialog.setCancelable(false);
                    progressNoDialog.setContentView(R.layout.progress_no_dialog);
                    progressNoDialog.getWindow().setBackgroundDrawableResource(
                            android.R.color.transparent
                    );

                    new Thread() {
                        @Override
                        public void run() {

                            exibirProduto();
                            progressNoDialog.dismiss();

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {

                                    if(SingletonProduto.getProduto().getDescricao_produto() != null){

                                        setarTextos();

                                    }else{
                                        Toast.makeText(getActivity(),"Produto Não encontrado!",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    }.start();
                } else{
                    Toast.makeText(getActivity(),"Informe o código do produto!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        botaoCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limparTela();
                closeKeyboard(view, getActivity());
            }
        });

        radioColetar.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId){
                    case R.id.estoque:
                        tipoAjuste = 1;
                        break;
                    case R.id.inventario:
                        tipoAjuste = 2;
                        break;
                }
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId){
                    case R.id.adicionar:
                        tipoQuery = 1;
                        break;
                    case R.id.subtrair:
                        tipoQuery = 2;
                        break;
                    case R.id.ajustar:
                        tipoQuery = 3;
                        break;
                }
            }
        });

        botaoEtiqueta.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {

                closeKeyboard(view, getActivity());

                if(!textoDescricaoProduto.getText().toString().trim().isEmpty()){

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Imprimir");
                    builder.setMessage("Tem certeza que deseja coletar a etiqueta para impressão?");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.show();
                            progressDialog.setCancelable(false);
                            progressDialog.setContentView(R.layout.progress_dialog);
                            progressDialog.getWindow().setBackgroundDrawableResource(
                                    android.R.color.transparent
                            );

                            new Thread() {
                                @Override
                                public void run() {
                                    salvarColetaDeEtiqueta();
                                    progressDialog.dismiss();
                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            Toast.makeText(getActivity(),"Etiqueta coletada para impressão!",Toast.LENGTH_SHORT).show();
                                            limparTela();
                                        }
                                    });
                                }
                            }.start();
                        }
                    })
                    .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .show();
                } else {
                    Toast.makeText(getActivity(),"Primeiro localize o produto!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        botaoAjustar.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                closeKeyboard(view, getActivity());

                String estoque = editarCampoEstoque.getEditText().getText().toString().trim();
                String preco = editarCampoPreco.getEditText().getText().toString().trim();

                // Pelo menos um dos campos devem ser preechido antes de confirmar o envio para o banco
                // isso garante que salve apenas os campos preenchidos.
                if(!textoDescricaoProduto.getText().toString().trim().isEmpty()){
                    if(!estoque.isEmpty() || !preco.isEmpty()){

                        dialogConsulta = new ProgressDialog(getActivity());
                        dialogConsulta.show();
                        dialogConsulta.setContentView(R.layout.dialog_consulta);
                        dialogConsulta.getWindow().setBackgroundDrawableResource(
                                android.R.color.transparent
                        );

                        TextView textoAjustar = dialogConsulta.findViewById(R.id.text_ajustar); // texto Estoque

                        TextView textoEstoqueAtual = dialogConsulta.findViewById(R.id.textView); // texto Estoque atual
                        TextView valorEstoqueAtual = dialogConsulta.findViewById(R.id.text_estoque_atual); // valor do Estoque atual
                        TextView valorEstoqueFinal = dialogConsulta.findViewById(R.id.text_estoque_final); // valor do Estoque finalTextView textoAjustar = dialogConsulta.findViewById(R.id.text_ajustar); // texto Estoque

                        TextView valorPrecoAtual = dialogConsulta.findViewById(R.id.text_preco_atual); // valor do Preço atual
                        TextView valorPrecoFinal = dialogConsulta.findViewById(R.id.text_preco_final); // valor do Preço final

                        Button botaoConfirmarAjustes = dialogConsulta.findViewById(R.id.button_confirmar_ajustes);

                        double estoqueAtual = Double.parseDouble(textoQuantidadeEstoque.getText().toString().trim());
                        double precoAtual = Double.parseDouble(textoPrecoVenda.getText().toString().trim());

                        if(!estoque.isEmpty()){

                            double campoEstoque = Double.parseDouble(editarCampoEstoque.getEditText().getText().toString().trim());

                            if(tipoAjuste == 2){
                                textoAjustar.setText("Inventário");
                                textoEstoqueAtual.setText("Inventário atual");
                                estoqueAtual = Double.parseDouble(textoQuantidadeInventario.getText().toString().trim());
                            }

                            if(tipoQuery == 1){ // somar
                                valorEstoqueAtual.setText("+" + estoqueAtual);

                                valorEstoqueFinal.setText(Util.formataDouble(estoqueAtual + campoEstoque));
                            }

                            if(tipoQuery == 2){ // subtrair
                                valorEstoqueAtual.setText("-" + estoqueAtual);

                                double resultadoEstoque = estoqueAtual - campoEstoque;

                                if(resultadoEstoque < 0){
                                    resultadoEstoque = 0;
                                }

                                valorEstoqueFinal.setText(Util.formataDouble(resultadoEstoque));
                            }

                            if(tipoQuery == 3){ // ajustar
                                valorEstoqueAtual.setText("=" + estoqueAtual);

                                valorEstoqueFinal.setText(Util.formataDouble(campoEstoque));
                            }
                        } else {
                            /*Toast.makeText(getActivity(),"Estoque vazio!",Toast.LENGTH_SHORT).show();*/

                            if(tipoAjuste == 2){
                                textoAjustar.setText("Inventário");
                                textoEstoqueAtual.setText("Inventário atual");
                                estoqueAtual = Double.parseDouble(textoQuantidadeInventario.getText().toString().trim());
                            }

                            valorEstoqueAtual.setText("" + estoqueAtual);

                            valorEstoqueFinal.setText("" + estoqueAtual);
                        }

                        if (!preco.isEmpty()) {

                            double campoPreco = Double.parseDouble(editarCampoPreco.getEditText().getText().toString().trim());

                            valorPrecoAtual.setText("=" + precoAtual);

                            valorPrecoFinal.setText(Util.formataDouble(campoPreco));
                        } else {
                            /*Toast.makeText(getActivity(),"Preço vazio!",Toast.LENGTH_SHORT).show();*/

                            precoAtual = Double.parseDouble(textoPrecoVenda.getText().toString().trim());
                            valorPrecoAtual.setText("" + precoAtual);

                            valorPrecoFinal.setText("" + precoAtual);
                        }

                        botaoConfirmarAjustes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                dialogConsulta.dismiss();

                                double qtdInvi = Double.parseDouble(valorEstoqueFinal.getText().toString().trim());
                                double qtdEsto = Double.parseDouble(valorEstoqueFinal.getText().toString().trim());
                                double qtdPre = Double.parseDouble(valorPrecoFinal.getText().toString().trim());

                                progressDialog = new ProgressDialog(getActivity());
                                progressDialog.show();
                                progressDialog.setCancelable(false);
                                progressDialog.setContentView(R.layout.progress_dialog);
                                progressDialog.getWindow().setBackgroundDrawableResource(
                                        android.R.color.transparent
                                );

                                new Thread() {
                                    @Override
                                    public void run() {

                                        atualizarQtds(qtdInvi, qtdEsto, qtdPre);
                                        progressDialog.dismiss();

                                        getActivity().runOnUiThread(new Runnable() {
                                            public void run() {

                                                if(!estoque.isEmpty()) {
                                                    if(tipoAjuste == 2){
                                                        Toast.makeText(getActivity(),"Inventário ajustado com Sucesso!",Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(getActivity(),"Estoque ajustado com Sucesso!",Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                if(!preco.isEmpty()) {
                                                    Toast.makeText(getActivity(),"Preço ajustado com Sucesso!",Toast.LENGTH_SHORT).show();
                                                }

                                                limparTela();
                                            }
                                        });
                                    }
                                }.start();
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(),"Campos vazios serão ignorados! \n" +
                                "Para continuar preencha os campos que deseja ajustar!",Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(),"Primeiro localize o produto!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);

        if(result != null){

            if(result.getContents() != null){
                editarCampoLocaliza.getEditText().setText(result.getContents());
                exibirProduto();

                if(SingletonProduto.getProduto().getDescricao_produto() != null){
                    setarTextos();
                }else{
                    Toast.makeText(getActivity(),"Produto Não encontrado!",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private View.OnClickListener mOnclickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button_scann:
                    IntentIntegrator.forSupportFragment(ConsultaFragment.this).initiateScan();
                    break;
            }
        }
    };

    public void exibirProduto(){
        String codigo = editarCampoLocaliza.getEditText().getText().toString().trim();

        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.localizaProdutoPorCodigo(getActivity(),codigo);

    }

    @SuppressLint("SetTextI18n")
    public void setarTextos(){

        textoIdColeta.setText(" " + SingletonProduto.getProduto().getId_Produtos());
        textoCodigoProduto.setText(" " + SingletonProduto.getProduto().getCodigo_produto());
        textoDescricaoProduto.setText(" " + SingletonProduto.getProduto().getDescricao_produto());
        textoUnidade.setText(" " + SingletonProduto.getProduto().getUnidade());
        textoQuantidadeInventario.setText(" " + SingletonProduto.getProduto().getQtd_inventario());
        textoQuantidadeEstoque.setText(" " + SingletonProduto.getProduto().getEstoque());
        textoPrecoVenda.setText(" " + SingletonProduto.getProduto().getPreco_venda());
        editarCampoLocaliza.getEditText().setText("");

    }

    public void limparTela(){

        textoIdColeta.setText("");
        textoCodigoProduto.setText("");
        textoDescricaoProduto.setText("");
        textoUnidade.setText("");
        textoQuantidadeInventario.setText("");
        textoQuantidadeEstoque.setText("");
        textoPrecoVenda.setText("");
        editarCampoLocaliza.getEditText().setText("");
        editarCampoEstoque.getEditText().setText("");
        editarCampoPreco.getEditText().setText("");
        radioAdicionar.setChecked(true);
        tipoQuery = 1;
        tipoAjuste = 1;
        radioEstoque.setChecked(true);

        if(conn_preferencias == 3){
            radioEstoque.setVisibility(View.GONE);
            tipoAjuste = 2;
            radioInvi.setChecked(true);
        }
    }

    public void atualizarQtds(double qtdInvi, double qtdEsto, double qtdPre){
        Controller_Conexao controller_conexao = new Controller_Conexao();

        String estoque = editarCampoEstoque.getEditText().getText().toString().trim();
        String preco = editarCampoPreco.getEditText().getText().toString().trim();

        String id = textoIdColeta.getText().toString().trim();
        produtoModel.setId_Produtos(Integer.parseInt(id));

        /*if(!preco.isEmpty()) {*/
            produtoModel.setPreco_venda(qtdPre);
        /*}*/

        if(tipoAjuste == 2){
            // INVENTARIO
            produtoModel.setQtd_inventario(qtdInvi);
            controller_conexao.atualizaQtdInventario(getActivity(), produtoModel, "qtd_inventario");
        } else {
            // ESTOQUE
            produtoModel.setEstoque(qtdEsto);
            // Apontando pro atualizar inventario pois conexao 3 nao precisa atualizar estoque
            // e demais conexoes controlam se e inventario ou estoque na propria DAO.
            controller_conexao.atualizaQtdInventario(getActivity(), produtoModel, "estoque");
        }

        SingletonProduto.getProduto().setListaProdutos(null);
        controller_conexao.listarProdutos(getActivity());

    }

    public void salvarColetaDeEtiqueta(){
        Controller_Conexao controller_conexao = new Controller_Conexao();
        String id = textoIdColeta.getText().toString().trim();
        produtoModel.setId_Produtos(Integer.parseInt(id));
            controller_conexao.inserirEtiquetaProduto(getActivity(), produtoModel);


        SingletonProduto.getProduto().setListaProdutos(null);
        controller_conexao.listarProdutos(getActivity());

    }
}