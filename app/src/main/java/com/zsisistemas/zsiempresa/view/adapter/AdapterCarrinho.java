package com.zsisistemas.zsiempresa.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ProdutoModel;

import java.util.List;

public class AdapterCarrinho extends RecyclerView.Adapter<AdapterCarrinho.MyViewHolder> {

    // Lista com produtos do carrinho
    private List<ProdutoModel> listaCarrinho;

    public AdapterCarrinho(List<ProdutoModel> lista) {
        this.listaCarrinho = lista;
    }

    @NonNull
    @Override
    public AdapterCarrinho.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mainCardViewLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cartao_recycler_carrinho, parent, false);
        // Define o layout do item
        return new AdapterCarrinho.MyViewHolder(mainCardViewLista);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCarrinho.MyViewHolder holder, int position) {
        // Seta os dados do produto para exibição
        ProdutoModel produto = listaCarrinho.get( position );

        holder.descricao_produto.setText( produto.getDescricao_produto());
        // holder.quantidade_estoque.setText("");
        holder.preco_venda.setText( String.valueOf(produto.getPreco_venda()));

        holder.preco_total.setText("Total R$: " + Util.formataDouble(produto.getPreco_total()));
        holder.id.setText(String.valueOf(produto.getId_Produtos()));

        holder.quantidade.setText(String.valueOf(produto.getQuantidadeProdutos()));

    }

    @Override
    public int getItemCount() {
        // Define a quantidade de itens na RecyclerView
        // Retornar tamanho da lista se não for nula
        return listaCarrinho != null ? listaCarrinho.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        // Criar objetos para exibir os itens mantendo a visualização
        // Cada item é um objeto da MyViewHolder

        TextView descricao_produto;
        TextView preco_venda;
        TextView preco_total;
        TextView id;
        TextView codigo;
        ImageButton botaoMais;
        ImageButton botaoAddQuantidade;
        EditText quantidade;
        ImageButton botaoRmvQuantidade;

        public MyViewHolder(View itemView){
            super(itemView);

            descricao_produto = itemView.findViewById(R.id.text_descricao);
          //  quantidade_estoque = itemView.findViewById(R.id.id_obs);
            preco_venda = itemView.findViewById(R.id.text_preco);
            preco_total = itemView.findViewById(R.id.text_preco_total);
            botaoMais = itemView.findViewById(R.id.imageButton_more);
            botaoAddQuantidade = itemView.findViewById(R.id.imageButton_add);
            quantidade = itemView.findViewById(R.id.edit_quantidade);
            botaoRmvQuantidade = itemView.findViewById(R.id.imageButton_rmv);

            id = itemView.findViewById(R.id.idProduto2);
            //codigo = itemView.findViewById(R.id.id_total);

//            botaoAddQuantidade.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    String qtdString = quantidade.getText().toString().trim();
//                    int qtd = 1;
//
//                    if(qtdString.isEmpty() || qtd == 0){
//                        qtd = 1;
//                        quantidade.setText(String.valueOf(qtd));
//                    }else {
//                        quantidade.setText(qtdString);
//                        qtd = Integer.parseInt(qtdString);
//                    }
//
//                    qtd++;
//                    quantidade.setText(String.valueOf(qtd));
////                quantidade.clearFocus();
//                }
//            });
//
//            botaoRmvQuantidade.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    quantidade.setText(quantidade.getText().toString());
//                    qtd = Integer.parseInt(quantidade.getText().toString());
////                quantidade.clearFocus();
//
//                    if(qtd > 0){
//                        if(qtd == 1){
//
//                        }else {
//                            qtd--;
//                            quantidade.setText(String.valueOf(qtd));
//                        }
//                    }
//                }
//            });

        }
    }
}
