package com.zsisistemas.zsiempresa.view.ui.carrinho;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.Cupom_itemModel;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom_item;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;
import com.zsisistemas.zsiempresa.view.activities.MainActivity;
import com.zsisistemas.zsiempresa.view.activities.RecyclerItemClickListener;
import com.zsisistemas.zsiempresa.view.adapter.AdapterCarrinho;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.zsisistemas.zsiempresa.controller.Util.closeKeyboard;
import static com.zsisistemas.zsiempresa.controller.Util.gerarNumeroPedido;
import static com.zsisistemas.zsiempresa.controller.Util.limparBanco;
import static com.zsisistemas.zsiempresa.controller.Util.validarConfigs;

public class ProdutosCarrinhoFragment extends Fragment {

    private RecyclerView recyclerView;
    public static List<ProdutoModel> listaCarrinho = new ArrayList<>();
    private double total = 0;
    private double subTotal = 0;
    private TextView textSubTotal, textVoid;
    private TextInputLayout editObservacoes;
    private Button botaoFazerPedido, botaoLimparCarrinho, botaoCancelar;
    private Controller_Conexao controller_conexao;
    public static AdapterCarrinho adapterCarrinho;
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";
    private ProgressDialog progressDialog;
    private CarrinhoFragment carrinhoFragment;
    private boolean pedidoChecked;
    private int numeroMesa = 0;
    private ProdutosPedidosFragment produtosPedidosFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_produtos_carrinho, container, false);

        recyclerView = view.findViewById(R.id.recycler_view_carrinho);
        textSubTotal = view.findViewById(R.id.id_subTotal);
        textVoid = view.findViewById(R.id.text_void);
        editObservacoes = view.findViewById(R.id.id_edit_obs);
        botaoFazerPedido = view.findViewById(R.id.id_botao_FazerPedido);
        botaoLimparCarrinho = view.findViewById(R.id.id_bt_limparCar);
        botaoCancelar = view.findViewById(R.id.id_bt_can_ped);
        controller_conexao = new Controller_Conexao();
        carrinhoFragment = new CarrinhoFragment();
        produtosPedidosFragment = new ProdutosPedidosFragment();

        SharedPreferences preferencesIdCliente = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if(preferencesIdCliente.contains("numeroMesa")){
            int numeroMesa = preferencesIdCliente.getInt("numeroMesa", 0);

            SingletonCupom_item.getCupom_Item().setNumeroMesa(numeroMesa);

            this.numeroMesa = SingletonCupom_item.getCupom_Item().getNumeroMesa();
        }

        // Verificar se usuário está logado ou não.
        SharedPreferences preferencesLoginFuncionario = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if (preferencesLoginFuncionario.contains("loginFun")){
            String loginFun = preferencesLoginFuncionario.getString("loginFun", null);

            SingletonCliente.getCliente().setLoginFuncionario(loginFun);
        } else {
            SingletonCliente.getCliente().setLoginFuncionario(null);
        }

        // Verificar se o carrinho é para inserção(false) ou atualização(true).
        pedidoChecked = SingletonProduto.getProduto().isCheckPedido();

        SharedPreferences preferencesPedido = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        SharedPreferences.Editor editor = preferencesPedido.edit();

        if (preferencesPedido.contains("pedido")){
            String pedido = preferencesPedido.getString("pedido", "pedido não definido");
            String obs = preferencesPedido.getString("obs", null);

            SingletonProduto.getProduto().setCheckPedido(Boolean.parseBoolean(pedido));
            pedidoChecked = Boolean.parseBoolean(pedido);

            SingletonProduto.getProduto().setObs(obs);

        } else {
            String pedido = "false";

            editor.putString("pedido", pedido);
            editor.apply();

            SingletonProduto.getProduto().setCheckPedido(Boolean.parseBoolean(pedido));
            pedidoChecked = Boolean.parseBoolean(pedido);
        }

        switchBotao();

        // Atualizar informações do sharedPreferences.
        Util.recuperaDadosCadastrados(getActivity(), ARQUIVO_PREFERENCIA);

        this.listarCarrinho();

        // Configurar Adapter
        listaCarrinho = SingletonProduto.getProduto().getListaCarrinho();

        // Cálculo do total do item e subTotal do pedido.
        this.total = 0;
        this.subTotal = 0;
        for (ProdutoModel p : listaCarrinho) {
            this.total = p.getPreco_venda() * p.getQuantidadeProdutos();
            p.setPreco_total(total);
            this.subTotal += total;
        }
        textSubTotal.setText("R$: " + Util.formataDouble(subTotal));

        // Setar lista de produtos no adapter carrinho.
        adapterCarrinho = new AdapterCarrinho( listaCarrinho );
        adapterCarrinho.notifyDataSetChanged();

        // Verificar se carrinho está vazio e dar visibilidade ou não aos itens da tela.
        if (listaCarrinho.size() == 0){
            textVoid.setVisibility(View.VISIBLE);
            editObservacoes.setVisibility(View.INVISIBLE);
        } else {
            textVoid.setVisibility(View.INVISIBLE);
            editObservacoes.setVisibility(View.VISIBLE);
        }

        // Configurar RecyclerView
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter( adapterCarrinho );
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

//         Evento de clique no item do recycler
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getActivity().getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                closeKeyboard(view, getActivity());

//                                Recupera item da posição clicada na recycler view.
                                ProdutoModel produtoModel = listaCarrinho.get(position);

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Produto: " + produtoModel.getDescricao_produto())
                                        .setMessage("Tem certeza que deseja remover este item do carrinho?")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // Salva conexão atual para recuperar no final do método.
                                                String con = SingletonConfiguracoes.getConfig().getConnection();

//                                Trocar conexão temporariamente para sqlite para remover o item da lista.
                                                SingletonConfiguracoes.getConfig().setConnection("3");
                                                controller_conexao.limparPorId(getActivity(), produtoModel);

//                                Recalcular total e subTotal após remover o item da lista.
                                                total = produtoModel.getPreco_venda() * produtoModel.getQuantidadeProdutos();
                                                subTotal -= total;
                                                textSubTotal.setText("R$: " + Util.formataDouble(subTotal));

//                                Evitar resultados negativos.
                                                if (subTotal < 0){
                                                    textSubTotal.setText("R$: " + Util.formataDouble(subTotal * -1));
                                                }

//                                Notificar recycler sobre posição removida da lista.
                                                listaCarrinho.remove(produtoModel);
                                                adapterCarrinho.notifyDataSetChanged();

//                                Se remover o último item da lista atualizar visualização de itens na tela.
                                                if (listaCarrinho.size() == 0){
                                                    textVoid.setVisibility(View.VISIBLE);
                                                    editObservacoes.setVisibility(View.INVISIBLE);
                                                }

//                                Recupera conexão correta.
                                                SingletonConfiguracoes.getConfig().setConnection(con);
                                            }
                                        })
                                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        })
                                        .show();
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );

        botaoLimparCarrinho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(v, getActivity());

                String nome = carrinhoFragment.nomeCliente.getText().toString();

                if (listaCarrinho.size() != 0 || !nome.equals("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Limpar carrinho")
                            .setMessage("Tem certeza que deseja limpar o carrinho?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    limparBanco(getActivity());
                                    listaCarrinho.clear();
                                    adapterCarrinho.notifyDataSetChanged();
                                    textSubTotal.setText("R$: " + Util.formataDouble(0.0));
                                    total = 0;
                                    subTotal = 0;

                                    String idCli = null;
                                    String nomeCli = null;
                                    String telefoneCli = null;
                                    int numeroMesa = 0;
                                    String obs = null;

                                    editor.putString("idCli", idCli);
                                    editor.putString("nomeCli", nomeCli);
                                    editor.putString("telefoneCli", telefoneCli);
                                    editor.putInt("numeroMesa", numeroMesa);
                                    editor.putString("obs", obs);
                                    editor.apply();

                                    SingletonCliente.getCliente().setNomeClienteSelecionado(nomeCli);

                                    carrinhoFragment.idCliente.setText(null);
                                    carrinhoFragment.nomeCliente.setText(null);
                                    carrinhoFragment.textoDefault.setText("não selecionado!");
                                    editObservacoes.getEditText().setText(null);
                                    textVoid.setVisibility(View.VISIBLE);
                                    editObservacoes.setVisibility(View.INVISIBLE);

                                    String pedido = "false";
                                    editor.putString("pedido", pedido);
                                    editor.apply();
                                    pedidoChecked = Boolean.parseBoolean(pedido);
                                    SingletonProduto.getProduto().setCheckPedido(pedidoChecked);
                                    switchBotao();
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }
            }
        });

        botaoCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(v, getActivity());

                if (botaoFazerPedido.getText().equals("ATUALIZAR PEDIDO")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Cancelar pedido")
                            .setMessage("Tem certeza que deseja cancelar este pedido?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    progressDialog = new ProgressDialog(getActivity());
                                    progressDialog.show();
                                    progressDialog.setCancelable(false);
                                    progressDialog.setContentView(R.layout.progress_dialog);
                                    progressDialog.getWindow().setBackgroundDrawableResource(
                                            android.R.color.transparent
                                    );

                                    new Thread() {
                                        @Override
                                        public void run() {

                                            if (botaoFazerPedido.getText().equals("ATUALIZAR PEDIDO")) {
                                                metodosStatus();
                                            } else {
                                                SingletonCupom_item.getCupom_Item().setStatusPedido(false);
                                            }

                                            if(SingletonCupom_item.getCupom_Item().isStatusPedido()){
                                                progressDialog.dismiss();

                                                getActivity().runOnUiThread(new Runnable() {
                                                    public void run() {

                                                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                                        builder.setTitle("Oops! Pedido já cancelado")
                                                                .setMessage("Impossível continuar! Este pedido será descartado!")
                                                                .setCancelable(false)
                                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        botaoFazerPedido.setText("CONFIRMAR PEDIDO");
                                                                        limparTela();
                                                                    }
                                                                })
                                                                .show();
                                                    }
                                                });
                                            } else {
                                                metodosCancela();
                                                progressDialog.dismiss();

                                                getActivity().runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        limparTela();
                                                        switchBotao();

                                                        Toast.makeText(getActivity(),"Pedido cancelado!",Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            }
                                        }
                                    }.start();
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }else {
                    Toast.makeText(getActivity(),"Para cancelar um pedido primeiro selecione-o na aba PEDIDOS!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        botaoFazerPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(v, getActivity());

                String nomeCliente = SingletonCliente.getCliente().getNomeClienteSelecionado();
                String loginFuncionario = SingletonCliente.getCliente().getLoginFuncionario();

//                Só atualiza ou insere se usuário estiver logado, cliente estiva=er selecionado e hover produtos no carrinho.
                if(loginFuncionario != null){
                    if(nomeCliente != null){
                        if(listaCarrinho.size() != 0){

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            if (botaoFazerPedido.getText().equals("ATUALIZAR PEDIDO")) {
                                builder.setTitle("Atualizar pedido")
                                        .setMessage("Tem certeza que deseja atualizar este pedido?");
                            } else {
                                builder.setTitle("Confirmar pedido")
                                        .setMessage("Tem certeza que deseja confirmar este pedido?");
                            }
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
//                                            Abrir dialogo até finalizar ou atualizar o pedido.
                                            progressDialog = new ProgressDialog(getActivity());
                                            progressDialog.show();
                                            progressDialog.setCancelable(false);
                                            progressDialog.setContentView(R.layout.progress_dialog);
                                            progressDialog.getWindow().setBackgroundDrawableResource(
                                                    android.R.color.transparent
                                            );

                                            new Thread() {
                                                @Override
                                                public void run() {

                                                    if (botaoFazerPedido.getText().equals("ATUALIZAR PEDIDO")) {
                                                        metodosStatus();
                                                    } else {
                                                        SingletonCupom_item.getCupom_Item().setStatusPedido(false);
                                                    }

                                                    if(SingletonCupom_item.getCupom_Item().isStatusPedido()){
                                                        progressDialog.dismiss();

                                                        getActivity().runOnUiThread(new Runnable() {
                                                            public void run() {

                                                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                                                builder.setTitle("Oops! Pedido já cancelado")
                                                                        .setMessage("Impossível continuar! Este pedido será descartado!")
                                                                        .setCancelable(false)
                                                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                            @Override
                                                                            public void onClick(DialogInterface dialog, int which) {
                                                                                botaoFazerPedido.setText("CONFIRMAR PEDIDO");
                                                                                limparTela();
                                                                            }
                                                                        })
                                                                        .show();
                                                            }
                                                        });
                                                    } else {
                                                        metodos();

                                                        progressDialog.dismiss();

                                                        getActivity().runOnUiThread(new Runnable() {
                                                            public void run() {
                                                                limparTela();

                                                                if(botaoFazerPedido.getText().equals("ATUALIZAR PEDIDO")){
                                                                    Toast.makeText(getActivity(),"Pedido Atualizado!",Toast.LENGTH_SHORT).show();
                                                                    botaoFazerPedido.setText("CONFIRMAR PEDIDO");
                                                                }else {
                                                                    Toast.makeText(getActivity(),"Pedido Confirmado!",Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            }.start();
                                        }
                                    })
                                    .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .show();
                        } else{
                            Toast.makeText(getActivity(),"Oops! Carrinho vazio!",Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getActivity(),"Selecione um cliente!",Toast.LENGTH_SHORT).show();
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Faça login primeiro")
                            .setMessage("Deseja reiniciar o app e fazer login agora?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if(!validarConfigs(getActivity(), ARQUIVO_PREFERENCIA)) {
                                        Toast.makeText(getActivity(), "Ajuste as configurações do app!!!", Toast.LENGTH_LONG).show();
                                    } else {
                                        getActivity().finish();
                                        Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                    }
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }
            }
        });

        return view;
    }

    // Obter produtos e salvar na lista carrinho.
    public void listarCarrinho(){
        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.listaProdutosTemporarioCarrinho(getActivity());
    }

    public void metodos(){
        Controller_Conexao controller = new Controller_Conexao();

        ArrayList<Cupom_itemModel> lista = new ArrayList<>();

        boolean alterar = false;
        int comanda = 0;

        lista = montarPedido();

        for (Cupom_itemModel c : lista) {
            if(c.getAlterado() == 2){
                // Alterar pedido
                alterar = true;
                comanda = c.getComanda();
            }
        }

        if(alterar){  // Cancela a comanda
            controller.cancelaPedido(getActivity(),comanda);
        }

        for (Cupom_itemModel cupomItem : lista) {
            controller.salvarPedidoCupom_item(getActivity(), cupomItem);
        }

        String conexao = SingletonConfiguracoes.getConfig().getConnection();

        if (conexao.equals("2")) {
//          Recarrega a lista pedidos pra atualizar a disponibilidade de clientes MESA na recycler listaClientes.
            produtosPedidosFragment.listarPedido(getActivity());
            Log.d("listaMesa", "conexao: " + conexao);
        }
    }

    public void metodosCancela(){
        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.cancelaPedido(getActivity(), SingletonCliente.getCliente().getIdClienteSelecionado());

        String conexao = SingletonConfiguracoes.getConfig().getConnection();

        if (conexao.equals("2")) {
//          Recarrega a lista pedidos pra atualizar a disponibilidade de clientes MESA na recycler listaClientes.
            produtosPedidosFragment.listarPedido(getActivity());
            Log.d("listaMesa", "conexao: " + conexao);
        }
    }

    public void metodosStatus(){
        Controller_Conexao controller = new Controller_Conexao();

        if(botaoFazerPedido.getText().equals("ATUALIZAR PEDIDO")){
            SingletonCupom_item.getCupom_Item().setListaPedido(null);

            if(this.numeroMesa != 0){
                controller.verificarStatusPedido(getActivity(), this.numeroMesa);
            }
        }
    }

//    Agrupar os produtos para formar cupom_item.
    public ArrayList<Cupom_itemModel> montarPedido(){
        int count = 0;

        ArrayList<Cupom_itemModel> listPedido = new ArrayList<>();
        int numeroPedidoGerado = gerarNumeroPedido();

        int comanda = gerarNumeroPedido();

        // Controla se é pra fazer um pedido novo(false) ou atualizar um existente(true).
        if(pedidoChecked){
            // No evento de clique da recycler pedidos:
            // pedidoChecked recebe true e
            // IdClienteSelecionado recebe numero da comanda do pedido clicado.
            comanda = SingletonCliente.getCliente().getIdClienteSelecionado();
       }

//        Salvar data e hora da ação feita no pedido.
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formataData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dataFormatada = formataData.format(calendar.getTime());
        SingletonCupom_item.getCupom_Item().setData(dataFormatada);

        String obs = editObservacoes.getEditText().getText().toString().trim();
        String conexaoLogado = SingletonConfiguracoes.getConfig().getConnection();
        String perfil = SingletonCliente.getCliente().getPerfil();
        String[] clienteMesa = new String[2];

        for (ProdutoModel p:listaCarrinho) {
            Cupom_itemModel cupom_itemModel = new Cupom_itemModel();
            count++;

            if (conexaoLogado.equals("2") && perfil.equalsIgnoreCase("funcionario")) {
                clienteMesa = SingletonCliente.getCliente().getNomeClienteSelecionado().split(" ");
                cupom_itemModel.setNumeroMesa(Integer.parseInt(clienteMesa[1]));
                cupom_itemModel.setTipoMesa("0");
            }else{
                cupom_itemModel.setNumeroMesa(numeroPedidoGerado);
                cupom_itemModel.setTipoMesa("1");
            }

            cupom_itemModel.setNumero_cupom(0);
            cupom_itemModel.setNomeCliete(SingletonCliente.getCliente().getNomeClienteSelecionado());
            cupom_itemModel.setAtendente(SingletonCliente.getCliente().getLoginFuncionario()); // atendente
            cupom_itemModel.setItem(count);  // item
            cupom_itemModel.setCod_produto(p.getCodigo_produto());  // cod_produto
            cupom_itemModel.setProduto_descricao(p.getDescricao_produto()); // produto_descricao
            cupom_itemModel.setUnidade(p.getUnidade()); // unidade
            cupom_itemModel.setQuantidade(p.getQuantidadeProdutos());   // quantidade
            cupom_itemModel.setValor(p.getPreco_venda());   // valor
            cupom_itemModel.setTotal(p.getPreco_total());   // Total do item.
            cupom_itemModel.setSubTotal(subTotal);   // subTotal do pedido.
            cupom_itemModel.setStatus("");  // status
            cupom_itemModel.setStatusComanda("Aberto");
            cupom_itemModel.setId_produto(p.getId_Produtos());  //id_produto
            cupom_itemModel.setData(SingletonCupom_item.getCupom_Item().getData());  // data

            cupom_itemModel.setValorTipo(0);
            cupom_itemModel.setQtd_item(adapterCarrinho.getItemCount());

            cupom_itemModel.setImpresso(p.getImpressao());

            cupom_itemModel.setFone(SingletonCliente.getCliente().getTelefoneClienteSelecionado());

            if(obs.isEmpty()){
                cupom_itemModel.setObs(" "); // Observaçoes
            } else {
                cupom_itemModel.setObs(obs); // Observaçoes
            }

            cupom_itemModel.setCod_pedidoFPTO("Dinhei");
            cupom_itemModel.setComanda(comanda);    // numero_cupom
            cupom_itemModel.setCaixa("01"); // caixa
            cupom_itemModel.setFinalizado(2);

            if(this.pedidoChecked == true){   // Define 2 pra alterar ou 1 pra inserir
                cupom_itemModel.setAlterado(2);
            }else{
                cupom_itemModel.setAlterado(1);
            }

         listPedido.add(cupom_itemModel);
        }

        return listPedido;
    }

    // Trocar texto do botão
    public void switchBotao(){
        if(pedidoChecked){
            botaoFazerPedido.setText("ATUALIZAR PEDIDO");

            editObservacoes.getEditText().setText(SingletonProduto.getProduto().getObs());
        }else{
            botaoFazerPedido.setText("CONFIRMAR PEDIDO");
        }
    }

    public void limparTela(){

        SharedPreferences preferencesPedido = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        SharedPreferences.Editor editor = preferencesPedido.edit();

        limparBanco(getActivity());
        listaCarrinho.clear();
        adapterCarrinho.notifyDataSetChanged();
        textSubTotal.setText("R$: " + Util.formataDouble(0.0));
        total = 0;
        subTotal = 0;

        Util.recuperaDadosCadastrados(getActivity(), ARQUIVO_PREFERENCIA);

        carrinhoFragment.idCliente.setText(null);
        carrinhoFragment.nomeCliente.setText(null);
        carrinhoFragment.textoDefault.setText("não selecionado!");
        textVoid.setVisibility(View.VISIBLE);
        editObservacoes.setVisibility(View.INVISIBLE);

        String idCli = null;
        String nomeCli = null;
        String telefoneCli = null;
        int numeroMesa = 0;
        String obs = null;

        editor.putString("idCli", idCli);
        editor.putString("nomeCli", nomeCli);
        editor.putString("telefoneCli", telefoneCli);
        editor.putInt("numeroMesa", numeroMesa);
        editor.putString("obs", obs);
        editor.apply();

        SingletonCliente.getCliente().setNomeClienteSelecionado(nomeCli);

        String pedido = "false";
        editor.putString("pedido", pedido);
        editor.apply();
        pedidoChecked = Boolean.parseBoolean(pedido);
        SingletonProduto.getProduto().setCheckPedido(pedidoChecked);
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences preferencesPedido = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        if (preferencesPedido.contains("pedido")){
            String pedido = preferencesPedido.getString("pedido", "pedido não definido");
            String obs = preferencesPedido.getString("obs", null);

            SingletonProduto.getProduto().setCheckPedido(Boolean.parseBoolean(pedido));
            pedidoChecked = Boolean.parseBoolean(pedido);

            SingletonProduto.getProduto().setObs(obs);
            editObservacoes.getEditText().setText(SingletonProduto.getProduto().getObs());

        }
    }
}
