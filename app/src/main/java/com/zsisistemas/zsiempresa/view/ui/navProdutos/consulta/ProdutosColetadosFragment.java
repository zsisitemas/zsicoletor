package com.zsisistemas.zsiempresa.view.ui.navProdutos.consulta;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;
import com.zsisistemas.zsiempresa.view.adapter.AdapterColetados;

import java.util.ArrayList;

public class ProdutosColetadosFragment extends Fragment {

    private RecyclerView recyclerView;
    private ArrayList<ProdutoModel> listaColetados = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_produtos_coletados, container, false);

        recyclerView = view.findViewById(R.id.recycler_view_coletados);

        listarColetados();

        // Configurar Adapter
        listaColetados = SingletonProduto.getProduto().getListaColetados();
        AdapterColetados adapterColetados = new AdapterColetados(listaColetados);

        // Configurar RecyclerView
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter( adapterColetados );

        return view;
    }

    public void listarColetados(){
        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.localizaProdutoColetados(getActivity());
    }
}
