package com.zsisistemas.zsiempresa.view.ui.navProdutos.produtos;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.view.ui.navProdutos.cadastro.CadastroFragment;
import com.zsisistemas.zsiempresa.view.ui.navProdutos.consulta.ColetaFragment;
import com.zsisistemas.zsiempresa.view.ui.navProdutos.consulta.ConsultaFragment;
import com.zsisistemas.zsiempresa.view.ui.navProdutos.validade.ValidadeFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class NavProdutosFragment extends Fragment {

    private ProdutosFragment produtosFragment;
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";
    public static FloatingActionButton fab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nav_produtos, container, false);

        setHasOptionsMenu(true);

        // Controla o fragment default do navigation_produtos
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_nav_produtos,
                new ProdutosFragment()).commit();

        produtosFragment = new ProdutosFragment();
        fab = view.findViewById(R.id.recycler_view_layour_fab);

        fab.setVisibility(View.VISIBLE);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.action_nav_produtos_to_nav_carrinho);
            }
        });

        BottomNavigationView bottomNavigationView = view.findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem nav_dashboard_lista = menu.findItem(R.id.fragmentNavLista);
        MenuItem nav_dashboard_consulta = menu.findItem(R.id.fragmentNavConsulta);
        MenuItem nav_dashboard_cadastro = menu.findItem(R.id.fragmentNavCadastro);
        nav_dashboard_cadastro.setVisible(false);

        SharedPreferences preferencesPerfilFun = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        if(preferencesPerfilFun.contains("perfilFun")){
            String perfilFun = preferencesPerfilFun.getString("perfilFun", "perfilFun não definido");

            SingletonCliente.getCliente().setPerfil(perfilFun);

            // Controlar conflito entre listas carrinho e produtos quando ha pessoa logada com banco sqlite
            if (SingletonConfiguracoes.getConfig().getConnection().equals("3")) {
                nav_dashboard_lista.setVisible(false);

                // Controla o fragment default do navigation_produtos
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_nav_produtos,
                        new ColetaFragment()).commit();

                nav_dashboard_consulta.setChecked(true);

                fab.setVisibility(View.INVISIBLE);
            }

            if(perfilFun.equalsIgnoreCase("admin")
                || perfilFun.equalsIgnoreCase("dev")){
                nav_dashboard_cadastro.setVisible(true);
            } else {
                nav_dashboard_cadastro.setVisible(false);
            }
        }

        return view;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch (item.getItemId()) {
                        case R.id.fragmentNavLista:
                            selectedFragment = new ProdutosFragment();
                            setHasOptionsMenu(true);
                            fab.setVisibility(View.VISIBLE);
                            break;
                        case R.id.fragmentNavConsulta:
                            selectedFragment = new ColetaFragment();
                            setHasOptionsMenu(false);
                            fab.setVisibility(View.INVISIBLE);
                            break;
                        case R.id.fragmentNavCadastro:
                            selectedFragment = new CadastroFragment();
                            setHasOptionsMenu(false);
                            fab.setVisibility(View.INVISIBLE);
                            break;
                        case R.id.fragmentNavValidade:
                            selectedFragment = new ValidadeFragment();
                            setHasOptionsMenu(false);
                            fab.setVisibility(View.INVISIBLE);
                            break;
                    }

                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_nav_produtos,
                            selectedFragment).commit();

                    return true;
                }
            };

    // Buscar produto na recycler pela letra digitada no menu search
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_busca, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.app_bar_buscar).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                procuraProduto(newText);
                Util.recuperaDadosCadastrados(getActivity(), ARQUIVO_PREFERENCIA);
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    public void procuraProduto(String descricao_produto){


        if(produtosFragment.listaProdutos != null){

            SingletonConfiguracoes.getConfig().setConnection("3");
            produtosFragment.listaProdutosFiltrados.clear();

            for(ProdutoModel p : produtosFragment.listaProdutos){

                if(p.getDescricao_produto() != null){
                    if (p.getDescricao_produto().toLowerCase().contains(descricao_produto.toLowerCase())){
                        produtosFragment.listaProdutosFiltrados.add(p);
                    }
                }
            }

            produtosFragment.adapterProdutos.notifyDataSetChanged();
        }
    }
}
