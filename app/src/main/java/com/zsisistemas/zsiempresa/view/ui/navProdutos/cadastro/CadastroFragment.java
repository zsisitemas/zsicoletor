package com.zsisistemas.zsiempresa.view.ui.navProdutos.cadastro;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.google.android.material.textfield.TextInputLayout;

import static com.zsisistemas.zsiempresa.controller.Util.closeKeyboard;

public class CadastroFragment extends Fragment {

    private ProdutoModel produto;
    private TextInputLayout editarDescricaoProduto, editarQuantidadeEstoque, editarCodigoProduto, editarUnidade, editarPrecoVenda, editarQuantidadeInventario;
    private Button botaoSalvarProduto;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cadastro, container, false);

        this.produto = new ProdutoModel();
        editarDescricaoProduto = view.findViewById(R.id.edit_descricao_produto);
        editarQuantidadeEstoque = view.findViewById(R.id.edit_quantidade_estoque);
        editarCodigoProduto = view.findViewById(R.id.edit_codigo_produto);
        editarUnidade = view.findViewById(R.id.edit_unidade);
        editarPrecoVenda = view.findViewById(R.id.edit_preco_venda);
        editarQuantidadeInventario = view.findViewById(R.id.edit_quantidade_inventario);
        botaoSalvarProduto = view.findViewById(R.id.button_salvar_produto);

        botaoSalvarProduto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(view, getActivity());

                produto.setDescricao_produto(editarDescricaoProduto.getEditText().getText().toString());

                if(!editarQuantidadeEstoque.getEditText().getText().toString().equals("")){
                    produto.setEstoque(Double.parseDouble(editarQuantidadeEstoque.getEditText().getText().toString()));
                } else {
                    produto.setEstoque(0);
                }

                if(!editarUnidade.getEditText().getText().toString().equals("")){
                    produto.setUnidade(editarUnidade.getEditText().getText().toString());
                } else {
                    produto.setUnidade("UN");
                }

                if(!editarQuantidadeInventario.getEditText().getText().toString().equals("")){
                    produto.setQtd_inventario(Util.converteStringDouble(editarQuantidadeInventario.getEditText().getText().toString()));
                } else {
                    produto.setQtd_inventario(0);
                }

                produto.setStatus(0);

                if(obrigatorios()){

                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.show();
                    progressDialog.setCancelable(false);
                    progressDialog.setContentView(R.layout.progress_dialog);
                    progressDialog.getWindow().setBackgroundDrawableResource(
                            android.R.color.transparent
                    );

                    new Thread() {
                        @Override
                        public void run() {
                            metodos();
                            progressDialog.dismiss();

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getActivity(),"Produto cadastrado!",Toast.LENGTH_SHORT).show();
                                    clear();
                                }
                            });

                        }
                    }.start();
                }
            }
        });

        return  view;
    }

    public boolean obrigatorios(){
        boolean retorno = true;

        if(editarDescricaoProduto.getEditText().getText().toString().trim().isEmpty()){
            editarDescricaoProduto.setError("Descrição não pode ser vazia");
            editarDescricaoProduto.requestFocus();

            retorno = false;
        } else {
            editarDescricaoProduto.setError(null);
            editarDescricaoProduto.setErrorEnabled(false);
        }

        if(editarCodigoProduto.getEditText().getText().toString().trim().isEmpty()){
            editarCodigoProduto.setError("Código não pode ser vazio");
            editarCodigoProduto.requestFocus();

            retorno = false;
        } else {
            String cod = editarCodigoProduto.getEditText().getText().toString();

            String codigoCompleto = Util.completaCodigo(cod);

            int codigoBalaca = Util.codBalanca(codigoCompleto);

            if(codigoBalaca == 2){
                editarCodigoProduto.setError("Código não pode começar com 2");
                editarCodigoProduto.requestFocus();

                if(editarDescricaoProduto.getEditText().getText().toString().trim().isEmpty()){
                    editarDescricaoProduto.requestFocus();
                } else {
                    editarCodigoProduto.requestFocus();
                }

                retorno = false;
            } else {
                produto.setCodigo_produto(codigoCompleto);

                editarCodigoProduto.setError(null);
                editarCodigoProduto.setErrorEnabled(false);
            }
        }

        if(editarPrecoVenda.getEditText().getText().toString().trim().isEmpty()){
            editarPrecoVenda.setError("Preço venda não pode ser vazio");
            editarPrecoVenda.requestFocus();

            if(editarDescricaoProduto.getEditText().getText().toString().trim().isEmpty()){
                editarDescricaoProduto.requestFocus();
            }else {
                if(editarCodigoProduto.getEditText().getText().toString().trim().isEmpty()) {
                    editarCodigoProduto.requestFocus();
                } else {
                    editarPrecoVenda.requestFocus();
                }
            }

            retorno = false;
        } else {
            double preco = Double.parseDouble(editarPrecoVenda.getEditText().getText().toString());

            if(preco == 0){
                editarPrecoVenda.setError("Preço venda não pode ser 0");
                editarPrecoVenda.requestFocus();

                if(editarDescricaoProduto.getEditText().getText().toString().trim().isEmpty()){
                    editarDescricaoProduto.requestFocus();
                }else {
                    if (editarCodigoProduto.getEditText().getText().toString().trim().isEmpty()) {
                        editarCodigoProduto.requestFocus();
                    } else {
                        editarPrecoVenda.requestFocus();
                    }
                }

                retorno = false;
            } else {
                produto.setPreco_venda(preco);

                editarPrecoVenda.setError(null);
                editarPrecoVenda.setErrorEnabled(false);
            }
        }

        if(retorno){
            return true;
        } else {
            return false;
        }

    }

    public void metodos(){
        Controller_Conexao controller_conexao = new Controller_Conexao();
        controller_conexao.salvar(getActivity(),produto);
        controller_conexao.listarProdutos(getActivity());
    }

    public void clear(){
        editarDescricaoProduto.getEditText().setText("");
        editarQuantidadeEstoque.getEditText().setText("");
        editarCodigoProduto.getEditText().setText("");
        editarUnidade.getEditText().setText("");
        editarPrecoVenda.getEditText().setText("");
        editarQuantidadeInventario.getEditText().setText("");

        editarDescricaoProduto.setError(null);
        editarCodigoProduto.setError(null);
        editarPrecoVenda.setError(null);
        editarDescricaoProduto.setErrorEnabled(false);
        editarCodigoProduto.setErrorEnabled(false);
        editarPrecoVenda.setErrorEnabled(false);
    }
}