package com.zsisistemas.zsiempresa.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.model.ProdutoModel;

import java.util.List;

public class AdapterColetar extends RecyclerView.Adapter<AdapterColetar.MyViewHolder> {

    // Lista com produtos coletados
    private List<ProdutoModel> listaColetar;

    public AdapterColetar(List<ProdutoModel> lista) {
        this.listaColetar = lista;
    }

    @NonNull
    @Override
    public AdapterColetar.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mainCardViewLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cartao_recycler_coletar, parent, false);
        // Define o layout do item
        return new AdapterColetar.MyViewHolder(mainCardViewLista);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterColetar.MyViewHolder holder, int position) {
        // Seta os dados do produto para exibição
        ProdutoModel produto = listaColetar.get(position);

        holder.descricao_produto.setText(produto.getDescricao_produto());
        holder.codigo_produto.setText(String.valueOf("Código: " + produto.getCodigo_produto()));
        holder.status.setText("Status: Não Coletado");
    }

    @Override
    public int getItemCount() {
        // Define a quantidade de itens na RecyclerView
        // Retornar tamanho da lista se não for nula
        return listaColetar != null ? listaColetar.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        // Criar objetos para exibir os itens mantendo a visualização
        // Cada item é um objeto da MyViewHolder

        TextView descricao_produto;
        TextView codigo_produto;
        TextView status;

        public MyViewHolder(View itemView){
            super(itemView);

            descricao_produto = itemView.findViewById(R.id.text_descricao);
            codigo_produto = itemView.findViewById(R.id.text_codigo);
            status = itemView.findViewById(R.id.text_status);
        }
    }
}
