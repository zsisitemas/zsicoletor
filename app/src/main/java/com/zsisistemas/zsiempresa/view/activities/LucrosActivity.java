package com.zsisistemas.zsiempresa.view.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.Cupom;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Calendar;

public class LucrosActivity extends AppCompatActivity  implements DatePickerDialog.OnDateSetListener {

    private TextView textoDataI, textoDataF, resetar, aplicar, dataI, dataF;
    private PieChart pieChart;
    private ProgressDialog progressFiltros, progressNoDialog;
    private String dataInicioAux = "";  // Determina se a data inicio mudou ou continua a mesma
    private String dataFimAux = "";   // Determina se a data fim mudou ou continua a mesma
    public static boolean inicioFim;    // Diferenciar se o peridoDialog foi aberto clicando no text dataI(true) ou no dataF(false)
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";
    private Controller_Conexao controller_conexao;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        controller_conexao = new Controller_Conexao();

        progressNoDialog = new ProgressDialog(LucrosActivity.this);
        progressNoDialog.show();
        progressNoDialog.setCancelable(false);
        progressNoDialog.setContentView(R.layout.progress_no_dialog);
        progressNoDialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent
                );

        new Thread() {
            @Override
            public void run() {
                metodosLucros();
                progressNoDialog.dismiss();

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        setContentView(R.layout.activity_lucros);

                        textoDataI = findViewById(R.id.text_data_i);
                        textoDataF = findViewById(R.id.text_data_f);
                        pieChart =  findViewById(R.id.pieChart);

                        String periodoI = "";
                        String periodoF = "";

                        if(SingletonConfiguracoes.getConfig().getDataInicio() != null){
                            periodoI = Util.formatarDataBr(SingletonConfiguracoes.getConfig().getDataInicio());
                        }

                        if(SingletonConfiguracoes.getConfig().getDataFim() != null){
                            periodoF = Util.formatarDataBr(SingletonConfiguracoes.getConfig().getDataFim());
                        }

                        if(!periodoI.equals("")){
                            textoDataI.setText(periodoI);
                        }else {
                            textoDataI.setText(Util.dataAtual());
                        }
                        if(!periodoF.equals("")){
                            textoDataF.setText(periodoF);
                        }else {
                            textoDataF.setText(Util.dataAtual());
                        }

                        ArrayList<PieEntry> lucros = new ArrayList<>();

                        ArrayList<Cupom> array = SingletonCupom.getCupom().getListaCupom();

                        double total = 0;

                        if (array != null) {
                            for (Cupom c : array) {

                                lucros.add(new PieEntry((float) c.getSumSubtotal(), c.getForma_pagamento() +" R$: "+ Util.formataDouble(c.getSumSubtotal())));
                                total += c.getSumSubtotal();
                            }
                        }

                        PieDataSet pieDataSet = new PieDataSet(lucros, "");
                        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
                        pieDataSet.setValueTextColor(Color.BLACK);
                        pieDataSet.setValueTextSize(12f);
                        pieDataSet.setValueFormatter(new PercentFormatter(pieChart));

                        PieData pieData = new PieData(pieDataSet);

                        pieChart.setData(pieData);
                        pieChart.getDescription().setEnabled(false);
                        pieChart.setUsePercentValues(true);

                        if(total == 0){
                            pieChart.setCenterText("Nenhum lucro \nnesse período.");
                        }else{
                            pieChart.setCenterText("Total \nR$: " + Util.formataDouble(total));
                        }

                        pieChart.setCenterTextSize(24);
                        pieChart.setUsePercentValues(true);
                        pieChart.setDrawHoleEnabled(true);

                        Legend legend = pieChart.getLegend();

                        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
                        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                        legend.setOrientation(Legend.LegendOrientation.VERTICAL);
                        legend.setTextSize(14);
                        legend.setDrawInside(false);
                        legend.setEnabled(true);

                        pieChart.animateY(1400, Easing.EaseInOutBack);
                    }
                });
            }
        }.start();

    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_filtros, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.filtros:

                progressFiltros = new ProgressDialog(this);
                progressFiltros.show();
                progressFiltros.setContentView(R.layout.progress_filtros);
                progressFiltros.getWindow().setBackgroundDrawableResource(
                        android.R.color.transparent
                );

                resetar = progressFiltros.findViewById(R.id.text_resetar);
                aplicar = progressFiltros.findViewById(R.id.text_aplicar);
                dataI = progressFiltros.findViewById(R.id.data_inicio);
                dataF = progressFiltros.findViewById(R.id.data_fim);

                if(SingletonConfiguracoes.getConfig().getDataInicio() == null){
                    dataI.setText(Util.dataAtual());
                    dataInicioAux = dataI.getText().toString();
                }else {
                    String recuperaDataI = Util.formatarDataBr(SingletonConfiguracoes.getConfig().getDataInicio());

                    dataI.setText(recuperaDataI);
                    dataInicioAux = recuperaDataI;
                }
                if(SingletonConfiguracoes.getConfig().getDataFim() == null){
                    dataF.setText(Util.dataAtual());
                    dataFimAux = dataF.getText().toString();
                }else {
                    String recuperaDataF = Util.formatarDataBr(SingletonConfiguracoes.getConfig().getDataFim());

                    dataF.setText(recuperaDataF);
                    dataFimAux = recuperaDataF;
                }

                dataI.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        inicioFim = true;
                        DialogFragment periodo = new PeriodoDialog();
                        periodo.show(getSupportFragmentManager(), "date inicio picker");
                    }
                });

                dataF.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        inicioFim = false;
                        DialogFragment periodo = new PeriodoDialog();
                        periodo.show(getSupportFragmentManager(), "date fim picker");
                    }
                });

                resetar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!dataI.getText().toString().equals(Util.dataAtual()) ||
                                !dataF.getText().toString().equals(Util.dataAtual())){
                            dataI.setText(Util.dataAtual());
                            dataF.setText(Util.dataAtual());
                        }
                    }
                });

                aplicar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String revertDataI = Util.formatarDataAm(dataI.getText().toString());
                        String revertDataF = Util.formatarDataAm(dataF.getText().toString());

                        SingletonConfiguracoes.getConfig().setDataInicio(revertDataI);
                        SingletonConfiguracoes.getConfig().setDataFim(revertDataF);

                        if(!dataI.getText().toString().equals(dataInicioAux) ||
                           !dataF.getText().toString().equals(dataFimAux)){    // So recarrega novo periodo se pelo menos uma das datas mudou
                            progressFiltros.dismiss();
                            reload();
                        }
                    }
                });

                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
    
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();

        month++;

        String mes = ""+month;
        String dia = ""+dayOfMonth;

        if(month < 10){
            mes = "0"+month;
        }

        if(dayOfMonth < 10){
            dia = "0"+dayOfMonth;
        }

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, Integer.parseInt(mes));
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dia));

        String dataAtualString = year + "-" + mes + "-" + dia;

        if(inicioFim){
            dataI.setText(Util.formatarDataBr(dataAtualString));
        }else {
            dataF.setText(Util.formatarDataBr(dataAtualString));
        }
    }

    public void metodosLucros(){
        SingletonCupom.getCupom().setListaCupom(null);

        SharedPreferences preferencesLoginFuncionario = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        String funLogado = "";

        if (preferencesLoginFuncionario.contains("loginFun")){
            String loginFun = preferencesLoginFuncionario.getString("loginFun", null);

            SingletonCliente.getCliente().setLoginFuncionario(loginFun);

            funLogado = SingletonCliente.getCliente().getLoginFuncionario();
        } else {
            SingletonCliente.getCliente().setLoginFuncionario(null);
        }

        SharedPreferences preferencesPerfilFun = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        if(preferencesPerfilFun.contains("perfilFun")){
            String perfilFun = preferencesPerfilFun.getString("perfilFun", "perfilFun não definido");

            SingletonCliente.getCliente().setPerfil(perfilFun);

            if(perfilFun.equalsIgnoreCase("admin")
                    || perfilFun.equalsIgnoreCase("dev")){
                controller_conexao.listarFormaPagamento(this);
            } else {
                controller_conexao.listarPagamentoFun(this, funLogado);
            }
        } else {
            controller_conexao.listarPagamentoFun(this, funLogado);
        }
    }

    public void reload() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }
}
