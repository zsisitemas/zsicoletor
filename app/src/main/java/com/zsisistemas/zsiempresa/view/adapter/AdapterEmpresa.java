package com.zsisistemas.zsiempresa.view.adapter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.Empresa;
import com.zsisistemas.zsiempresa.model.ValidadeModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.model.singleton.SingletonEmpresa;
import com.zsisistemas.zsiempresa.view.ui.empresa.EmpresaFragment;

import java.util.Calendar;
import java.util.List;

import static com.zsisistemas.zsiempresa.controller.Util.MD5_Hash;

public class AdapterEmpresa extends RecyclerView.Adapter<AdapterEmpresa.MyViewHolder> {

    // Lista com todos as empresas
    private List<Empresa> listaEmpresas;
    Context ctx;
    private ProgressDialog progressDialog;
    private Empresa empresa;
    private String novaData;

    public AdapterEmpresa(List<Empresa> lista, Context ctx) {
        this.listaEmpresas = lista;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mainCardViewLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cartao_recycler_empresa, parent, false);
        // Define o layout do item
        return new MyViewHolder(mainCardViewLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // Seta os dados do cliente para exibição
        Empresa empresa = listaEmpresas.get( position );

        holder.idTexto.setText("Id: ");
        holder.idEmpresa.setText(""+empresa.getId());
        holder.fantasia.setText(empresa.getFantasia());
        holder.aviso.setText(empresa.getData_aviso());
        holder.vencimento.setText(Util.formatarDataBr(empresa.getData_vencimento()));
        holder.acesso.setText("Último acesso: " + Util.formatarDataBr(empresa.getUltimoAcesso()));

        if(empresa.getStatus_financa() == 0) {
            holder.status.setText("Pendente");
        } else {
            holder.status.setText("Pago");
        }
    }

    @Override
    public int getItemCount() {
        // Define a quantidade de itens na RecyclerView
        // Retornar tamanho da lista se não for nula
        return listaEmpresas != null ? listaEmpresas.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{

        // Criar objetos para exibir os itens mantendo a visualização
        // Cada item é um objeto da MyViewHolder

        TextView fantasia;
        TextView vencimento;
        TextView aviso;
        TextView idTexto;
        TextView idEmpresa;
        TextView status;
        TextView acesso;

        public MyViewHolder(View itemView){
            super(itemView);

            fantasia = itemView.findViewById(R.id.text_fantasia);
            vencimento = itemView.findViewById(R.id.text_vencimento);
            aviso = itemView.findViewById(R.id.text_aviso);
            idTexto = itemView.findViewById(R.id.text_id);
            idEmpresa = itemView.findViewById(R.id.text_idEmpresa);
            status = itemView.findViewById(R.id.text_status);
            acesso = itemView.findViewById(R.id.text_acesso);

            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            menu.setHeaderTitle(fantasia.getText().toString());
            menu.add("Confirmar pagamento").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                    builder.setTitle("Liberar acesso")
                            .setMessage("Tem certeza que deseja confirmar que o pagamento foi realizado?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    empresa = new Empresa();

                                    empresa.setId(Integer.parseInt(idEmpresa.getText().toString()));
                                    empresa.setData_vencimento(Util.maisUMmes(Util.formatarDataAm(vencimento.getText().toString())));
                                    empresa.setData_aviso(Util.maisUMmes(aviso.getText().toString()));
                                    empresa.setStatus_financa(1);

                                    progressDialog = new ProgressDialog(ctx);
                                    progressDialog.show();
                                    progressDialog.setContentView(R.layout.progress_dialog);
                                    progressDialog.getWindow().setBackgroundDrawableResource(
                                            android.R.color.transparent
                                    );

                                    new Thread() {
                                        @Override
                                        public void run() {

                                            liberarAcessoEmpresa();
                                            progressDialog.dismiss();

                                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                @Override
                                                public void run() {

                                                    vencimento.setText(Util.formatarDataBr(empresa.getData_vencimento()));
                                                    status.setText(empresa.getStatus_financa() != 0 ? "Pago" : "Pendente");

                                                    Toast.makeText(ctx, "Pagamento confirmado!", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    }.start();
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .show();
                    return true;
                }
            });

            menu.add("Ajustar vencimento").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    empresa = new Empresa();

                    if(!vencimento.getText().toString().isEmpty()){

                        String dataSelecionada = vencimento.getText().toString();
                        String s[] = dataSelecionada.split("-");

                        int mYear = Integer.parseInt(s[2]);
                        int mMonth = Integer.parseInt(s[1]);
                        int mDay = Integer.parseInt(s[0]);
                        mMonth--;

                        DatePickerDialog dateDialog= new DatePickerDialog(ctx, datePickerListener, mYear, mMonth, mDay);
                        dateDialog.show();

                        dateDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dateDialog.getDatePicker().clearFocus();

                                        int dia = dateDialog.getDatePicker().getDayOfMonth();
                                        int mes = dateDialog.getDatePicker().getMonth();
                                        String ano = ""+dateDialog.getDatePicker().getYear();

                                        mes++;
                                        if(mes < 10){
                                            mes = Integer.parseInt("0"+mes);
                                        }
                                        if(dia < 10){
                                            dia = Integer.parseInt("0"+dia);
                                        }

                                        novaData = ano+"-"+mes+"-"+dia;

                                        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                                        builder.setTitle("Ajustar vencimento")
                                                .setMessage("Tem certeza que deseja ajustar data de vencimento para " + Util.formatarDataBr(novaData) + "?")
                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        empresa.setId(Integer.parseInt(idEmpresa.getText().toString()));
                                                        empresa.setData_vencimento(novaData);

                                                        progressDialog = new ProgressDialog(ctx);
                                                        progressDialog.show();
                                                        progressDialog.setContentView(R.layout.progress_dialog);
                                                        progressDialog.getWindow().setBackgroundDrawableResource(
                                                                android.R.color.transparent
                                                        );

                                                        new Thread() {
                                                            @Override
                                                            public void run() {

                                                                alterarVencimentoEmpresa();
                                                                progressDialog.dismiss();

                                                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                                    @Override
                                                                    public void run() {

                                                                        /*novaData = dateYouChoosed;*/
                                                                        vencimento.setText(Util.formatarDataBr(novaData));

                                                                        Toast.makeText(ctx, "Nova data de vencimento confirmada!", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });
                                                            }
                                                        }.start();
                                                    }
                                                })
                                                .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                })
                                                .show();
                                    }
                                });
                        dateDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dateDialog.getDatePicker().clearFocus();
                                    }
                                });
                    } else {
                        Toast.makeText(ctx, "Data de vencimento inválida!", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });
        }
    }

    public void liberarAcessoEmpresa(){
        Controller_Conexao cc = new Controller_Conexao();
        cc.liberarAcessoEmpresa(empresa);
        cc.listarEmpresas(ctx);
    }

    public void alterarVencimentoEmpresa(){
        Controller_Conexao cc = new Controller_Conexao();
        cc.alterarVencimntoEmpresa(empresa);
        cc.listarEmpresas(ctx);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            monthOfYear++;

            String mes = ""+monthOfYear;
            String dia = ""+dayOfMonth;

            if(monthOfYear < 10){
                mes = "0"+monthOfYear;
            }

            if(dayOfMonth < 10){
                dia = "0"+dayOfMonth;
            }

            String dateYouChoosed = year + "-" + mes + "-" + dia;
        }
    };
}
