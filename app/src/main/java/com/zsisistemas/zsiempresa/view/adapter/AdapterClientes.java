package com.zsisistemas.zsiempresa.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ClienteModel;
import com.zsisistemas.zsiempresa.model.Cupom_itemModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom_item;
import com.zsisistemas.zsiempresa.view.ui.clientes.CadastroClientesFragment;
import com.zsisistemas.zsiempresa.view.ui.clientes.ClientesFragment;

import java.util.ArrayList;
import java.util.List;

public class AdapterClientes extends RecyclerView.Adapter<AdapterClientes.MyViewHolder> {

    // Lista com todos os clientes
    private List<ClienteModel> listaClientes;
    ClientesFragment clientesFragment = new ClientesFragment();
    Context ctx;

    public AdapterClientes(List<ClienteModel> lista, Context ctx) {
        this.listaClientes = lista;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mainCardViewLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cartao_recycler_cliente, parent, false);
        // Define o layout do item
        return new MyViewHolder(mainCardViewLista);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // Seta os dados do cliente para exibição
        ClienteModel cliente = listaClientes.get( position );
        List<Cupom_itemModel> listaPedido = new ArrayList<>();

        // Define dados default de todos os cartoes do adapter programaticamente, pois, o adapter se recusa a aceitar
        // os dados default que ta vindo do cartão
        holder.cardView.setBackgroundColor((Color.parseColor("#FFFFFF")));
        holder.telefone.setVisibility(View.VISIBLE);
        holder.disponibilidade.setVisibility(View.GONE);
        holder.perfil.setVisibility(View.VISIBLE);
        cliente.setObservacao("disponível");

        holder.nomeCliente.setText(cliente.getNome());
        holder.idTexto.setText("Id: ");
        holder.idCliente.setText(String.valueOf(cliente.getId()));
        holder.telefone.setText("Telefone: " + cliente.getTelefone());
        holder.perfil.setText("perfil: " + cliente.getPerfil());

        if (cliente.getPerfil().equalsIgnoreCase("mesa")) {

            listaPedido = SingletonCupom_item.getCupom_Item().getListaPedido();

            if(listaPedido != null ){
                for (Cupom_itemModel cupom_item:listaPedido) {
                    if (cliente.getNome().equalsIgnoreCase(cupom_item.getNomeCliete())) {
                        cliente.setObservacao("OCUPADO!");

                        //TODO CONTROLA 'OPACIDADE' DO CARTÂO
                        holder.cardView.setBackgroundColor((Color.parseColor("#906F6F6F")));

                    }
                }
            }

            holder.telefone.setVisibility(View.GONE);
            holder.perfil.setVisibility(View.GONE);
            holder.disponibilidade.setVisibility(View.VISIBLE);
            holder.disponibilidade.setText("" + cliente.getObservacao());
        }
    }

    @Override
    public int getItemCount() {
        // Define a quantidade de itens na RecyclerView
        // Retornar tamanho da lista se não for nula
        return listaClientes != null ? listaClientes.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{

        // Criar objetos para exibir os itens mantendo a visualização
        // Cada item é um objeto da MyViewHolder

        TextView nomeCliente;
        TextView idTexto;
        TextView idCliente;
        TextView telefone;
        TextView perfil;
        TextView disponibilidade;
        ImageButton botaoMais;
        CardView cardView;

        public MyViewHolder(View itemView){
            super(itemView);

            nomeCliente = itemView.findViewById(R.id.text_nome);
            idTexto = itemView.findViewById(R.id.text_id);
            idCliente = itemView.findViewById(R.id.text_idCliente);
            telefone = itemView.findViewById(R.id.text_telefone);
            perfil = itemView.findViewById(R.id.text_perfil);
            disponibilidade = itemView.findViewById(R.id.text_disponibilidade);
            botaoMais = itemView.findViewById(R.id.imageButton_more);
            cardView = itemView.findViewById(R.id.card_cartao_cliente);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            Util.closeKeyboard(v, ctx);

            menu.setHeaderTitle(nomeCliente.getText().toString());
            menu.add("Excluir").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {

                    //TODO criar dialogo de confirmaçao para invalidae o cliente

                    return true;
                }
            });

            menu.add("Editar").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    CadastroClientesFragment cadastroClientesFragment = new CadastroClientesFragment();

                    cadastroClientesFragment.checkCliente = idCliente.getText().toString();

                    clientesFragment.botaoCadastroCliente.performClick();
                    return true;
                }
            });
        }
    }
}
