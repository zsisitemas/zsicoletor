package com.zsisistemas.zsiempresa.view.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ClienteModel;
import com.zsisistemas.zsiempresa.model.ConfiguracoesModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.google.android.material.textfield.TextInputLayout;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom_item;
import com.zsisistemas.zsiempresa.view.ui.carrinho.ProdutosPedidosFragment;

import java.util.ArrayList;
import java.util.List;

import static com.zsisistemas.zsiempresa.controller.Util.limpaSingletons;

public class AberturaActivity extends AppCompatActivity {

    private View mDecorView;
    private TextInputLayout inputLogin, inputSenha;
    private TextView labelNaotenhoLogin;
    private Button botaoEntrar;
    private CheckBox checkLembrar;
    private String mResult = "";
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";
    private List<ClienteModel> listaFuncionario;
    private List<ConfiguracoesModel> listaChaves;
    /*private List<Chave> listaChaves;*/
    Controller_Conexao controller_conexao;
    private ProgressDialog progressNoDialog;
    private ProdutosPedidosFragment produtosPedidosFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abertura);

        labelNaotenhoLogin = findViewById(R.id.id_naotenhologin);
        inputLogin = findViewById(R.id.input_login);
        inputSenha = findViewById(R.id.input_senha);
        botaoEntrar = findViewById(R.id.button_entrar);
        checkLembrar = findViewById(R.id.check_lembrar);
        listaFuncionario = new ArrayList<>();
        listaChaves = new ArrayList<>();
        controller_conexao = new Controller_Conexao();
        produtosPedidosFragment = new ProdutosPedidosFragment();

        SingletonConfiguracoes.getConfig().setConnection("0");
        SingletonConfiguracoes.getConfig().setNomeBanco("u323187073_zsikey");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mDecorView = getWindow().getDecorView();
        mDecorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if (visibility == 0){
                    mDecorView.setSystemUiVisibility(hideSystemBars());
                }
            }
        });

        progressNoDialog = new ProgressDialog(this);
        progressNoDialog.show();
        progressNoDialog.setCancelable(false);
        progressNoDialog.setContentView(R.layout.progress_no_dialog);
        progressNoDialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent
        );

        new Thread() {

            @Override
            public void run() {
                metodoChaves();
                listaFuncionario = SingletonCliente.getCliente().getListaFuncionario();
                listaChaves = SingletonConfiguracoes.getConfig().getListaDadosBanco();
                /*listaChaves = SingletonChave.getChave().getListaClientesChaves();*/
                if(listaFuncionario == null){
                    progressNoDialog.dismiss();
                  //  finish();
                } else {
                    progressNoDialog.dismiss();
                }
            }
        }.start();

        SharedPreferences preferencesLoginFuncionario = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        if (preferencesLoginFuncionario.contains("lembrarFun")){
            String lembrarFun = preferencesLoginFuncionario.getString("lembrarFun", "lembrarFun não definido");

            SingletonConfiguracoes.getConfig().setListaLembrarLogin(lembrarFun);

            mResult = lembrarFun;
        }

        if (mResult.equals("lembrar")){

            checkLembrar.setChecked(true);
            botaoEntrar.setEnabled(true);

            if (preferencesLoginFuncionario.contains("loginFun")){
                String loginFun = preferencesLoginFuncionario.getString("loginFun", null);
                String senhaFun = preferencesLoginFuncionario.getString("senhaFun", null);

                SingletonCliente.getCliente().setLoginFuncionario(loginFun);
                SingletonCliente.getCliente().setSenhaFuncionario(senhaFun);

                inputLogin.getEditText().setText(loginFun);
                inputSenha.getEditText().setText(senhaFun);
            }

        } else {
            String loginFun = preferencesLoginFuncionario.getString("loginFun", null);

            SingletonCliente.getCliente().setLoginFuncionario(loginFun);

            inputLogin.getEditText().setText(loginFun);
        }

        if (SingletonConfiguracoes.getConfig().getListaLembrarLogin() != null){
            mResult = SingletonConfiguracoes.getConfig().getListaLembrarLogin();
        }

        checkLembrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkLembrar.isChecked()){
                    mResult = "lembrar";
                } else {
                    mResult = "";
                }
            }
        });

        labelNaotenhoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferencesPerfilFun = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                SharedPreferences.Editor editor = preferencesPerfilFun.edit();

                if (preferencesPerfilFun.contains("chaveId")) { // Ja teve algum user logado antes
                    restauraSharedPreferences();
                } else {
                    Util.restauraSharedPreferences(getApplication(),ARQUIVO_PREFERENCIA);
                    limpaSingletons();
                }

                finish();
            }
        });
        
        botaoEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validarLogin()) {
                    if(listaFuncionario != null){
                        Toast.makeText(getApplicationContext(), "Login e senha não correspondem. Tente novamente!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Primeiro ajuste as configurações", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String loginFuncionario = inputLogin.getEditText().getText().toString().trim();
                    String senhaFuncionario = inputSenha.getEditText().getText().toString().trim();

                    SharedPreferences preferencesLoginFuncionario = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                    SharedPreferences.Editor editor = preferencesLoginFuncionario.edit();

                    String loginFun = loginFuncionario;
                    String senhaFun = senhaFuncionario;
                    String lembrarFun = mResult;

                    editor.putString("loginFun", loginFun);
                    editor.putString("senhaFun", senhaFun);
                    editor.putString("lembrarFun", lembrarFun);
                    editor.apply();

                    SingletonCliente.getCliente().setLoginFuncionario(loginFun);
                    SingletonCliente.getCliente().setSenhaFuncionario(senhaFun);
                    SingletonConfiguracoes.getConfig().setListaLembrarLogin(lembrarFun);

                    SingletonConfiguracoes.getConfig().setListaLembrarLogin(mResult);

                    progressNoDialog.show();
                    progressNoDialog.setCancelable(false);
                    progressNoDialog.setContentView(R.layout.progress_no_dialog);
                    progressNoDialog.getWindow().setBackgroundDrawableResource(
                            android.R.color.transparent
                    );

                    new Thread() {

                        @Override
                        public void run() {
                            metodos();
                            progressNoDialog.dismiss();
                            finish();
                        }
                    }.start();
                }
            }
        });

        inputLogin.getEditText().addTextChangedListener(loginTextWatcher);
        inputSenha.getEditText().addTextChangedListener(loginTextWatcher);
    }

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String loginFuncionario = inputLogin.getEditText().getText().toString().trim();
            String senhaFuncionario = inputSenha.getEditText().getText().toString().trim();

            botaoEntrar.setEnabled(!loginFuncionario.isEmpty() && !senhaFuncionario.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private boolean validarLogin() {
        String loginFuncionario = inputLogin.getEditText().getText().toString().trim();
        String senhaFuncionario = inputSenha.getEditText().getText().toString().trim();
        boolean retorno = false;

        if (loginFuncionario.isEmpty() | senhaFuncionario.isEmpty()) {
            retorno = false;
        } else {

            if(listaFuncionario != null){
                int posicao = 0;
                for (ClienteModel c:listaFuncionario) {
                    String login = c.getLoginFuncionario();
                    String senha = c.getSenhaFuncionario();

                    if (!loginFuncionario.equalsIgnoreCase(login) | !senhaFuncionario.equals(senha)) {
                        retorno = false;
                    } else {//login, senha, perfil, nome

                        SharedPreferences preferencesPerfilFun = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                        SharedPreferences.Editor editor = preferencesPerfilFun.edit();

                        if (preferencesPerfilFun.contains("chaveId")) { // Ja teve algum user logado antes
                            String chaveId = preferencesPerfilFun.getString("chaveId", "chaveId não definido");

                            if (!chaveId.equals(c.getChaveId())) { // Outro user tentando fazer login
                                restauraSharedPreferences();
                            }
                        } else {
                            restauraSharedPreferences();
                        }

                        String perfilFun = c.getPerfil();
                        editor.putString("perfilFun", perfilFun);
                        editor.apply();
                        SingletonCliente.getCliente().setPerfil(perfilFun);

                        String nomeUsuario = c.getNome();
                        editor.putString("nomeUsuario", nomeUsuario);
                        editor.apply();
                        SingletonCliente.getCliente().setNome(nomeUsuario);

                        String chaveId = c.getChaveId();
                        editor.putString("chaveId", chaveId);
                        editor.apply();
                        SingletonCliente.getCliente().setChaveId(chaveId);

                        ConfiguracoesModel config = listaChaves.get(posicao);

                        // TODO aqui setar dados do banco de acordo com o usuario logado
                        if(!config.getConnection().equals("3")){
                            String conn = config.getConnection();
                            editor.putString("conn", conn);
                            editor.apply();
                            SingletonConfiguracoes.getConfig().setConnection(conn);

                            String nome = config.getNomeBanco();
                            editor.putString("nome", nome);
                            editor.commit();
                            SingletonConfiguracoes.getConfig().setNomeBanco(nome);

                            String ip = config.getIp();
                            editor.putString("ip", ip);
                            editor.commit();
                            SingletonConfiguracoes.getConfig().setIp(ip);
                        } else {

                            // Se usuario que logou tem conexao SQLite(3) a ordem de setar no sharedPreferences muda.
                            String conn = config.getConnection();
                            editor.putString("conn", conn);
                            editor.apply();
                            SingletonConfiguracoes.getConfig().setConnection(conn);

                            String nome = "local";
                            editor.putString("nome", nome);
                            editor.commit();
                            SingletonConfiguracoes.getConfig().setNomeBanco(nome);

                            String ip = "127.0.0.1";
                            editor.putString("ip", ip);
                            editor.commit();
                            SingletonConfiguracoes.getConfig().setIp(ip);

                            String chave = config.getNomeBanco();
                            editor.putString("chave", chave);
                            editor.commit();
                            SingletonConfiguracoes.getConfig().setChave(chave);

                            String validade = config.getIp();
                            editor.putString("validade", validade);
                            editor.commit();
                            SingletonConfiguracoes.getConfig().setChave(validade);
                        }

                        //SingletonConfiguracoes.getConfig()

                        inputLogin.setError(null);
                        inputSenha.setError(null);

                        retorno = true;
                        return true;
                    }
                    posicao++;
                }
            }
            return false;
        }

        if (!retorno){
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    public void metodos(){
        controller_conexao.listarProdutos(this);
        controller_conexao.listarValidades(this);
        controller_conexao.listarEmpresas(this);

        String conexao = SingletonConfiguracoes.getConfig().getConnection();
        String perfil = SingletonCliente.getCliente().getPerfil();

        if (conexao.equals("2") && perfil.equalsIgnoreCase("funcionario")) {
            controller_conexao.listaMesa(this);
            produtosPedidosFragment.listarPedido(this);
            Log.d("listaMesa", "conexao: " + conexao + "\n perfil: " + perfil);
        } else {
            if (conexao.equals("2")) {
                produtosPedidosFragment.listarPedido(this);
            }
            controller_conexao.listaCliete(this);
            Log.d("listaCliete()", "conexao: " + conexao + "\n perfil: " + perfil);
        }
    }

    public void metodoChaves(){
        controller_conexao.listaChavesCliete(this);
    }

    /*public void listaPedidos() {
        SingletonCupom_item.getCupom_Item().setListaPedido(null);
        Controller_Conexao controller_conexao = new Controller_Conexao();

        SharedPreferences preferencesLoginFuncionario = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        String funLogado = "";

        if (preferencesLoginFuncionario.contains("loginFun")){
            String loginFun = preferencesLoginFuncionario.getString("loginFun", null);

            SingletonCliente.getCliente().setLoginFuncionario(loginFun);

            funLogado = SingletonCliente.getCliente().getLoginFuncionario();
        } else {
            SingletonCliente.getCliente().setLoginFuncionario(null);
        }

        SharedPreferences preferencesPerfilFun = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        if(preferencesPerfilFun.contains("perfilFun")){
            String perfilFun = preferencesPerfilFun.getString("perfilFun", "perfilFun não definido");

            SingletonCliente.getCliente().setPerfil(perfilFun);

            if(perfilFun.equalsIgnoreCase("admin")
                    || perfilFun.equalsIgnoreCase("dev")){
                controller_conexao.listarCupom(getApplicationContext().getApplicationContext());
            } else {
                controller_conexao.listarCupomFun(getApplicationContext().getApplicationContext(), funLogado);
            }
        } else {
            controller_conexao.listarCupomFun(getApplicationContext().getApplicationContext(), funLogado);
        }
    }*/

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            mDecorView.setSystemUiVisibility(hideSystemBars());
        }
    }

    // Este trecho esconde as barras do sistema.
    private int hideSystemBars() {
        // Configurar a IMMERSIVE flag.
        // Configurar o conteúdo para aparecer embaixo das barras do sistema para que o conteúdo
        // não redimensione  quando as barras do sistema sumir ou aparecer.
                return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // Esconder barra de status
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION; // Esconder barra de navegação
    }

    public void restauraSharedPreferences() {
        SharedPreferences preferencesIdCliente = getApplicationContext().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        SharedPreferences.Editor editor = preferencesIdCliente.edit();

        Util.restauraSharedPreferences(getApplication(),ARQUIVO_PREFERENCIA);
        Util.limparBanco(getApplicationContext());

        // Limpar dados do cliente selecionado do carrinho
        String idCli = null;
        String nomeCli = null;
        String telefoneCli = null;
        int numeroMesa = 0;
        String obs = null;

        editor.putString("idCli", idCli);
        editor.putString("nomeCli", nomeCli);
        editor.putString("telefoneCli", telefoneCli);
        editor.putInt("numeroMesa", numeroMesa);
        editor.putString("obs", obs);
        editor.apply();
    }
}
