package com.zsisistemas.zsiempresa.view.ui.clientes;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.model.ClienteModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import static com.zsisistemas.zsiempresa.controller.Util.closeKeyboard;

public class CadastroClientesFragment extends Fragment {

    private ClienteModel cliente;
    private TextInputLayout nomeCliente, cpf, sexo, telefone, e_mail,endereco,bairro,complemento;
    private Button botaoSalvarCliente, botaoCancelar;
    private ProgressDialog progressDialog;
    private List<ClienteModel> listaClientes = new ArrayList<>();
    public static String checkCliente = ""; // vazio novo cliente, vindo com id atualizar.

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cadastro_cliente, container, false);
        cliente = new ClienteModel();

        nomeCliente = view.findViewById(R.id.edit_nome_cliente);
        cpf = view.findViewById(R.id.edit_cpf);
        sexo = view.findViewById(R.id.edit_sexo);
        telefone = view.findViewById(R.id.edit_telefone);
        e_mail = view.findViewById(R.id.edit_email);
        endereco = view.findViewById(R.id.edit_endereco);
        bairro = view.findViewById(R.id.edit_bairro);
        complemento = view.findViewById(R.id.edit_complemento);
        botaoSalvarCliente = view.findViewById(R.id.button_salvar_cliente);
        botaoCancelar = view.findViewById(R.id.button_cancelar);
        listaClientes = SingletonCliente.getCliente().getListaclientes();
        telefone.getEditText().addTextChangedListener(cadastroTextWatcher);

        botaoSalvarCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(view, getActivity());

                cliente.setNome(nomeCliente.getEditText().getText().toString());

                if(cpf.getEditText().getText().toString().equals("")){
                    cliente.setCpf(telefone.getEditText().getText().toString());
                }else{
                    cliente.setCpf(cpf.getEditText().getText().toString());
                }

                cliente.setSexo(sexo.getEditText().getText().toString());
                cliente.setTelefone(telefone.getEditText().getText().toString());
                cliente.setE_mail(e_mail.getEditText().getText().toString());
                cliente.setRua(endereco.getEditText().getText().toString()); // Endereço
                cliente.setBairro(bairro.getEditText().getText().toString());
                cliente.setComplemento(complemento.getEditText().getText().toString());

                if(nomeCliente.getEditText().getText().toString().trim().isEmpty()){
                    nomeCliente.setError("Nome não pode ser vazio");
                    nomeCliente.requestFocus();
                } else {
                    nomeCliente.setError(null);
                    nomeCliente.setErrorEnabled(false);
                }

                if(telefone.getEditText().getText().toString().trim().isEmpty()){
                    telefone.setError("Telefone não pode ser vazio");

                    if(nomeCliente.getEditText().getText().toString().trim().isEmpty()){
                        nomeCliente.requestFocus();
                    }else {
                        telefone.requestFocus();
                    }
                } else {
                    telefone.setError(null);
                    telefone.setErrorEnabled(false);
                }

                if(nomeCliente.getEditText().getText().toString().trim().isEmpty() ||
                   telefone.getEditText().getText().toString().trim().isEmpty()){
                } else {

                    boolean exists = false;

                    if(checkCliente.trim().isEmpty()){
                        for (ClienteModel c:listaClientes) {
                            if(c.getNome().equalsIgnoreCase(nomeCliente.getEditText().getText().toString().trim())){
                                nomeCliente.setError("Este nome ja pertence a um cliente.");
                                nomeCliente.requestFocus();
                                exists = true;
                            }
                            if(c.getTelefone().equalsIgnoreCase(telefone.getEditText().getText().toString().trim())){
                                telefone.setError("Este telefone ja pertence a um cliente.");

                                if(exists){
                                    nomeCliente.requestFocus();
                                }else {
                                    telefone.requestFocus();
                                    exists = true;
                                }

                                if(exists){
                                    break;
                                } else {
                                    nomeCliente.setError(null);
                                    telefone.setError(null);
                                    nomeCliente.setErrorEnabled(false);
                                    telefone.setErrorEnabled(false);
                                }
                            }
                        }
                    }

                    if(!exists){

                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.show();
                        progressDialog.setCancelable(false);
                        progressDialog.setContentView(R.layout.progress_dialog);
                        progressDialog.getWindow().setBackgroundDrawableResource(
                                android.R.color.transparent
                        );

                        new Thread() {
                            @Override
                            public void run() {

                                metodos();
                                progressDialog.dismiss();

                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        if(botaoSalvarCliente.getText().equals("ATUALIZAR")){
                                            Toast.makeText(getActivity(),"Cliente atualizado!",Toast.LENGTH_SHORT).show();
                                            botaoSalvarCliente.setText("Salvar");
                                        }else {
                                            Toast.makeText(getActivity(),"Cliente cadastrado!",Toast.LENGTH_SHORT).show();
                                        }

                                        clear();
                                    }
                                });
                            }
                        }.start();
                    }
                }
            }
        });

        botaoCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!checkCliente.trim().isEmpty()){

            botaoCancelar.setVisibility(View.VISIBLE);
            botaoSalvarCliente.setText("ATUALIZAR");

            int id = Integer.parseInt(checkCliente);

            for (ClienteModel c:listaClientes) {
                if(c.getId() == id){

                    nomeCliente.getEditText().setText(c.getNome().trim());

                    cpf.getEditText().setText(c.getCpf().trim());

                    telefone.getEditText().setText(c.getTelefone().trim());

                    if(!c.getCpf().equals(c.getTelefone())){
                        cpf.getEditText().setText(c.getCpf().trim());
                    }else {
                        cpf.getEditText().setText(c.getTelefone().trim());
                    }

                    if(c.getSexo() != null){
                        sexo.getEditText().setText(c.getSexo().trim());
                    }

                    if(c.getE_mail() != null){
                        e_mail.getEditText().setText(c.getE_mail().trim());
                    }

                    if(c.getRua() != null){
                        endereco.getEditText().setText(c.getRua().trim());
                    }

                    if(c.getBairro() != null){
                        bairro.getEditText().setText(c.getBairro().trim());
                    }

                    if(c.getComplemento() != null){
                        complemento.getEditText().setText(c.getComplemento().trim());
                    }
                }
            }
        }else {
            clear();
        }
    }

    public void metodos(){
        Controller_Conexao controller_conexao = new Controller_Conexao();

        if(!checkCliente.trim().isEmpty()){
            int id = Integer.parseInt(checkCliente);

            controller_conexao.atualizarCliete(cliente, id);
        }else {
            controller_conexao.salvarCliente(getActivity(),cliente);
        }

        controller_conexao.listaCliete(getActivity());
    }

    private TextWatcher cadastroTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            String numeroTelefone = telefone.getEditText().getText().toString().trim();
            String numeroCpf = cpf.getEditText().getText().toString().trim();
                if (numeroCpf.equals(numeroTelefone)){
                    cpf.getEditText().setText(null);
                }
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String numeroTelefone = telefone.getEditText().getText().toString().trim();
            String numeroCpf = cpf.getEditText().getText().toString().trim();
                if (numeroCpf.isEmpty()){
                    cpf.getEditText().setText(numeroTelefone);
                }
        }
        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public void clear(){

        nomeCliente.getEditText().setText("");
        cpf.getEditText().setText("");
        sexo.getEditText().setText("");
        telefone.getEditText().setText("");
        e_mail.getEditText().setText("");
        endereco.getEditText().setText("");
        bairro.getEditText().setText("");
        complemento.getEditText().setText("");

        botaoSalvarCliente.setText("Salvar");
        checkCliente = "";

        nomeCliente.setError(null);
        telefone.setError(null);
        nomeCliente.setErrorEnabled(false);
        telefone.setErrorEnabled(false);

        botaoCancelar.setVisibility(View.GONE);
    }
}