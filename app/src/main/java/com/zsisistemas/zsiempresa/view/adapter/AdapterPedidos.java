package com.zsisistemas.zsiempresa.view.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.Cupom_itemModel;

import java.util.List;

import static android.view.View.GONE;

public class AdapterPedidos extends RecyclerView.Adapter<AdapterPedidos.MyViewHolder> {

    // Lista com todos os pedidos
    private List<Cupom_itemModel> listaPedidos;
    private double total = 0;

    public AdapterPedidos(List<Cupom_itemModel> lista) {
        this.listaPedidos = lista;
    }

    @NonNull
    @Override
    public AdapterPedidos.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mainCardViewLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cartao_recycler_pedido, parent, false);
        // Define o layout do item
        return new AdapterPedidos.MyViewHolder(mainCardViewLista);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull AdapterPedidos.MyViewHolder holder, int position) {
        // Seta os dados do produto para exibição
        Cupom_itemModel cupom_itemModel = listaPedidos.get( position );

        holder.nomeCliente.setText("Cliente: " + cupom_itemModel.getNomeCliete());
        holder.numeroComanda.setText("Pedido: " + cupom_itemModel.getComanda());
        holder.numeroTotal.setText("Subtotal R$: " + Util.formataDouble(cupom_itemModel.getSubTotal()));

        if (cupom_itemModel.getTipoMesa().equals("0")){
            holder.telefoneCliente.setVisibility(View.GONE);
        } else {
            holder.telefoneCliente.setText("Telefone: " + cupom_itemModel.getFone());
        }

        if (cupom_itemModel.getAlterado() == 2) {
            // Pedido editado
            holder.dataHora.setText("Editado: " + Util.formatarDataHora(cupom_itemModel.getData()));
        } else {
            holder.dataHora.setText(Util.formatarDataHora(cupom_itemModel.getData()));
        }
    }

    @Override
    public int getItemCount() {
        // Define a quantidade de itens na RecyclerView
        // Retornar tamanho da lista se não for nula
        return listaPedidos != null ? listaPedidos.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        // Criar objetos para exibir os itens mantendo a visualização
        // Cada item é um objeto da MyViewHolder

        TextView numeroTotal;
        TextView numeroComanda;
        TextView nomeCliente;
        TextView telefoneCliente;
        TextView dataHora;
        ImageButton botaoEditPedido;

        public MyViewHolder(View itemView){
            super(itemView);

            numeroTotal = itemView.findViewById(R.id.text_numero_cupom);
            numeroComanda = itemView.findViewById(R.id.text_numero_mesa);
            nomeCliente = itemView.findViewById(R.id.text_nome_cliente);
            telefoneCliente = itemView.findViewById(R.id.text_telefone_cliente);
            botaoEditPedido = itemView.findViewById(R.id.imageButton_edit);
            dataHora = itemView.findViewById(R.id.text_hora);

        }
    }
}

