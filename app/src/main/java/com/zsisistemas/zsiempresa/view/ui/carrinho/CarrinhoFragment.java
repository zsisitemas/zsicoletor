package com.zsisistemas.zsiempresa.view.ui.carrinho;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCupom_item;

public class CarrinhoFragment extends Fragment {

    public static Button botaoCarrinho, botaoPedidos;
    private ProdutosCarrinhoFragment produtosCarrinhoFragment;
    private ProdutosPedidosFragment produtosPedidosFragment;
    public static View viewCarrinho, viewPedidos;
    public static TextView textoDefault, idCliente, nomeCliente;
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_carrinho, container, false);

        botaoCarrinho = view.findViewById(R.id.button_carrinho);
        botaoPedidos = view.findViewById(R.id.button_pedidos);
        viewCarrinho = view.findViewById(R.id.view_carrinho);
        viewPedidos = view.findViewById(R.id.view_pedidos);
        produtosCarrinhoFragment = new ProdutosCarrinhoFragment();
        produtosPedidosFragment = new ProdutosPedidosFragment();

        textoDefault = view.findViewById(R.id.text_default);
        idCliente = view.findViewById(R.id.text_id_cliente);
        nomeCliente = view.findViewById(R.id.text_nome_cliente);
        idCliente.setText("");
        nomeCliente.setText("");

        SharedPreferences preferencesIdCliente = getActivity().getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        if (preferencesIdCliente.contains("idCli")){
            String idCli = preferencesIdCliente.getString("idCli", "idCli não definido");
            String nomeCli = preferencesIdCliente.getString("nomeCli", "nomeCli não definido");
            String telefoneCli = preferencesIdCliente.getString("telefoneCli", "telefoneCli não definido");
            int numeroMesa = preferencesIdCliente.getInt("numeroMesa", 0);

            SingletonCliente.getCliente().setIdClienteSelecionado(Integer.parseInt(idCli)); // Setar dados salvos do celular no ipConfigurações
            SingletonCliente.getCliente().setNomeClienteSelecionado(nomeCli);
            SingletonCliente.getCliente().setTelefoneClienteSelecionado(telefoneCli);
            SingletonCupom_item.getCupom_Item().setNumeroMesa(numeroMesa);

            idCliente.setText(idCli);
            nomeCliente.setText(" - " + nomeCli);
        } else {
            textoDefault.setText("não selecionado!");
        }

        botaoPedidos.setTextColor(Color.parseColor("#CCD6D7D7"));
        botaoCarrinho.setTextColor(Color.parseColor("#FFFFFF"));

        viewPedidos.setBackgroundColor(Color.parseColor("#00000000"));
        viewCarrinho.setBackgroundColor(Color.parseColor("#FFFFFF"));
        // Configurar o objeto para o fragment
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_conteudo, produtosCarrinhoFragment);
        transaction.commit();

        botaoCarrinho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                botaoPedidos.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoCarrinho.setTextColor(Color.parseColor("#FFFFFF"));

                viewPedidos.setBackgroundColor(Color.parseColor("#00000000"));
                viewCarrinho.setBackgroundColor(Color.parseColor("#FFFFFF"));

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_conteudo, produtosCarrinhoFragment);
                transaction.commit();
            }
        });

        botaoPedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_conteudo, produtosPedidosFragment);
                transaction.commit();

                botaoCarrinho.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoPedidos.setTextColor(Color.parseColor("#FFFFFF"));

                viewCarrinho.setBackgroundColor(Color.parseColor("#00000000"));
                viewPedidos.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        });

        return view;
    }
}