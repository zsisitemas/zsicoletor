package com.zsisistemas.zsiempresa.view.ui.navProdutos.consulta;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ClienteModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;
import com.zsisistemas.zsiempresa.view.ui.clientes.CadastroClientesFragment;
import com.zsisistemas.zsiempresa.view.ui.clientes.HistoricoClienteFragment;
import com.zsisistemas.zsiempresa.view.ui.clientes.ListaClientesFragment;

public class ColetaFragment extends Fragment {

    public static Button botaoColeta, botaoColetar, botaoColetados;
    private ConsultaFragment consultaFragment;
    private ProdutosColetarFragment produtosColetarFragment;
    private ProdutosColetadosFragment produtosColetadosFragment;
    private View viewColeta, viewHistorico, viewColetados;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_coleta, container, false);

        botaoColeta = view.findViewById(R.id.button_coleta);
        botaoColetar = view.findViewById(R.id.button_coletar);
        botaoColetados = view.findViewById(R.id.button_coletados);
        viewColeta = view.findViewById(R.id.view_coleta);
        viewHistorico = view.findViewById(R.id.view_coletar);
        viewColetados = view.findViewById(R.id.view_coletados);
        consultaFragment = new ConsultaFragment();
        produtosColetarFragment = new ProdutosColetarFragment();
        produtosColetadosFragment = new ProdutosColetadosFragment();

        // Configurar o objeto para o fragment

        botaoColetados.setTextColor(Color.parseColor("#CCD6D7D7"));
        botaoColetar.setTextColor(Color.parseColor("#CCD6D7D7"));
        botaoColeta.setTextColor(Color.parseColor("#FFFFFF"));

        viewColetados.setBackgroundColor(Color.parseColor("#00000000"));
        viewHistorico.setBackgroundColor(Color.parseColor("#00000000"));
        viewColeta.setBackgroundColor(Color.parseColor("#FFFFFF"));

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_conteudo, consultaFragment);
        setHasOptionsMenu(true);
        transaction.commit();

        botaoColeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                botaoColetados.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoColetar.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoColeta.setTextColor(Color.parseColor("#FFFFFF"));

                viewColetados.setBackgroundColor(Color.parseColor("#00000000"));
                viewHistorico.setBackgroundColor(Color.parseColor("#00000000"));
                viewColeta.setBackgroundColor(Color.parseColor("#FFFFFF"));

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_conteudo, consultaFragment);
                setHasOptionsMenu(true);
                transaction.commit();
            }
        });

        botaoColetar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                botaoColetados.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoColeta.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoColetar.setTextColor(Color.parseColor("#FFFFFF"));

                viewColetados.setBackgroundColor(Color.parseColor("#00000000"));
                viewColeta.setBackgroundColor(Color.parseColor("#00000000"));
                viewHistorico.setBackgroundColor(Color.parseColor("#FFFFFF"));

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_conteudo, produtosColetarFragment);
                setHasOptionsMenu(false);
                transaction.commit();

            }
        });

        botaoColetados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                botaoColeta.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoColetar.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoColetados.setTextColor(Color.parseColor("#FFFFFF"));

                viewHistorico.setBackgroundColor(Color.parseColor("#00000000"));
                viewColeta.setBackgroundColor(Color.parseColor("#00000000"));
                viewColetados.setBackgroundColor(Color.parseColor("#FFFFFF"));

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_conteudo, produtosColetadosFragment);
                setHasOptionsMenu(false);
                transaction.commit();
            }
        });
        return view;
    }
}