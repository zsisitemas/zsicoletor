package com.zsisistemas.zsiempresa.view.ui.navProdutos.validade;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.zsisistemas.zsiempresa.R;

public class ValidadeFragment extends Fragment {

    public static Button botaoAdicionarValidade, botaoListarValidade;
    private AdicionarValidadeFragment adicionarValidadeFragment;
    private ListarValidadeFragment listarValidadeFragment;
    public static View viewAdicionarValidade, viewListarValidade;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_validade, container, false);

        botaoAdicionarValidade = view.findViewById(R.id.button_adicionar_validade);
        botaoListarValidade = view.findViewById(R.id.button_listar_validade);
        viewAdicionarValidade = view.findViewById(R.id.view_adicionar_validade);
        viewListarValidade = view.findViewById(R.id.view_listar_validade);
        adicionarValidadeFragment = new AdicionarValidadeFragment();
        listarValidadeFragment = new ListarValidadeFragment();

        botaoAdicionarValidade.setTextColor(Color.parseColor("#CCD6D7D7"));
        botaoListarValidade.setTextColor(Color.parseColor("#FFFFFF"));

        viewAdicionarValidade.setBackgroundColor(Color.parseColor("#00000000"));
        viewListarValidade.setBackgroundColor(Color.parseColor("#FFFFFF"));
        
        // Configurar o objeto para o fragment
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_conteudo, listarValidadeFragment);
        transaction.commit();

        botaoAdicionarValidade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_conteudo, adicionarValidadeFragment);
                transaction.commit();

                botaoListarValidade.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoAdicionarValidade.setTextColor(Color.parseColor("#FFFFFF"));

                viewListarValidade.setBackgroundColor(Color.parseColor("#00000000"));
                viewAdicionarValidade.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        });

        botaoListarValidade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_conteudo, listarValidadeFragment);
                transaction.commit();

                botaoAdicionarValidade.setTextColor(Color.parseColor("#CCD6D7D7"));
                botaoListarValidade.setTextColor(Color.parseColor("#FFFFFF"));

                viewAdicionarValidade.setBackgroundColor(Color.parseColor("#00000000"));
                viewListarValidade.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        });

        return view;
    }
}