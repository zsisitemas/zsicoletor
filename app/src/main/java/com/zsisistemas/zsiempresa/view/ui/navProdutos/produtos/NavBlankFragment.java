package com.zsisistemas.zsiempresa.view.ui.navProdutos.produtos;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zsisistemas.zsiempresa.R;

public class NavBlankFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nav_blank, container, false);

        return view;
    }
}