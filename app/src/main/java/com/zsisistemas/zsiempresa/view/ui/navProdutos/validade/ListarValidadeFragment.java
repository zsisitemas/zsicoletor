package com.zsisistemas.zsiempresa.view.ui.navProdutos.validade;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zsisistemas.zsiempresa.R;
import com.zsisistemas.zsiempresa.controller.Controller_Conexao;
import com.zsisistemas.zsiempresa.controller.Util;
import com.zsisistemas.zsiempresa.model.ValidadeModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonValidade;
import com.zsisistemas.zsiempresa.view.adapter.AdapterValidade;

import java.util.ArrayList;
import java.util.List;

public class ListarValidadeFragment extends Fragment {

    private RecyclerView recyclerView;
    public static AdapterValidade adapterValidade;
    public static List<ValidadeModel> listaValidade = new ArrayList<>();
    public static List<ValidadeModel> listaValidadeDTO = new ArrayList<>();
    private ProgressDialog progressNoDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listar_validade, container, false);

        recyclerView = view.findViewById(R.id.recycler_view_validade);

        progressNoDialog = new ProgressDialog(getActivity());
        progressNoDialog.show();
        progressNoDialog.setCancelable(false);
        progressNoDialog.setContentView(R.layout.progress_no_dialog);
        progressNoDialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent
        );

        new Thread() {
            @Override
            public void run() {

                if(SingletonValidade.getValidade().getListaValidade() == null){
                    listaValidade.clear();
                    listarValidades();
                }

                progressNoDialog.dismiss();

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {

                        if(listaValidadeDTO != null){
                            listaValidadeDTO.clear();
                        }
                        listaValidadeDTO = SingletonValidade.getValidade().getListaValidade();

                        // Configurar Adapter
                        if(listaValidadeDTO != null) {

                            for (ValidadeModel validade : listaValidadeDTO) {
                                String diasFaltando = Util.formatarDataBr(validade.getDataValidade());
                                int dia = Util.verificaDiasVencimento(diasFaltando);
                                if (dia <= 30) {
                                    ValidadeModel validadeModel = new ValidadeModel();
                                    validadeModel.setAlterado(validade.getAlterado());
                                    validadeModel.setChave(validade.getChave());
                                    validadeModel.setCodigo(validade.getCodigo());
                                    validadeModel.setDataValidade(validade.getDataValidade());
                                    validadeModel.setDescricao(validade.getDescricao());
                                    validadeModel.setQtdEstoque(validade.getQtdEstoque());
                                    listaValidade.add(validadeModel);
                                }
                            }
                        }
                       // listaValidade = listaValidadeDTO;
                        adapterValidade = new AdapterValidade(listaValidade, getActivity());
                        adapterValidade.notifyDataSetChanged();

                        // Configurar RecyclerView
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter( adapterValidade );
                        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

                    }
                });
            }
        }.start();

        return view;
    }

    public void listarValidades(){
        SingletonValidade.getValidade().setListaValidade(null);
        Controller_Conexao cc = new Controller_Conexao();
        cc.listarValidades(getActivity());
    }
}