package com.zsisistemas.zsiempresa.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.zsisistemas.zsiempresa.db.ConexaoSqlite;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonProduto;

import java.util.ArrayList;
import java.util.List;

public class ProdutoDAO_Sqlite {

    private ConexaoSqlite conexao;
    private SQLiteDatabase banco;

    public ProdutoDAO_Sqlite(Context context) {
        conexao = ConexaoSqlite.getInstance(context);
        banco = conexao.getWritableDatabase();
    }

    // CadastroFragment ImportExport ProdutosFragment
    public long inserir(ProdutoModel produto) {
        ContentValues values = new ContentValues();

//         values.put("id_filial", produto.getId_filial());
        values.put("codigo_produto", produto.getCodigo_produto());
        values.put("descricao_produto", produto.getDescricao_produto());
        values.put("unidade", "un");
        values.put("preco_venda", produto.getPreco_venda());
        values.put("estoque", produto.getEstoque());
        values.put("qtd_inventario", 0);
//         values.put("loja", produto.getLoja());
        values.put("status", produto.getStatus());
        values.put("quantidadeProdutos", produto.getQuantidadeProdutos());
        values.put("preco_total", produto.getPreco_total());
        values.put("impressao", produto.getImpressao());

        return banco.insert("produtos", null, values);
    }

    // CadastroFragment ImportExport ProdutosFragment
    public long importToSqlite(ProdutoModel produto) {
        ContentValues values = new ContentValues();

        /*values.put("id_Produtos", produto.getId_Produtos());*/
        values.put("codigo_produto", produto.getCodigo_produto());
        values.put("descricao_produto", produto.getDescricao_produto());
        values.put("estoque", produto.getEstoque());

        return banco.insert("produtos", null, values);
    }

    // ProdutosCarrinhoFragment AberturaActivity ImportExport ProdutosFragment
    public List<ProdutoModel> obterTodos(){
        List<ProdutoModel> ListaProdutos = new ArrayList<>();
        @SuppressLint("Recycle") Cursor cursor = banco.query("produtos", new String[]{"id_Produtos" , "descricao_produto", "unidade", "preco_venda", "estoque",
                        "qtd_inventario", "codigo_produto", "status", "quantidadeProdutos", "preco_total", "impressao"},
                null, null, null, null, null, null);

        while (cursor.moveToNext()){
            ProdutoModel produto = new ProdutoModel();

            produto.setId_Produtos(cursor.getInt(0));
//            produto.setId_filial(cursor.getInt(1));
            produto.setDescricao_produto(cursor.getString(1));
            produto.setUnidade(cursor.getString(2));
            produto.setPreco_venda(cursor.getDouble(3));
            produto.setEstoque(cursor.getDouble(4));
            produto.setQtd_inventario(cursor.getDouble(5));
            produto.setCodigo_produto(cursor.getString(6));
//            produto.setLoja(cursor.getString(8));
            produto.setStatus(cursor.getInt(7));
            produto.setQuantidadeProdutos(cursor.getDouble(8));
            produto.setPreco_total(cursor.getDouble(9));
            produto.setImpressao(cursor.getInt(10));

            ListaProdutos.add(produto);
        }
        return ListaProdutos;
    }

    // Produtos coletados
    public void localizaProdutoStatus(){
        // TODO replicar esse metodo na ProdutoDAO_Mysql

        ArrayList<ProdutoModel> listaColetados = new ArrayList<>();

        Cursor cursor = banco.query(true,"produtos",  new String[]{"id_Produtos", "descricao_produto", "unidade", "preco_venda", "estoque",
                        "qtd_inventario", "codigo_produto", "status", "quantidadeProdutos", "preco_total", "impressao"},
                 "status = 1",null, null, null, null, null);
        while (cursor.moveToNext()){

            ProdutoModel produto = new ProdutoModel();
            produto.setId_Produtos(cursor.getInt(0));
            produto.setDescricao_produto(cursor.getString(1));
            produto.setUnidade(cursor.getString(2));
            produto.setPreco_venda(cursor.getDouble(3));
            produto.setEstoque(cursor.getDouble(4));
            produto.setQtd_inventario(cursor.getDouble(5));
            produto.setCodigo_produto(cursor.getString(6));
            produto.setStatus(cursor.getInt(7));
            produto.setQuantidadeProdutos(cursor.getDouble(8));
            produto.setPreco_total(cursor.getDouble(9));
            produto.setImpressao(cursor.getInt(10));

            listaColetados.add(produto);
        }
            SingletonProduto.getProduto().setListaColetados(listaColetados);
    }

    // Produtos NÃO coletados
    public void localizaProdutoColetar(){
        ArrayList<ProdutoModel> listaColetar = new ArrayList<>();

        Cursor cursor = banco.query(true,"produtos",  new String[]{"id_Produtos", "descricao_produto", "unidade", "preco_venda", "estoque",
                        "qtd_inventario", "codigo_produto", "status", "quantidadeProdutos", "preco_total", "impressao"},
                "qtd_inventario = 0",null, null, null, null, null);
        while (cursor.moveToNext()){

            ProdutoModel produto = new ProdutoModel();
            produto.setId_Produtos(cursor.getInt(0));
            produto.setDescricao_produto(cursor.getString(1));
            produto.setUnidade(cursor.getString(2));
            produto.setPreco_venda(cursor.getDouble(3));
            produto.setEstoque(cursor.getDouble(4));
            produto.setQtd_inventario(cursor.getDouble(5));
            produto.setCodigo_produto(cursor.getString(6));
            produto.setStatus(cursor.getInt(7));
            produto.setQuantidadeProdutos(cursor.getDouble(8));
            produto.setPreco_total(cursor.getDouble(9));
            produto.setImpressao(cursor.getInt(10));

            listaColetar.add(produto);
        }
        SingletonProduto.getProduto().setListaColetar(listaColetar);
    }

    // Produtos coletados
    public void localizaProdutoColetado(){
        ArrayList<ProdutoModel> listaColetados = new ArrayList<>();

        Cursor cursor = banco.query(true,"produtos",  new String[]{"id_Produtos", "descricao_produto", "unidade", "preco_venda", "estoque",
                        "qtd_inventario", "codigo_produto", "status", "quantidadeProdutos", "preco_total", "impressao"},
                "qtd_inventario > 0",null, null, null, null, null);
        while (cursor.moveToNext()){

            ProdutoModel produto = new ProdutoModel();
            produto.setId_Produtos(cursor.getInt(0));
            produto.setDescricao_produto(cursor.getString(1));
            produto.setUnidade(cursor.getString(2));
            produto.setPreco_venda(cursor.getDouble(3));
            produto.setEstoque(cursor.getDouble(4));
            produto.setQtd_inventario(cursor.getDouble(5));
            produto.setCodigo_produto(cursor.getString(6));
            produto.setStatus(cursor.getInt(7));
            produto.setQuantidadeProdutos(cursor.getDouble(8));
            produto.setPreco_total(cursor.getDouble(9));
            produto.setImpressao(cursor.getInt(10));

            listaColetados.add(produto);
        }
        SingletonProduto.getProduto().setListaColetados(listaColetados);
    }

    //Ajuste Coleta Consulta
    public void localizaProduto(String codigo_produto){
        @SuppressLint("Recycle") Cursor cursor = banco.query("produtos",  new String[]{"id_Produtos", "descricao_produto", "unidade", "preco_venda", "estoque",
                        "qtd_inventario", "codigo_produto", "status", "quantidadeProdutos", "preco_total", "impressao"},
                null, null, null, null, null, null);
        while (cursor.moveToNext()){
            if(cursor.getString(6).equals(codigo_produto)){
                ProdutoModel produto = new ProdutoModel();
                produto.setId_Produtos(cursor.getInt(0));
                produto.setDescricao_produto(cursor.getString(1));
                produto.setUnidade(cursor.getString(2));
                produto.setPreco_venda(cursor.getDouble(3));
                produto.setEstoque(cursor.getDouble(4));
                produto.setQtd_inventario(cursor.getDouble(5));
                produto.setCodigo_produto(cursor.getString(6));
                produto.setStatus(cursor.getInt(7));
                produto.setQuantidadeProdutos(cursor.getDouble(8));
                produto.setPreco_total(cursor.getDouble(9));
                produto.setImpressao(cursor.getInt(10));

                SingletonProduto.getProduto().setId_Produtos(produto.getId_Produtos());
                SingletonProduto.getProduto().setDescricao_produto(produto.getDescricao_produto());
                SingletonProduto.getProduto().setUnidade(produto.getUnidade());
                SingletonProduto.getProduto().setEstoque(produto.getEstoque());
                SingletonProduto.getProduto().setQtd_inventario(produto.getQtd_inventario());
                SingletonProduto.getProduto().setPreco_venda(produto.getPreco_venda());
                SingletonProduto.getProduto().setCodigo_produto(produto.getCodigo_produto());
                SingletonProduto.getProduto().setStatus(produto.getStatus());
                SingletonProduto.getProduto().setQuantidadeProdutos(produto.getQuantidadeProdutos());
                SingletonProduto.getProduto().setPreco_total(produto.getPreco_total());
                SingletonProduto.getProduto().setImpressao(produto.getImpressao());

            }
        }
    }

    public void excluir(ProdutoModel p){
        banco.delete("produtos", "id_Produtos = ?", new String[]{p.getId_Produtos().toString()});
    }

    public void excluirTodos(){
        banco.delete("produtos", "id_Produtos > ?", new String[]{"0"});
    }

    public void atualizarProdutoId(ProdutoModel produto){
        ContentValues values = new ContentValues();

//         values.put("id_filial", produto.getId_filial());
        values.put("codigo_produto", produto.getCodigo_produto());
        values.put("descricao_produto", produto.getDescricao_produto());
        values.put("unidade", "un");
        values.put("preco_venda", produto.getPreco_venda());
        values.put("estoque", produto.getEstoque());
        values.put("qtd_inventario", 0);
//         values.put("loja", produto.getLoja());
        values.put("status", produto.getStatus());
        values.put("quantidadeProdutos", produto.getQuantidadeProdutos());
        values.put("preco_total", produto.getPreco_total());
        values.put("impressao", produto.getImpressao());

        banco.update("produtos", values, "id_Produtos = ?", new String[]{produto.getId_Produtos().toString()});
    }

    public void atualizarEstoque(ProdutoModel produto){
        ContentValues values = new ContentValues();
        values.put("estoque", produto.getEstoque());
        banco.update("produtos", values, "id_Produtos = ?", new String[]{produto.getId_Produtos().toString()});
    }

    public void atualizarQtdInventario(ProdutoModel produto){
        ContentValues values = new ContentValues();
        values.put("preco_venda", produto.getPreco_venda());
        values.put("qtd_inventario", produto.getQtd_inventario());//0
        values.put("status", 1);
        banco.update("produtos", values, "id_Produtos = ?", new String[]{produto.getId_Produtos().toString()});
    }

    public void atualizarInventario(){
        ContentValues values = new ContentValues();
        values.put("qtd_inventario", 0);
        values.put("status", 0);
        banco.update("produtos", values, "id_Produtos > ?", new String[]{"0"});
    }
}
