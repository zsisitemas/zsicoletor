package com.zsisistemas.zsiempresa.dao;

import android.annotation.SuppressLint;

import com.zsisistemas.zsiempresa.db.Conexao;
import com.zsisistemas.zsiempresa.db.ConexaoCloud;
import com.zsisistemas.zsiempresa.model.ClienteModel;
import com.zsisistemas.zsiempresa.model.Cupom_itemModel;
import com.zsisistemas.zsiempresa.model.Empresa;
import com.zsisistemas.zsiempresa.model.ProdutoModel;
import com.zsisistemas.zsiempresa.model.ValidadeModel;
import com.zsisistemas.zsiempresa.model.singleton.SingletonCliente;
import com.zsisistemas.zsiempresa.model.singleton.SingletonConfiguracoes;

import java.sql.ResultSet;

public class ProdutoDAO_Mysql {

    private String tipoConexao = SingletonConfiguracoes.getConfig().getConnection();

    public ProdutoDAO_Mysql() {
        super();
    }

    public void inserir(ProdutoModel produtoModel) {
        String comando = "";
            comando = String.format("INSERT INTO produtos (descricao_produto, estoque, codigo_produto, unidade, preco_venda, qtd_inventario," +
                            " alterado,status, categoria, sub_categoria, validade)" +
                            " VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                    produtoModel.getDescricao_produto().toUpperCase(), produtoModel.getEstoque(), produtoModel.getCodigo_produto().toUpperCase(),
                    produtoModel.getUnidade().toUpperCase(), produtoModel.getPreco_venda(), produtoModel.getQtd_inventario(), 1, "ATIVO", "OUTROS", "OUTROS", "2030-01-01");

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"produtos");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"produtos");
        }
    }

    public void inserirValidade(ValidadeModel validadeModel) {
        String comando = "";
            comando = String.format("INSERT INTO validade (codigo, descricao, qtd, data, status, alterado, chave)" +
                            " VALUES ('%s','%s','%s','%s','%s','%s','%s');",
                    validadeModel.getCodigo().toUpperCase(), validadeModel.getDescricao(), validadeModel.getQtdEstoque(),
                    validadeModel.getDataValidade().toUpperCase(), 1, 1, validadeModel.getChave());

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"validade");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"validade");
        }
    }

//    Warning: #1364 Field 'id' doesn't have a default value
    public void inserirCliente(ClienteModel clienteModel) {
        String comando = "";
            comando = String.format("INSERT INTO usuario (nome, cpf, sexo, telefone, e_mail, perfil, status, rua, bairro, complemento, alterado)" +
                            " VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                    clienteModel.getNome().toUpperCase(), clienteModel.getCpf().toUpperCase(), clienteModel.getSexo().toUpperCase(), clienteModel.getTelefone().toUpperCase(),
                    clienteModel.getE_mail().toUpperCase(), "CLIENTE", "ATIVO", clienteModel.getRua().toUpperCase(), clienteModel.getBairro().toUpperCase(),
                    clienteModel.getComplemento().toUpperCase(), 1);

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"usuario");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"usuario");
        }
    }

    public void inserirCupom_item(Cupom_itemModel cupom_itemModel) {
        String comando = "";
        comando = String.format("INSERT INTO cupom_item (id_produto, cod_produto, data, quantidade, unidade, valor, total, comanda," +
                                                        " status, produto_descricao, caixa, item, finalizado, numeroMesa, nomeCliente," +
                                                        " atendente, statusComanda, tipoMesa, valorTipo, numero_cupom, obs, impressao," +
                                                        " fone, cod_pedido, alterado, subTotal, novoP, qtd_item) " +
                                "VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'," +
                                "'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                cupom_itemModel.getId_produto(), cupom_itemModel.getCod_produto().toUpperCase(), cupom_itemModel.getData().toUpperCase(), cupom_itemModel.getQuantidade(),
                cupom_itemModel.getUnidade().toUpperCase(), cupom_itemModel.getValor(), cupom_itemModel.getTotal(), cupom_itemModel.getComanda(), "",
                cupom_itemModel.getProduto_descricao().toUpperCase(), "01", cupom_itemModel.getItem(), 2, cupom_itemModel.getNumeroMesa(),
                cupom_itemModel.getNomeCliete().toUpperCase(), cupom_itemModel.getAtendente().toUpperCase(), "ABERTO", cupom_itemModel.getTipoMesa(), 0, cupom_itemModel.getNumero_cupom(),
                cupom_itemModel.getObs().toUpperCase(), cupom_itemModel.getImpresso(), cupom_itemModel.getFone(), cupom_itemModel.getCod_pedidoFPTO(), cupom_itemModel.getAlterado(),
                cupom_itemModel.getSubTotal(), 1, cupom_itemModel.getQtd_item());

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"produtos");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"produtos");
        }
    }

    @SuppressLint("DefaultLocale")
    public void atualizarCupomItem(Cupom_itemModel cupom_itemModel) {
        String comando = "";
        comando = String.format("UPDATE cupom_item SET cod_produto = '%s', data = '%s', quantidade = '%s', unidade = '%s', valor = '%s', total = '%s', comanda = '%s'," +
                        " status = '%s', produto_descricao = '%s', caixa = '%s', item = '%s', finalizado = '%s', numeroMesa = '%s', nomeCliente = '%s'," +
                        " atendente = '%s', statusComanda = '%s', tipoMesa = '%s', valorTipo = '%s', numero_cupom = '%s', obs = '%s', impressao = '%s'," +
                        " fone = '%s', cod_pedido = '%s', alterado = 2, subTotal = '%s', qtd_item = '%s' WHERE id_cupom = %d;",
                cupom_itemModel.getId_produto(), cupom_itemModel.getCod_produto().toUpperCase(), cupom_itemModel.getData().toUpperCase(), cupom_itemModel.getQuantidade(),
                cupom_itemModel.getUnidade().toUpperCase(), cupom_itemModel.getValor(), cupom_itemModel.getTotal(), cupom_itemModel.getComanda(), "",
                cupom_itemModel.getProduto_descricao().toUpperCase(), "01", cupom_itemModel.getItem(), 2, cupom_itemModel.getNumeroMesa(),
                cupom_itemModel.getNomeCliete().toUpperCase(), cupom_itemModel.getAtendente().toUpperCase(), "ABERTO", "1", 0, cupom_itemModel.getNumero_cupom(),
                cupom_itemModel.getObs().toUpperCase(), 1, cupom_itemModel.getFone().toUpperCase(), cupom_itemModel.getCod_pedidoFPTO().toUpperCase(), cupom_itemModel.getAlterado(),
                cupom_itemModel.getSubTotal(), cupom_itemModel.getQtd_item());

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"produtos");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"produtos");
        }

    }

    @SuppressLint("DefaultLocale")
    public void atualizarPrduto(ProdutoModel produtoModel) {
        String comando = "";
            comando = String.format("UPDATE produtos SET descricao_produto = '%s', estoque = '%s', qtd_inventario = '%s', alterado = 2 WHERE id_Produtos = %d;",
                    produtoModel.getDescricao_produto().toUpperCase(), produtoModel.getEstoque(), produtoModel.getQtd_inventario(), produtoModel.getId_Produtos());

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"produtos");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"produtos");
        }

    }

    @SuppressLint("DefaultLocale")
    public void atualizaColetaEtiqueta(ProdutoModel produtoModel) {
        String comando = "";

        comando = String.format("UPDATE produtos SET  alterado = 2, impEtiqueta = 1 WHERE id_Produtos = %d;",
                produtoModel.getId_Produtos());//empEtiqueta = '%s',

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"produtos");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"produtos");
        }
//UPDATE produtos SET alterado = '1', impEtiqueta = '1' WHERE (`id_Produtos` = '3080');
    }

    @SuppressLint("DefaultLocale")
    public void atualizarEstoque(ProdutoModel produtoModel) {
        String comando = "";
        comando = String.format("UPDATE produtos SET  estoque = '%s', alterado = 2 WHERE id_Produtos = %d;",
                 produtoModel.getEstoque(), produtoModel.getId_Produtos());

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"produtos");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"produtos");
        }

    }

    @SuppressLint("DefaultLocale")
    public void atualizarQtdInvantario(ProdutoModel produtoModel, String campo) {
        String comando = "";

        if(campo.equals("estoque")){
            comando = String.format("UPDATE produtos SET "+campo+" = '%s', alterado = 2, preco_venda = '%s' WHERE id_Produtos = %d;",
                    produtoModel.getEstoque(), produtoModel.getPreco_venda(), produtoModel.getId_Produtos());
        }else{
            comando = String.format("UPDATE produtos SET "+campo+" = '%s', alterado = 2, preco_venda = '%s' WHERE id_Produtos = %d;",
                    produtoModel.getQtd_inventario(), produtoModel.getPreco_venda(), produtoModel.getId_Produtos());
        }

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"produtos");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"produtos");
        }
    }

    public void cancelaPedido(int comanda) {
        String comando = "";
        comando = String.format("UPDATE cupom_item SET finalizado =3, status='CANC', alterado=2 WHERE comanda="+comanda+";");

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"produtos");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"produtos");
        }
    }

    public void selectProdutoPorCodigo(String codigo_produto) {

        ProdutoModel p = new ProdutoModel();

            p.setCodigo_produto(codigo_produto);
            String codigo = "SELECT * FROM produtos where codigo_produto = "+p.getCodigo_produto()+";";

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"produtos");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"produtos");
            }
        }

    public void listarProdutos(){

        try {
            String codigo = "SELECT * FROM produtos where status = 'ativo' order by descricao_produto asc";

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"produtos");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"produtos");
            }

        }catch (Exception ex){

        }
    }

    public void listarValidades(){

        try {
            String codigo = "SELECT * FROM validade where qtd > 0 and status = 1 ORDER by data asc";

           // String codigo = "SELECT * FROM validade where qtd > 0 and status = 1 and data between now() and DATE_ADD(now(), INTERVAL 30 DAY) ORDER by data asc";
            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"validade");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"validade");
            }

        }catch (Exception ex){

        }
    }

    public void listaFormaPagamento(String dataInicio, String dataFinal){

        try {
            String codigo = "SELECT distinct(forma_pagamento), sum(valor) as valor FROM cupom where status = 'A' " +
                    "and data between "+"'"+dataInicio+"'"+ " and " +"'"+dataFinal+"'"+ " group by forma_pagamento";

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"cupom");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"cupom");
            }

        }catch (Exception ex){

        }
    }

    public void listaPagamentoFun(String dataInicio, String dataFinal, String loginFun){

        try {
            String codigo = "SELECT distinct(forma_pagamento), sum(valor) as valor FROM cupom where status = 'A' " +
                    "and data between "+"'"+dataInicio+"'"+ " and " +"'"+dataFinal+"'"+ " and operador = "+"'"+loginFun+"'"+" group by forma_pagamento";

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"cupom");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"cupom");
            }

        }catch (Exception ex){

        }
    }

    public void listarPedidos(){

        try {
            String codigo = "SELECT * FROM cupom_item where finalizado=2 and status != 'CANC' group by comanda order by numeroMesa ASC";

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"cupom_item");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"cupom_item");
            }

        }catch (Exception ex){

        }
    }

    public void listarPedidosFun(String loginFun){
        String conexaoLogado = SingletonConfiguracoes.getConfig().getConnection();
        String perfil = SingletonCliente.getCliente().getPerfil();

        try {
            String codigo = "SELECT * FROM cupom_item where finalizado=2 and status != 'CANC' and atendente = "+"'"+loginFun+"'"+" group by comanda order by numeroMesa ASC";

            if (conexaoLogado.equals("2") && perfil.equalsIgnoreCase("funcionario")) {
                 codigo = "SELECT * FROM cupom_item where finalizado=2 and status != 'CANC' and tipoMesa = 0 group by comanda order by numeroMesa ASC";
            }

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"cupom_item");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"cupom_item");
            }

        }catch (Exception ex){

        }
    }

    public void listaItensPedido(Cupom_itemModel cupom_itemModel){
        try {
            String codigo = "SELECT * FROM cupom_item where comanda="+ cupom_itemModel.getComanda()+" and finalizado=2";

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"cupom_item");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"cupom_item");
            }

        }catch (Exception ex){

        }
    }

    public void verificarStatusPedido(int numeroMesa){

        try {
            String codigo = "SELECT * FROM cupom_item where numeroMesa = "+ numeroMesa+" and finalizado = 2";

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"cupom_item");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"cupom_item");
            }

        }catch (Exception ex){

        }
    }

    public void apagar(Integer id_Produtos){
        ProdutoModel p = new ProdutoModel();

        p.setId_Produtos(id_Produtos);
        @SuppressLint("DefaultLocale") String comando = "DELETE FROM produtos WHERE id_Produtos = " + p.getId_Produtos() + ";";

        if(tipoConexao.equalsIgnoreCase("0")){
            ConexaoCloud conexaoCloud = new ConexaoCloud();
            conexaoCloud.execute(comando,"produtos");
        }else{
            Conexao conexao = new Conexao();
            conexao.execute(comando,"produtos");
        }
    }

    public void listarChavesClientes(){

        try {
            String codigo = "SELECT * FROM chaves where status = 'ativo' order by id asc";

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"chaves");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"chaves");
            }

        }catch (Exception ex){

        }
    }

    public void listarClientes(String perfil){

        try {
            String codigo = "SELECT * FROM usuario where status = 'ativo' order by nome asc";

            if (!perfil.isEmpty() && perfil.equalsIgnoreCase("MESA")){
                //So cai aqui se tiver usuario logado e sua conexao for 2 e seu perfil for FUNCIONARIO.
                codigo = "SELECT * FROM usuario where status = 'ativo' AND perfil = 'mesa' order by nome asc";
            }

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"usuario");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"usuario");
            }

        }catch (Exception ex){
            ex.getMessage();
        }
    }

    public void atualizarCliente(ClienteModel clienteModel, int id){

        try {

            String codigo = String.format("UPDATE usuario SET nome = '%s', cpf = '%s', sexo = '%s', telefone = '%s', e_mail = '%s', rua = '%s', bairro = '%s', complemento = '%s' WHERE id ="+"'"+id+"'"+";",
                    clienteModel.getNome().toUpperCase(), clienteModel.getCpf().toUpperCase(), clienteModel.getSexo().toUpperCase(), clienteModel.getTelefone().toUpperCase(),
                    clienteModel.getE_mail().toUpperCase(), clienteModel.getRua().toUpperCase(), clienteModel.getBairro().toUpperCase(), clienteModel.getComplemento().toUpperCase());

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                conexaoCloud.execute(codigo,"usuario");
            }else{
                Conexao conexao = new Conexao();
                conexao.execute(codigo,"usuario");
            }

        }catch (Exception ex){

        }
    }

    public void atualizarValidade(ValidadeModel validadeModel, String chave){

        try {

            String codigo = String.format("UPDATE validade SET qtd = '%s', data = '%s', status = '%s', alterado = '%s' WHERE chave ="+"'"+chave+"'"+";",
                    validadeModel.getQtdEstoque(), validadeModel.getDataValidade().toUpperCase(), 1, 2);

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                conexaoCloud.execute(codigo,"validade");
            }else{
                Conexao conexao = new Conexao();
                conexao.execute(codigo,"validade");
            }

        }catch (Exception ex){

        }
    }

    public void inativarVencimento(String chave){

        try {

            String codigo = String.format("UPDATE validade SET status = '%s', alterado = '%s' WHERE chave ="+"'"+chave+"'"+";",
                    0, 0);

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                conexaoCloud.execute(codigo,"validade");
            }else{
                Conexao conexao = new Conexao();
                conexao.execute(codigo,"validade");
            }

        }catch (Exception ex){

        }
    }

    public void listarClientesEmpresa(){
        try {
            String codigo = "SELECT * FROM empresa WHERE status = 'ativo' ORDER BY data_vencimento DESC";

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                ResultSet resultSet = conexaoCloud.select(codigo,"empresa");
            }else{
                Conexao conexao = new Conexao();
                ResultSet resultSet = conexao.select(codigo,"empresa");
            }

        }catch (Exception ex){

        }
    }

    public void pagar(Empresa empresa){
        try {
            @SuppressLint("DefaultLocale") String codigo = String.format("UPDATE empresa SET data_aviso = '%s', data_vencimento = '%s', status_financa = '%s' WHERE id = %d;",
                    empresa.getData_aviso(), empresa.getData_vencimento(), empresa.getStatus_financa(), empresa.getId());

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                conexaoCloud.execute(codigo,"empresa");
            }else{
                Conexao conexao = new Conexao();
                conexao.execute(codigo,"empresa");
            }
        }catch (Exception ex){
        }
    }

    public void alterarVencimento(Empresa empresa){
        try {
            @SuppressLint("DefaultLocale") String codigo = String.format("UPDATE empresa SET data_vencimento = '%s' WHERE id = %d;",
                    empresa.getData_vencimento(), empresa.getId());

            if(tipoConexao.equalsIgnoreCase("0")){
                ConexaoCloud conexaoCloud = new ConexaoCloud();
                conexaoCloud.execute(codigo,"empresa");
            }else{
                Conexao conexao = new Conexao();
                conexao.execute(codigo,"empresa");
            }
        }catch (Exception ex){
        }
    }
}
