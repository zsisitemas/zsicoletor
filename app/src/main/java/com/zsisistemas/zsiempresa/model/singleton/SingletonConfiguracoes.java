package com.zsisistemas.zsiempresa.model.singleton;

import com.zsisistemas.zsiempresa.model.ConfiguracoesModel;

/**
* Permite o acesso global da instacia
*/

public class SingletonConfiguracoes {

    public static ConfiguracoesModel instancia;

    private SingletonConfiguracoes() {
    }

    public static ConfiguracoesModel getConfig() {

        if (instancia == null) {
            instancia = new ConfiguracoesModel();
        }
        return instancia;
    }
}
