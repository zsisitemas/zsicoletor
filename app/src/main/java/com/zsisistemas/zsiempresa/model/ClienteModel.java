package com.zsisistemas.zsiempresa.model;

import com.zsisistemas.zsiempresa.db._Default;

import java.util.ArrayList;

public class ClienteModel extends _Default {

    private int id;
    private String nome;
    private String cpf;
    private String data_nasc;
    private String sexo;
    private String observacao;
    private String rua;
    private int numero;
    private String complemento;
    private String bairro;
    private String cidade;
    private String uf;
    private int cep;
    private String telefone;
    private String e_mail;
    private String status;
    private String perfil;
    private String nomeClienteSelecionado;
    private int idClienteSelecionado;
    private String telefoneClienteSelecionado;
    private String chaveId;

    private ArrayList<ClienteModel> listaclientes;
    private ArrayList<ClienteModel> listaFuncionario;

    private String loginFuncionario = null;
    private String senhaFuncionario;

    public ClienteModel(){
        super();
        this.id = -1;
        this.nome = "";
        this.status = "";
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ClienteModel> getListaclientes() {
        return listaclientes;
    }

    public void setListaclientes(ArrayList<ClienteModel> listaclientes) {
        this.listaclientes = listaclientes;
    }

    public String getNomeClienteSelecionado() {
        return nomeClienteSelecionado;
    }

    public void setNomeClienteSelecionado(String nomeClienteSelecionado) {
        this.nomeClienteSelecionado = nomeClienteSelecionado;
    }

    public int getIdClienteSelecionado() {
        return idClienteSelecionado;
    }

    public void setIdClienteSelecionado(int idClienteSelecionado) {
        this.idClienteSelecionado = idClienteSelecionado;
    }

    public String getTelefoneClienteSelecionado() {
        return telefoneClienteSelecionado;
    }

    public void setTelefoneClienteSelecionado(String telefoneClienteSelecionado) {
        this.telefoneClienteSelecionado = telefoneClienteSelecionado;
    }

    public String getData_nasc() {
        return data_nasc;
    }

    public void setData_nasc(String data_nasc) {
        this.data_nasc = data_nasc;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public int getCep() {
        return cep;
    }

    public void setCep(int cep) {
        this.cep = cep;
    }

    public String getLoginFuncionario() {
        return loginFuncionario;
    }

    public void setLoginFuncionario(String loginFuncionario) {
        this.loginFuncionario = loginFuncionario;
    }

    public String getSenhaFuncionario() {
        return senhaFuncionario;
    }

    public void setSenhaFuncionario(String senhaFuncionario) {
        this.senhaFuncionario = senhaFuncionario;
    }

    public ArrayList<ClienteModel> getListaFuncionario() {
        return listaFuncionario;
    }

    public void setListaFuncionario(ArrayList<ClienteModel> listaFuncionario) {
        this.listaFuncionario = listaFuncionario;
    }

    public String getChaveId() {
        return chaveId;
    }

    public void setChaveId(String chaveId) {
        this.chaveId = chaveId;
    }
}
