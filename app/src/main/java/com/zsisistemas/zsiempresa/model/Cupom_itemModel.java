package com.zsisistemas.zsiempresa.model;

import java.util.ArrayList;

public class Cupom_itemModel {

    private int id_cupom;
    private int id_produto;
    private int item;
    private int finalizado;
    private int numero_cupom;
    private int comanda;
    private int numeroMesa;
    private int impresso;
    private double quantidade;
    private double valor;
    private double total;
    private double subTotal;
    private double totalPagar;
    private double valorTipo;
    private double lucroUNI;
    private double lucroTTAL;
    private double valorPago;
    private String nomeCliete;
    private String atendente;
    private String unidade;
    private String caixa;
    private String cod_produto;
    private String data;
    private String produto_descricao;
    private String status;
    private String statusComanda;
    private String tipoMesa;
    private String fone;
    private String obs = "";
    private String cod_pedidoFPTO;
    private ArrayList<Cupom_itemModel> listaPedido;
    private int qtd_item;

    private int alterado;

    private boolean statusPedido;

    public int getId_cupom() {
        return id_cupom;
    }

    public void setId_cupom(int id_cupom) {
        this.id_cupom = id_cupom;
    }

    public int getId_produto() {
        return id_produto;
    }

    public void setId_produto(int id_produto) {
        this.id_produto = id_produto;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public int getFinalizado() {
        return finalizado;
    }

    public void setFinalizado(int finalizado) {
        this.finalizado = finalizado;
    }

    public int getNumero_cupom() {
        return numero_cupom;
    }

    public void setNumero_cupom(int numero_cupom) {
        this.numero_cupom = numero_cupom;
    }

    public int getComanda() {
        return comanda;
    }

    public void setComanda(int comanda) {
        this.comanda = comanda;
    }

    public int getNumeroMesa() {
        return numeroMesa;
    }

    public void setNumeroMesa(int numeroMesa) {
        this.numeroMesa = numeroMesa;
    }

    public int getImpresso() {
        return impresso;
    }

    public void setImpresso(int impresso) {
        this.impresso = impresso;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getTotal() {
        return total;
    }

    public double setTotal(double total) {
        this.total = total;
        return total;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(double totalPagar) {
        this.totalPagar = totalPagar;
    }

    public double getValorTipo() {
        return valorTipo;
    }

    public void setValorTipo(double valorTipo) {
        this.valorTipo = valorTipo;
    }

    public double getLucroUNI() {
        return lucroUNI;
    }

    public void setLucroUNI(double lucroUNI) {
        this.lucroUNI = lucroUNI;
    }

    public double getLucroTTAL() {
        return lucroTTAL;
    }

    public void setLucroTTAL(double lucroTTAL) {
        this.lucroTTAL = lucroTTAL;
    }

    public double getValorPago() {
        return valorPago;
    }

    public void setValorPago(double valorPago) {
        this.valorPago = valorPago;
    }

    public String getNomeCliete() {
        return nomeCliete;
    }

    public void setNomeCliete(String nomeCliete) {
        this.nomeCliete = nomeCliete;
    }

    public String getAtendente() {
        return atendente;
    }

    public void setAtendente(String atendente) {
        this.atendente = atendente;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getCaixa() {
        return caixa;
    }

    public void setCaixa(String caixa) {
        this.caixa = caixa;
    }

    public String getCod_produto() {
        return cod_produto;
    }

    public void setCod_produto(String cod_produto) {
        this.cod_produto = cod_produto;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getProduto_descricao() {
        return produto_descricao;
    }

    public void setProduto_descricao(String produto_descricao) {
        this.produto_descricao = produto_descricao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusComanda() {
        return statusComanda;
    }

    public void setStatusComanda(String statusComanda) {
        this.statusComanda = statusComanda;
    }

    public String getTipoMesa() {
        return tipoMesa;
    }

    public void setTipoMesa(String tipoMesa) {
        this.tipoMesa = tipoMesa;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getCod_pedidoFPTO() {
        return cod_pedidoFPTO;
    }

    public void setCod_pedidoFPTO(String cod_pedidoFPTO) {
        this.cod_pedidoFPTO = cod_pedidoFPTO;
    }

    public int getAlterado() {
        return alterado;
    }

    public void setAlterado(int alterado) {
        this.alterado = alterado;
    }

    public ArrayList<Cupom_itemModel> getListaPedido() {
        return listaPedido;
    }

    public void setListaPedido(ArrayList<Cupom_itemModel> listaPedido) {
        this.listaPedido = listaPedido;
    }

    public boolean isStatusPedido() {
        return statusPedido;
    }

    public void setStatusPedido(boolean statusPedido) {
        this.statusPedido = statusPedido;
    }

    public int getQtd_item() {
        return qtd_item;
    }

    public void setQtd_item(int qtd_item) {
        this.qtd_item = qtd_item;
    }
}
