package com.zsisistemas.zsiempresa.model;

import java.util.ArrayList;

public class Chave {
    private int id;
    private String login;//login
    private String senha;//, senha
    private String chave;
    private String perfil;//, perfil,
    private String status;
    private String conexao;
    private String banco;
    private String nome;
    private ArrayList<Chave> listaClientesChaves;

    public ArrayList<Chave> getListaClientesChaves() {
        return listaClientesChaves;
    }

    public void setListaClientesChaves(ArrayList<Chave> listaClientesChaves) {
        this.listaClientesChaves = listaClientesChaves;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getConexao() {
        return conexao;
    }

    public void setConexao(String conexao) {
        this.conexao = conexao;
    }
}
