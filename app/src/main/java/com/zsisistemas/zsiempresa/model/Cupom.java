package com.zsisistemas.zsiempresa.model;

import java.util.ArrayList;

public class Cupom {


    private int id_cupom;
    private int numero_cupom;
    private int id_abertura;
    private int qtd_cupom;

    private double valor;
    private double subtotal;
    private double abertura;
    private double desconto;
    private double sumSubtotal;

    private String data;
    private String dataF;
    private String hora;
    private String forma_pagamento;
    private String fechamento;
    private String status;
    private String operador;

    //*********************dados do property
    private int imprimeCupom;
    private int opcaoTela;
    private int taxa10;
    private int carga;
    private int guilhotinha;

    private String caixa;
    private String portaImpressora;
    private String portaImpressora2;
    private String mensagem;
    private String pedido;
    private String radioPedido;
    private String controle;
    private String resumedescricao;
    private String localproduoto;
    private String impressora;
    private String imprimeZero;
    private double taxa;
    private int alterado;
    private String valorRecebido;
    private String troco;
    private ArrayList<Cupom> listaCupom;

    public ArrayList<Cupom> getListaCupom() {
        return listaCupom;
    }

    public void setListaCupom(ArrayList<Cupom> listaCupom) {
        this.listaCupom = listaCupom;
    }

    public int getId_cupom() {
        return id_cupom;
    }

    public void setId_cupom(int id_cupom) {
        this.id_cupom = id_cupom;
    }

    public int getNumero_cupom() {
        return numero_cupom;
    }

    public void setNumero_cupom(int numero_cupom) {
        this.numero_cupom = numero_cupom;
    }

    public int getId_abertura() {
        return id_abertura;
    }

    public void setId_abertura(int id_abertura) {
        this.id_abertura = id_abertura;
    }

    public int getQtd_cupom() {
        return qtd_cupom;
    }

    public void setQtd_cupom(int qtd_cupom) {
        this.qtd_cupom = qtd_cupom;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getAbertura() {
        return abertura;
    }

    public void setAbertura(double abertura) {
        this.abertura = abertura;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public double getSumSubtotal() {
        return sumSubtotal;
    }

    public void setSumSubtotal(double sumSubtotal) {
        this.sumSubtotal = sumSubtotal;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDataF() {
        return dataF;
    }

    public void setDataF(String dataF) {
        this.dataF = dataF;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getForma_pagamento() {
        return forma_pagamento;
    }

    public void setForma_pagamento(String forma_pagamento) {
        this.forma_pagamento = forma_pagamento;
    }

    public String getFechamento() {
        return fechamento;
    }

    public void setFechamento(String fechamento) {
        this.fechamento = fechamento;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public int getImprimeCupom() {
        return imprimeCupom;
    }

    public void setImprimeCupom(int imprimeCupom) {
        this.imprimeCupom = imprimeCupom;
    }

    public int getOpcaoTela() {
        return opcaoTela;
    }

    public void setOpcaoTela(int opcaoTela) {
        this.opcaoTela = opcaoTela;
    }

    public int getTaxa10() {
        return taxa10;
    }

    public void setTaxa10(int taxa10) {
        this.taxa10 = taxa10;
    }

    public int getCarga() {
        return carga;
    }

    public void setCarga(int carga) {
        this.carga = carga;
    }

    public int getGuilhotinha() {
        return guilhotinha;
    }

    public void setGuilhotinha(int guilhotinha) {
        this.guilhotinha = guilhotinha;
    }

    public String getCaixa() {
        return caixa;
    }

    public void setCaixa(String caixa) {
        this.caixa = caixa;
    }

    public String getPortaImpressora() {
        return portaImpressora;
    }

    public void setPortaImpressora(String portaImpressora) {
        this.portaImpressora = portaImpressora;
    }

    public String getPortaImpressora2() {
        return portaImpressora2;
    }

    public void setPortaImpressora2(String portaImpressora2) {
        this.portaImpressora2 = portaImpressora2;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getPedido() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }

    public String getRadioPedido() {
        return radioPedido;
    }

    public void setRadioPedido(String radioPedido) {
        this.radioPedido = radioPedido;
    }

    public String getControle() {
        return controle;
    }

    public void setControle(String controle) {
        this.controle = controle;
    }

    public String getResumedescricao() {
        return resumedescricao;
    }

    public void setResumedescricao(String resumedescricao) {
        this.resumedescricao = resumedescricao;
    }

    public String getLocalproduoto() {
        return localproduoto;
    }

    public void setLocalproduoto(String localproduoto) {
        this.localproduoto = localproduoto;
    }

    public String getImpressora() {
        return impressora;
    }

    public void setImpressora(String impressora) {
        this.impressora = impressora;
    }

    public String getImprimeZero() {
        return imprimeZero;
    }

    public void setImprimeZero(String imprimeZero) {
        this.imprimeZero = imprimeZero;
    }

    public double getTaxa() {
        return taxa;
    }

    public void setTaxa(double taxa) {
        this.taxa = taxa;
    }

    public int getAlterado() {
        return alterado;
    }

    public void setAlterado(int alterado) {
        this.alterado = alterado;
    }

    public String getValorRecebido() {
        return valorRecebido;
    }

    public void setValorRecebido(String valorRecebido) {
        this.valorRecebido = valorRecebido;
    }

    public String getTroco() {
        return troco;
    }

    public void setTroco(String troco) {
        this.troco = troco;
    }
}
