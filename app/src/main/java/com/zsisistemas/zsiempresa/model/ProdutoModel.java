package com.zsisistemas.zsiempresa.model;

import com.zsisistemas.zsiempresa.db._Default;

import java.util.ArrayList;

public class ProdutoModel extends _Default {

    private Integer id_Produtos;
    private Integer id_filial;
    private String descricao_produto;
    private String unidade;
    private double preco_venda;
    private double preco_total;
    private double estoque;
    private double qtd_inventario;
    private String codigo_produto;
    private String loja;
    private int status;
    private String clienteVirtual;
    private String obs;
    private boolean checkPedido = false;    // Trata se pedido é novo(false) ou atualizável(true).
    private ArrayList<ProdutoModel> listaProdutos;
    private ArrayList<ProdutoModel> listaColetar;
    private ArrayList<ProdutoModel> listaColetados;
    private ArrayList<ProdutoModel> listaCarrinho;
    private double quantidadeProdutos;
    private ProdutoModel produtoInstancia;
    private Integer impressao; // Identificar se o produto do carrinho veio do cupom_item(1) ou se foi adicionado novo(0)

    public ProdutoModel(){
        super();
        this.id_Produtos = -1;
        this.estoque = 0;
        this.descricao_produto = "";
        this.codigo_produto = "";
    }

    // Construtor para a classe Import/Export
    public ProdutoModel(/*Integer id_Produtos,*/ String codigo_produto, String descricao_produto, double estoque) {
        /*this.id_Produtos = id_Produtos;*/
        this.codigo_produto = codigo_produto;
        this.descricao_produto = descricao_produto;
        this.estoque = estoque;
    }

    public ProdutoModel(String unidade, double preco_venda, double estoque, double qtd_inventario, String codigo_produto) {
        this.unidade = unidade;
        this.preco_venda = preco_venda;
        this.estoque = estoque;
        this.qtd_inventario = qtd_inventario;
        this.codigo_produto = codigo_produto;
    }

    public String getClienteVirtual() {
        return clienteVirtual;
    }

    public void setClienteVirtual(String clienteVirtual) {
        this.clienteVirtual = clienteVirtual;
    }

    public Integer getId_Produtos() {
        return id_Produtos;
    }

    public void setId_Produtos(Integer id_Produtos) {
        this.id_Produtos = id_Produtos;
    }

    public Integer getId_filial() {
        return id_filial;
    }

    public void setId_filial(Integer id_filial) {
        this.id_filial = id_filial;
    }

    public String getDescricao_produto() {
        return descricao_produto;
    }

    public void setDescricao_produto(String descricao_produto) {
        this.descricao_produto = descricao_produto;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public double getPreco_venda() {
        return preco_venda;
    }

    public void setPreco_venda(double preco_venda) {
        this.preco_venda = preco_venda;
    }

    public double getEstoque() {
        return estoque;
    }

    public double setEstoque(double estoque) {
        this.estoque = estoque;
        return estoque;
    }

    public double getQtd_inventario() {
        return qtd_inventario;
    }

    public double setQtd_inventario(double qtd_inventario) {
        this.qtd_inventario = qtd_inventario;
        return qtd_inventario;
    }

    public String getCodigo_produto() {
        return codigo_produto;
    }

    public void setCodigo_produto(String codigo_produto) {
        this.codigo_produto = codigo_produto;
    }

    public String getLoja() {
        return loja;
    }

    public void setLoja(String loja) {
        this.loja = loja;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isCheckPedido() {
        return checkPedido;
    }

    public void setCheckPedido(boolean checkPedido) {
        this.checkPedido = checkPedido;
    }

    public ArrayList<ProdutoModel> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(ArrayList<ProdutoModel> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }

    public ArrayList<ProdutoModel> getListaColetar() {
        return listaColetar;
    }

    public void setListaColetar(ArrayList<ProdutoModel> listaColetar) {
        this.listaColetar = listaColetar;
    }

    public ArrayList<ProdutoModel> getListaColetados() {
        return listaColetados;
    }

    public void setListaColetados(ArrayList<ProdutoModel> listaColetados) {
        this.listaColetados = listaColetados;
    }

    public ArrayList<ProdutoModel> getListaCarrinho() {
        return listaCarrinho;
    }

    public void setListaCarrinho(ArrayList<ProdutoModel> listaCarrinho) {
        this.listaCarrinho = listaCarrinho;
    }

    public double getQuantidadeProdutos() {
        return quantidadeProdutos;
    }

    public void setQuantidadeProdutos(double quantidadeProdutos) {
        this.quantidadeProdutos = quantidadeProdutos;
    }

    public ProdutoModel getProdutoInstancia() {
        return produtoInstancia;
    }

    public void setProdutoInstancia(ProdutoModel produtoInstancia) {
        this.produtoInstancia = produtoInstancia;
    }

    public double getPreco_total() {
        return preco_total;
    }

    public void setPreco_total(double preco_total) {
        this.preco_total = preco_total;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Integer getImpressao() {
        return impressao;
    }

    public void setImpressao(Integer impressao) {
        this.impressao = impressao;
    }
}
