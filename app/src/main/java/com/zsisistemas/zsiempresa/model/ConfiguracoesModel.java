package com.zsisistemas.zsiempresa.model;

import java.util.ArrayList;

public class ConfiguracoesModel {

    private String ip;
    private String connection = "3";
    private String NomeBanco;
    private String chave;
    private String validade;
    private String root;
    private ArrayList<ConfiguracoesModel> listaDadosBanco;
    private String listaLembrarLogin;
    private String dataInicio;
    private String dataFim;

    private String dataInicioAux;
    private String dataFimAux;

    private boolean verificarConfigs = false;

    public ArrayList<ConfiguracoesModel> getListaDadosBanco() {
        return listaDadosBanco;
    }

    public void setListaDadosBanco(ArrayList<ConfiguracoesModel> listaDadosBanco) {
        this.listaDadosBanco = listaDadosBanco;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public String getDataInicioAux() {
        return dataInicioAux;
    }

    public void setDataInicioAux(String dataInicioAux) {
        this.dataInicioAux = dataInicioAux;
    }

    public String getDataFimAux() {
        return dataFimAux;
    }

    public void setDataFimAux(String dataFimAux) {
        this.dataFimAux = dataFimAux;
    }

    public String getValidade() { return validade;
    }

    public void setValidade(String validade) { this.validade = validade;
    }

    public String getChave() { return chave;
    }

    public void setChave(String chave) { this.chave = chave;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getNomeBanco() {
        return NomeBanco;
    }

    public void setNomeBanco(String nomeBanco) {
        NomeBanco = nomeBanco;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getListaLembrarLogin() {
        return listaLembrarLogin;
    }

    public void setListaLembrarLogin(String listaLembrarLogin) {
        this.listaLembrarLogin = listaLembrarLogin;
    }

    public boolean isVerificarConfigs() {
        return verificarConfigs;
    }

    public void setVerificarConfigs(boolean verificarConfigs) {
        this.verificarConfigs = verificarConfigs;
    }
}
