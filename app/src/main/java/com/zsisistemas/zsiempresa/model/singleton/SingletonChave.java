package com.zsisistemas.zsiempresa.model.singleton;

import com.zsisistemas.zsiempresa.model.Chave;

/**
* Permite o acesso global da instacia
*/

public class SingletonChave {

    public static Chave instancia;

    private SingletonChave() {
    }

    public static Chave getChave() {

        if (instancia == null) {
            instancia = new Chave();
        }
        return instancia;
    }
}
