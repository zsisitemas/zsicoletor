package com.zsisistemas.zsiempresa.model.singleton;

import com.zsisistemas.zsiempresa.model.Cupom_itemModel;

/**
 * Permite o acesso global da instacia
 */

public class SingletonCupom_item {

    public static Cupom_itemModel instancia;

    private SingletonCupom_item() {
    }

    public static Cupom_itemModel getCupom_Item() {

        if (instancia == null) {
            instancia = new Cupom_itemModel();
        }
        return instancia;
    }
}
