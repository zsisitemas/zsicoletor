package com.zsisistemas.zsiempresa.model;

import java.util.ArrayList;

public class Empresa {
    private int id; //id             int(30)
    private int qtd_login;//qtd_login      int(15)

    private String razao;//razao          varchar(250)
    private String fantasia;//fantasia       varchar(250)
    private String cnpj;//cnpj           varchar(50)
    private String inscricao;//inscricao      varchar(30)
    private String rua;//rua            varchar(250)
    private String numero;//numero         varchar(30)
    private String cep;//cep            varchar(20)
    private String complemento;//complemento    varchar(100)
    private String bairro;//bairro         varchar(200)
    private String cidade;//cidade         varchar(100)
    private String estado;//estado         varchar(100)
    private String fone1;//fone1          varchar(20)
    private String fone2;//fone2          varchar(20)
    private String e_mail;//e_mail         varchar(250)
    private String tipo_pacote; //tipo_pacote;    varchar(50)
    private String data_aviso;//data_aviso     date
    private String data_vencimento;//data_vencimento     date
    private String login;//login          varchar(100)
    private String tipoConexao;  //tipoConexao    varchar(5)
    private String url;//url            varchar(30)
    private String banco;//banco          varchar(100)
    private String status;//status         varchar(30)
    private String atividade;//atividade      varchar(100)
    private int bloqueio_auto;// bloqueio_auto     int(11)
    private int tipoLicenca;//tipoLicenca       int(11)
    private String ultimoAcesso;//ultimoAcesso      date

    private int alt;             //int(10)
    private String data_implementacao; //     date
    private String rede_social;       //varchar(200)
    private double valor_mensal;      //double
    private String rg;                //varchar(50)
    private String cpf;               //varchar(50)
    private String ponto_ref;         //varchar(250)
    private String projeto;          /// varchar(100)
    private String obs;              // varchar(500)
    private String versao;            //varchar(30)
    private int status_financa;   // int(5)
    private String ip;               // varchar(50)
    private String dataNasc;         // date
    private String msgCliente;
    private String licenca;
    private String chave;
    private String off;
    private int mei;
    private int qtdCaixa;
    private String app;
    private ArrayList<Empresa> listaEmpresas;

    public ArrayList<Empresa> getListaEmpresas() {
        return listaEmpresas;
    }

    public void setListaEmpresas(ArrayList<Empresa> listaEmpresas) {
        this.listaEmpresas = listaEmpresas;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }



    public int getQtdCaixa() {
        return qtdCaixa;
    }

    public void setQtdCaixa(int qtdCaixa) {
        this.qtdCaixa = qtdCaixa;
    }


    public int getMei() {
        return mei;
    }

    public void setMei(int mei) {
        this.mei = mei;
    }

    public String getOff() {
        return off;
    }

    public void setOff(String off) {
        this.off = off;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public String getLicenca() {
        return licenca;
    }

    public void setLicenca(String licenca) {
        this.licenca = licenca;
    }

    public String getMsgCliente() {
        return msgCliente;
    }

    public void setMsgCliente(String msgCliente) {
        this.msgCliente = msgCliente;
    }

    public int getAlt() {
        return alt;
    }

    public void setAlt(int alt) {
        this.alt = alt;
    }

    public String getData_implementacao() {
        return data_implementacao;
    }

    public void setData_implementacao(String data_implementacao) {
        this.data_implementacao = data_implementacao;
    }

    public String getRede_social() {
        return rede_social;
    }

    public void setRede_social(String rede_social) {
        this.rede_social = rede_social;
    }

    public double getValor_mensal() {
        return valor_mensal;
    }

    public void setValor_mensal(double valor_mensal) {
        this.valor_mensal = valor_mensal;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getPonto_ref() {
        return ponto_ref;
    }

    public void setPonto_ref(String ponto_ref) {
        this.ponto_ref = ponto_ref;
    }

    public String getProjeto() {
        return projeto;
    }

    public void setProjeto(String projeto) {
        this.projeto = projeto;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getVersao() {
        return versao;
    }

    public void setVersao(String versao) {
        this.versao = versao;
    }

    public int getStatus_financa() {
        return status_financa;
    }

    public void setStatus_financa(int status_financa) {
        this.status_financa = status_financa;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(String dataNasc) {
        this.dataNasc = dataNasc;
    }

    public String getUltimoAcesso() {
        return ultimoAcesso;
    }

    public void setUltimoAcesso(String ultimoAcesso) {
        this.ultimoAcesso = ultimoAcesso;
    }

    public int getTipoLicenca() {
        return tipoLicenca;
    }

    public void setTipoLicenca(int tipoLicenca) {
        this.tipoLicenca = tipoLicenca;
    }

    public int getBloqueio_auto() {
        return bloqueio_auto;
    }

    public void setBloqueio_auto(int bloqueio_auto) {
        this.bloqueio_auto = bloqueio_auto;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAtividade() {
        return atividade;
    }

    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }

    public String getTipoConexao() {
        return tipoConexao;
    }

    public void setTipoConexao(String tipoConexao) {
        this.tipoConexao = tipoConexao;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRazao() {
        return razao;
    }

    public void setRazao(String razao) {
        this.razao = razao;
    }

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInscricao() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao = inscricao;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFone1() {
        return fone1;
    }

    public void setFone1(String fone1) {
        this.fone1 = fone1;
    }

    public String getFone2() {
        return fone2;
    }

    public void setFone2(String fone2) {
        this.fone2 = fone2;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public String getTipo_pacote() {
        return tipo_pacote;
    }

    public void setTipo_pacote(String tipo_pacote) {
        this.tipo_pacote = tipo_pacote;
    }

    public int getQtd_login() {
        return qtd_login;
    }

    public void setQtd_login(int qtd_login) {
        this.qtd_login = qtd_login;
    }

    public String getData_aviso() {
        return data_aviso;
    }

    public void setData_aviso(String data_aviso) {
        this.data_aviso = data_aviso;
    }

    public String getData_vencimento() {
        return data_vencimento;
    }

    public void setData_vencimento(String data_vencimento) {
        this.data_vencimento = data_vencimento;
    }

}
