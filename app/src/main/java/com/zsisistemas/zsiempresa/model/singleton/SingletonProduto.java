package com.zsisistemas.zsiempresa.model.singleton;

import com.zsisistemas.zsiempresa.model.ProdutoModel;

/**
* Permite o acesso global da instacia
*/

public class SingletonProduto {

    public static ProdutoModel instancia;

    private SingletonProduto() {
    }

    public static ProdutoModel getProduto() {

        if (instancia == null) {
            instancia = new ProdutoModel();
        }
        return instancia;
    }
}
