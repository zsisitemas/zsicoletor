package com.zsisistemas.zsiempresa.model.singleton;

import com.zsisistemas.zsiempresa.model.ClienteModel;

/**
* Permite o acesso global da instacia
*/

public class SingletonCliente {

    public static ClienteModel instancia;

    private SingletonCliente() {
    }

    public static ClienteModel getCliente() {

        if (instancia == null) {
            instancia = new ClienteModel();
        }
        return instancia;
    }
}
