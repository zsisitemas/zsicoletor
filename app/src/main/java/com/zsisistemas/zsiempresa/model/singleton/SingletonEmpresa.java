package com.zsisistemas.zsiempresa.model.singleton;

import com.zsisistemas.zsiempresa.model.Empresa;


/**
* Permite o acesso global da instacia
*/

public class SingletonEmpresa {

    public static Empresa instancia;

    private SingletonEmpresa() {
    }

    public static Empresa getEmpresa() {

        if (instancia == null) {
            instancia = new Empresa();
        }
        return instancia;
    }
}
