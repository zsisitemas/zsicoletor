package com.zsisistemas.zsiempresa.model.singleton;

import com.zsisistemas.zsiempresa.model.ValidadeModel;

/**
* Permite o acesso global da instacia
*/

public class SingletonValidade {

    public static ValidadeModel instancia;

    private SingletonValidade() {
    }

    public static ValidadeModel getValidade() {

        if (instancia == null) {
            instancia = new ValidadeModel();
        }
        return instancia;
    }
}
