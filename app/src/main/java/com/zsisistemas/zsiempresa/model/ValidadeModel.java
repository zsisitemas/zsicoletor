package com.zsisistemas.zsiempresa.model;

import com.zsisistemas.zsiempresa.db._Default;

import java.util.ArrayList;

public class ValidadeModel extends _Default {

    private int idValidade;
    private String codigo;
    private String descricao;
    private double qtdEstoque;
    private String dataValidade;
    private int status;
    private int alterado;
    private String chave;
    private String dias;
    private ArrayList<ValidadeModel> listaValidade;

    public int getIdValidade() {
        return idValidade;
    }

    public void setIdValidade(int idValidade) {
        this.idValidade = idValidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getQtdEstoque() {
        return qtdEstoque;
    }

    public void setQtdEstoque(double qtdEstoque) {
        this.qtdEstoque = qtdEstoque;
    }

    public String getDataValidade() {
        return dataValidade;
    }

    public void setDataValidade(String dataValidade) {
        this.dataValidade = dataValidade;
    }

    public String getDias() {
        return dias;
    }

    public void setDias(String dias) {
        this.dias = dias;
    }

    public ArrayList<ValidadeModel> getListaValidade() {
        return listaValidade;
    }

    public void setListaValidade(ArrayList<ValidadeModel> listaValidade) {
        this.listaValidade = listaValidade;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAlterado() {
        return alterado;
    }

    public void setAlterado(int alterado) {
        this.alterado = alterado;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }
}
