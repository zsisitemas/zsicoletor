package com.zsisistemas.zsiempresa.model.singleton;
import com.zsisistemas.zsiempresa.model.Cupom;

public class SingletonCupom {

    public static Cupom instancia;

    private SingletonCupom() {
    }

    public static Cupom getCupom() {

        if (instancia == null) {
            instancia = new Cupom();
        }
        return instancia;
    }

}
